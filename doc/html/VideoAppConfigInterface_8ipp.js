var VideoAppConfigInterface_8ipp =
[
    [ "VideoAppConfigC", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC" ],
    [ "VideoAppConfigInterfaceC", "classVisualMediaN_1_1UtilityN_1_1VideoAppConfigInterfaceC.html", "classVisualMediaN_1_1UtilityN_1_1VideoAppConfigInterfaceC" ],
    [ "VIDEO_APP_CONFIG_INTERFACE_IPP_7701904", "VideoAppConfigInterface_8ipp.html#a7a1f85a6ea6dd9cfafde00c9be68e3be", null ],
    [ "ColourSpaceT", "VideoAppConfigInterface_8ipp.html#a6d4ea7ec1af8c4b001abc6a1f4d75973", [
      [ "YUV", "VideoAppConfigInterface_8ipp.html#a6d4ea7ec1af8c4b001abc6a1f4d75973ab915b1a658d4fc573f623f17b1434601", null ],
      [ "RGB", "VideoAppConfigInterface_8ipp.html#a6d4ea7ec1af8c4b001abc6a1f4d75973a889574aebacda6bfd3e534e2b49b8028", null ]
    ] ],
    [ "VideoSourceCodeT", "VideoAppConfigInterface_8ipp.html#a6d0e431d0118fd644a5803c2f806cd3e", [
      [ "LOCAL", "VideoAppConfigInterface_8ipp.html#a6d0e431d0118fd644a5803c2f806cd3ea54b4c4075463b2e02cd69f5cd139b5b2", null ],
      [ "USB", "VideoAppConfigInterface_8ipp.html#a6d0e431d0118fd644a5803c2f806cd3ea7aca5ec618f7317328dcd7014cf9bdcf", null ],
      [ "FRAMEGRABBER", "VideoAppConfigInterface_8ipp.html#a6d0e431d0118fd644a5803c2f806cd3ea2d3ea2153217b532c5a00667396beb51", null ]
    ] ]
];