var classBlackmagicFrameGrabberCallbackC =
[
    [ "BlackmagicFrameGrabberCallbackC", "classBlackmagicFrameGrabberCallbackC.html#a0cb96564b701f26f4f24f64e65f8884d", null ],
    [ "AddRef", "classBlackmagicFrameGrabberCallbackC.html#a8eab5aa126318d7e7b12bb023d7f1e62", null ],
    [ "QueryInterface", "classBlackmagicFrameGrabberCallbackC.html#a3b4d13cbd4da399a762a9e0560808851", null ],
    [ "Release", "classBlackmagicFrameGrabberCallbackC.html#a495acbdcab107e5d937f9be4ac8b441e", null ],
    [ "VideoInputFormatChanged", "classBlackmagicFrameGrabberCallbackC.html#a9e3f2464de742d02699baf7c3b515830", null ],
    [ "VideoInputFrameArrived", "classBlackmagicFrameGrabberCallbackC.html#ab3841a94f0c5bd947e2905aa09f64fae", null ],
    [ "refCount", "classBlackmagicFrameGrabberCallbackC.html#a34704ab61a01d1854c66affe4227c372", null ]
];