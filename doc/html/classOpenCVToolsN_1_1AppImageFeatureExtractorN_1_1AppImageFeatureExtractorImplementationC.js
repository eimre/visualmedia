var classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC =
[
    [ "AppImageFeatureExtractorImplementationC", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#aecfd5bb43aeb4de42fda016dcba47ecf", null ],
    [ "DecimateImage", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#a3c038d7895b80f5f98323848fce18e6c", null ],
    [ "ExtractORB", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#aa8c08b7ab9a9ec976345e94fd7f6adb0", null ],
    [ "GenerateConfigFile", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#a8a4f79506fb278415756479b991699a1", null ],
    [ "Run", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#af87808b8ed56f3d1f47db1e9ba65755b", null ],
    [ "Save", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#aa344b54c2254162c471113209bc7d496", null ],
    [ "flagHaveCuda", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#a6e6b6a00ad2dcc70990f21d24a6955a0", null ],
    [ "logger", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#ad8b9db11883cc7567e0b9e91ffdd5b33", null ],
    [ "parameters", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html#a58951442346785230b51e3a4c222eb5b", null ]
];