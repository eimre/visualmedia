var classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC =
[
    [ "DisplayModeC", "structVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC_1_1DisplayModeC.html", "structVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC_1_1DisplayModeC" ],
    [ "DecklinkWrapperC", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a8df81893f77f48f97019dd6ce76349f6", null ],
    [ "~DecklinkWrapperC", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#aba6418c80abd36a7e9aef8e8b5a33326", null ],
    [ "DecklinkWrapperC", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a97c037b1bec75b0a86621092b327cdff", null ],
    [ "GetFrameHeight", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#ab0fd7688f15b17f1b766b88f3ac786dd", null ],
    [ "GetFrameRate", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#ad002c472e2a9fbbbd7e5a13858531fbf", null ],
    [ "GetFrameWidth", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a73cd6f04277b952f8476be31ebdae651", null ],
    [ "GetLastFrame", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#ad456114d07322840c6c6e71bcc4efd8b", null ],
    [ "Initialise", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a26fbde92cb6ea4ed90176cdab2b4fc59", null ],
    [ "IsValid", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a1dde8d50d2d3c80d1af08a1dd802e768", null ],
    [ "operator=", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#adf7cf81e7ad2bb730d02b173592cfd66", null ],
    [ "QueryNewFrame", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#af21490f92fed92d77cd175b8d73245fa", null ],
    [ "StartCapture", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a89d2baf729627fd01e3823313b894161", null ],
    [ "StopCapture", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#ad27ce7130ada65036895dbe953c3dbc5", null ],
    [ "flagValid", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html#a25bacbbb71938dd0dc8db8dad509638d", null ]
];