var classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC =
[
    [ "VideoPropertiesC", "structVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC_1_1VideoPropertiesC.html", "structVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC_1_1VideoPropertiesC" ],
    [ "VideoInputDeviceC", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a428614167662e4892dcdb6a91d51cbc9", null ],
    [ "VideoInputDeviceC", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#abd49473070de8fdb400fdaab8bcbf7a7", null ],
    [ "GetBGRConversionCode", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#ad60b5497b9a03c975554f8f3b9f39e80", null ],
    [ "GetFrame", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a07ef6c1388f17c2404e9b382fbd9a8b6", null ],
    [ "GetFrameRate", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a5eee0589ba96ed18c711ba1fe437f58f", null ],
    [ "GetGrayscaleConversionCode", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a57d5f1c297782dbc62b4efa800583305", null ],
    [ "GetHeight", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a3ead6df6c2e5073f4c502a5fbcba4dc2", null ],
    [ "GetPosition", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a8920ae63449d2260cd3ef470d69116ca", null ],
    [ "GetWidth", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#afd8b76b2db6b2896d37ae76de819c70b", null ],
    [ "Initialise", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a22ff641bcd4dc020f172ba936af84425", null ],
    [ "IsEoF", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#aa291c788f457bd699520de8a803e8960", null ],
    [ "IsLive", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a640d672726788f806762a299bb4857b4", null ],
    [ "IsValid", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#abc25c84e2408621510c3ebc8b55e7dfe", null ],
    [ "operator=", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a04f09da7932f209e29521e86b6246294", null ],
    [ "QueryNewFrame", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#acb569de84a65ded78d75de8fba901042", null ],
    [ "config", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a97e4fb21d3e5d711f4ed797c3d93bcbd", null ],
    [ "flagValid", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#abfbe1598d65a36a33c440072f232061c", null ],
    [ "frameGrabberSource", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a99260a49e2b10f648c70edb2a3a28d77", null ],
    [ "ocvVideoSource", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a6fa8d8a51cdb6a067dfa3727d1606c5b", null ],
    [ "properties", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html#a5b57f0ea4c754b688c96c78c052f317e", null ]
];