var dir_9761b53e3d9349800c0cb59b71c8cd3d =
[
    [ "BrainstormRemoteMaps.cpp", "BrainstormRemoteMaps_8cpp.html", null ],
    [ "BrainstormRemoteMaps.h", "BrainstormRemoteMaps_8h.html", null ],
    [ "BrainstormRemoteMaps.ipp", "BrainstormRemoteMaps_8ipp.html", "BrainstormRemoteMaps_8ipp" ],
    [ "DecklinkWrapper.cpp", "DecklinkWrapper_8cpp.html", null ],
    [ "DecklinkWrapper.h", "DecklinkWrapper_8h.html", null ],
    [ "DecklinkWrapper.ipp", "DecklinkWrapper_8ipp.html", "DecklinkWrapper_8ipp" ],
    [ "OCVFeatures2d.cpp", "OCVFeatures2d_8cpp.html", "OCVFeatures2d_8cpp" ],
    [ "OCVFeatures2d.h", "OCVFeatures2d_8h.html", "OCVFeatures2d_8h" ],
    [ "OCVFeatures2d.ipp", "OCVFeatures2d_8ipp.html", "OCVFeatures2d_8ipp" ],
    [ "PoseFilter.cpp", "PoseFilter_8cpp.html", null ],
    [ "PoseFilter.h", "PoseFilter_8h.html", [
      [ "PoseFilterC", "classVisualMediaN_1_1UtilityN_1_1PoseFilterC.html", "classVisualMediaN_1_1UtilityN_1_1PoseFilterC" ]
    ] ],
    [ "PoseFilter.ipp", "PoseFilter_8ipp.html", "PoseFilter_8ipp" ],
    [ "VideoAppConfigInterface.cpp", "VideoAppConfigInterface_8cpp.html", null ],
    [ "VideoAppConfigInterface.h", "VideoAppConfigInterface_8h.html", null ],
    [ "VideoAppConfigInterface.ipp", "VideoAppConfigInterface_8ipp.html", "VideoAppConfigInterface_8ipp" ],
    [ "VideoInputDevice.cpp", "VideoInputDevice_8cpp.html", null ],
    [ "VideoInputDevice.h", "VideoInputDevice_8h.html", null ],
    [ "VideoInputDevice.ipp", "VideoInputDevice_8ipp.html", "VideoInputDevice_8ipp" ]
];