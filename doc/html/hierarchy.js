var hierarchy =
[
    [ "VisualMediaN::App3DSceneModelBuilderN::App3dSceneModelBuilderImplementationC", "classVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderImplementationC.html", null ],
    [ "VisualMediaN::App3DSceneModelBuilderN::App3dSceneModelBuilderParametersC", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html", null ],
    [ "OpenCVToolsN::AppImageFeatureExtractorN::AppImageFeatureExtractorImplementationC", "classOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorImplementationC.html", null ],
    [ "OpenCVToolsN::AppImageFeatureExtractorN::AppImageFeatureExtractorParametersC", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html", null ],
    [ "VisualMediaN::AppNodalCameraTrackerN::AppNodalCameraTrackerImplementationC", "classVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerImplementationC.html", null ],
    [ "VisualMediaN::AppNodalCameraTrackerN::AppNodalCameraTrackerParametersC", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html", null ],
    [ "VisualMediaN::AppPanoramaBuilderN::AppPanoramaBuilderImplementationC", "classVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderImplementationC.html", null ],
    [ "VisualMediaN::AppPanoramaBuilderN::AppPanoramaBuilderParametersC", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html", null ],
    [ "VisualMediaN::AppRoamingCameraTrackerN::AppRoamingCameraTrackerImplementationC", "classVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerImplementationC.html", null ],
    [ "VisualMediaN::AppRoamingCameraTrackerN::AppRoamingCameraTrackerParametersC", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html", null ],
    [ "BlackmagicFrameGrabberWrapperC", "classBlackmagicFrameGrabberWrapperC.html", null ],
    [ "VisualMediaN::UtilityN::BrainstormRemoteMapsC", "classVisualMediaN_1_1UtilityN_1_1BrainstormRemoteMapsC.html", null ],
    [ "VisualMediaN::UtilityN::DecklinkWrapperC", "classVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC.html", null ],
    [ "VisualMediaN::UtilityN::DecklinkWrapperC::DisplayModeC", "structVisualMediaN_1_1UtilityN_1_1DecklinkWrapperC_1_1DisplayModeC.html", null ],
    [ "IDeckLinkInputCallback", "classIDeckLinkInputCallback.html", [
      [ "BlackmagicFrameGrabberCallbackC", "classBlackmagicFrameGrabberCallbackC.html", null ]
    ] ],
    [ "VisualMediaN::UtilityN::InterfaceKAZEC", "classVisualMediaN_1_1UtilityN_1_1InterfaceKAZEC.html", null ],
    [ "VisualMediaN::UtilityN::InterfaceORBC", "classVisualMediaN_1_1UtilityN_1_1InterfaceORBC.html", null ],
    [ "VisualMediaN::UtilityN::KAZEParametersC", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html", null ],
    [ "OpenCVToolsN::AppImageFeatureExtractorN::AppImageFeatureExtractorParametersC::ORBParameters", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html", null ],
    [ "VisualMediaN::UtilityN::ORBParametersC", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html", null ],
    [ "VisualMediaN::UtilityN::PoseFilterC< SolverT >", "classVisualMediaN_1_1UtilityN_1_1PoseFilterC.html", null ],
    [ "VisualMediaN::UtilityN::PoseFilterC< P2PfSolverC >", "classVisualMediaN_1_1UtilityN_1_1PoseFilterC.html", null ],
    [ "VisualMediaN::UtilityN::PoseFilterC< P4PSolverC >", "classVisualMediaN_1_1UtilityN_1_1PoseFilterC.html", null ],
    [ "VisualMediaN::UtilityN::VideoAppConfigC", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html", null ],
    [ "VisualMediaN::UtilityN::VideoAppConfigInterfaceC", "classVisualMediaN_1_1UtilityN_1_1VideoAppConfigInterfaceC.html", null ],
    [ "VisualMediaN::UtilityN::VideoInputDeviceC", "classVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC.html", null ],
    [ "VisualMediaN::UtilityN::VideoInputDeviceC::VideoPropertiesC", "structVisualMediaN_1_1UtilityN_1_1VideoInputDeviceC_1_1VideoPropertiesC.html", null ]
];