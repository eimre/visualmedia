var structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC =
[
    [ "ORBParameters", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters" ],
    [ "AppImageFeatureExtractorParametersC", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a3d8ef79e0e128bc8caae64e1fac81f45", null ],
    [ "decimationFactor", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a333d27416ed2aba39ba5fe4875e5edf0", null ],
    [ "flagGPU", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#ac3fefc9fa905c01cffd1ef7c2e8a3f6f", null ],
    [ "imageFilename", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a4afcee829400d0b4c862a90e0d272a2e", null ],
    [ "initialFrame", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a65b4ac82a0d6b93506659222a2073572", null ],
    [ "nFrames", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a57602071416c913dadc1f3dd5fbfcbf8", null ],
    [ "orbParameters", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#a227026b3f107931d0ac79c3f3347260e", null ],
    [ "outputFilename", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC.html#ad47dc62e30b30d91bcec1fcbeea56559", null ]
];