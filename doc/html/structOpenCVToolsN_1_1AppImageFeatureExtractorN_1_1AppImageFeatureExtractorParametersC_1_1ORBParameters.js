var structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters =
[
    [ "ORBParameters", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a55c5e83ea17a0c5796c8bea22f9c7b50", null ],
    [ "edgeThreshold", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#af4a2d56dfee2ac339cd3ed073c6b3a12", null ],
    [ "fastThreshold", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#aa6e337c343667a7578e2ba36875b5974", null ],
    [ "firstLevel", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a5c847bbd21093f2aacf19437b7d59f4d", null ],
    [ "nFeatures", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#af59c58d58dfb46254a6f64d446aac5b1", null ],
    [ "nLevels", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a511663d9de02528eb9fe9d5c754b7564", null ],
    [ "patchSize", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a32c1f8d2d6ae98490dc658f6ed024669", null ],
    [ "scaleFactor", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#aec00093a832439d99045a1827f5a57af", null ],
    [ "scoreType", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a964981e16a61b7aa70e78f65527ae001", null ],
    [ "WTA_K", "structOpenCVToolsN_1_1AppImageFeatureExtractorN_1_1AppImageFeatureExtractorParametersC_1_1ORBParameters.html#a0ca9f60c6148b7c37cceca693a5b1d55", null ]
];