var structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC =
[
    [ "cameraFile", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a076ea06e90801edcca8f1cdf4ab75546", null ],
    [ "captureDelay", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a99bcfffa125e5d3a52c94e25a70483ec", null ],
    [ "flagVerbose", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#aa4ffc3389201b81add71d4f82a6b8485", null ],
    [ "focalLength", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a30eaac0760cc5542a4e3024e2c176855", null ],
    [ "kazeParameters", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#af42e2b45bd0d3e453f1c9243a351f5c8", null ],
    [ "logFile", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#aea1786628fd9b130df62244c5733fa34", null ],
    [ "minIntensityChange", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a5e491fafe33f026adfebc83fea0ee120", null ],
    [ "minSeparation", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#ae988e691bc1a64403ac9020620c98157", null ],
    [ "nKeyrames", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#ac57033f8d6075045ea3239bd2e32247a", null ],
    [ "nThreads", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a3c3fde5e6b43e60fcaf574cabf78e42c", null ],
    [ "orbParameters", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a10608f33b0a1dea49d873283c03cf66e", null ],
    [ "outputPath", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a7869cbed8edf20fd437cba0846b79a2f", null ],
    [ "pointCloudFile", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a1b1657a0541f8262707ed5b26fbb07ee", null ],
    [ "quantisationFactor", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a1a0fd111ea4de1d4a84fa72cedbde8b1", null ],
    [ "recordedFeedFile", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a8750e28a3988a17670ca8dfdd117cd42", null ],
    [ "s3dParameters", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#ac7c2f5cfe6908cc4202c1c2048879e37", null ],
    [ "sceneFeatureFile", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#aeadf2be7e4c76759ff5b5e86d19af710", null ],
    [ "seed", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#a717c727ecdb3b9cd2a94a474b180a163", null ],
    [ "sfmParameters", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#ac793a414b37ac82eded09fa2c1e60416", null ],
    [ "videoConfig", "structVisualMediaN_1_1App3DSceneModelBuilderN_1_1App3dSceneModelBuilderParametersC.html#ac3bbee58589c179fe9fd33586d843a19", null ]
];