var structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC =
[
    [ "cameraId", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#aca911e6e5f6d89c925ae8bbfd46782f8", null ],
    [ "cameraTrackFile", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a7fb084c15f8688d72e0473ef45d2142d", null ],
    [ "displayCost", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a79db566ac180f8cf8c4db1980548ecf8", null ],
    [ "endpointIP", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a9835cae7b8e04278af3b88cdb08dea4f", null ],
    [ "endpointPort", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a807a496766b16faa29f552798eef56c6", null ],
    [ "flagDisplay", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a9feb9318ffe8c010ca4abf9b69284a12", null ],
    [ "flagFilterMeasurements", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#ab4979ae6018af520ac2d97eab821b82c", null ],
    [ "flagMask", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#adcb1db088c8c2e212c9d227e08199797", null ],
    [ "flagVerbose", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#ae93fa476a7bab22cd47cf9f882655e4a", null ],
    [ "logFile", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#aadbdae3d577193cc138a0f129c0a95a7", null ],
    [ "minRotation", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a7af8e20027a06deb1f8cf44cb4be8920", null ],
    [ "nThreads", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a8fc6f811e714c4a635de799b4d469bc3", null ],
    [ "orbParameters", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a0f23934f574670803dcb1615f1c46526", null ],
    [ "outputPath", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#afd0114155f576e5d5e102ac8422787d3", null ],
    [ "recordedFeedFile", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a58296794074cce67ce4323e7df580a28", null ],
    [ "referenceCameraFile", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a3449169428807b6cebcc23952b45a417", null ],
    [ "searchRegionScale", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#ad5c1a6d3cfd21f3b71d1a1aafe013046", null ],
    [ "subpixelRefinementWindowSize", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#afdb1a5c6beedf62ba7eb1d0ae852915d", null ],
    [ "trackerParameters", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a32093b071d6a65fc0be1d4490d060b5a", null ],
    [ "videoConfig", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#a32094f56c932ac3879f8f9891eb3c165", null ],
    [ "worldFile", "structVisualMediaN_1_1AppNodalCameraTrackerN_1_1AppNodalCameraTrackerParametersC.html#af18ba86fbf90c71fc819e7079dc0aa4c", null ]
];