var structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC =
[
    [ "cameraFile", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#ac737c8f6351927d4cd7896f43f8b78f4", null ],
    [ "captureDelay", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a6ea1522ca13db7a3599f8e3a0d5591cd", null ],
    [ "flagVerbose", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a8edafbaa3e0faba199fd195850eb3bfe", null ],
    [ "focalLength", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#afc196a3b42fa30a18cce49eecf62253e", null ],
    [ "kazeParameters", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#aa371e43e55761b770ac991b0ad2910f1", null ],
    [ "logFile", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a1770413ae95481e4c74246faf2ff48fd", null ],
    [ "minIntensityChange", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a1af6f6a7bafe550875c652d82eb8d8df", null ],
    [ "minSeparation", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a6d742df1f9751e021958354675f52625", null ],
    [ "nKeyrames", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a3a0a1384a5dbe1292c4fc395b0e667ae", null ],
    [ "nThreads", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a8572d50bf919ed8e64960c89c2412993", null ],
    [ "orbParameters", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a199e4e7e57504f817f6d4fe945b3cb83", null ],
    [ "outputPath", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#aa9a6154b094a4b377f658fd3bce7a116", null ],
    [ "panoramaImage", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a3f3c74bfc7daf83e378d7ab77c90ae14", null ],
    [ "pfmParameters", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a0272fc48f24ad00933b2473d4b041a51", null ],
    [ "pointCloudFile", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a0ecc9c0379a281eb07bf96e54063a751", null ],
    [ "recordedFeedFile", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#af38bf1389dd6bd024f09babb251c9951", null ],
    [ "sceneFeatureFile", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#aa17e5e003f4f65f3a9bb255cec5ebacf", null ],
    [ "seed", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#af465106e1fbe98acc3029a800d452909", null ],
    [ "susParameters", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#a50c1aa94e11267236ad59c11587eb82f", null ],
    [ "videoConfig", "structVisualMediaN_1_1AppPanoramaBuilderN_1_1AppPanoramaBuilderParametersC.html#ac16d009630bbafd531c5a99f5c006e92", null ]
];