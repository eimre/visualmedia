var structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC =
[
    [ "cameraId", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#aa2889fc7165ad1a751c369e2495bd445", null ],
    [ "cameraTrackFile", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a22be881c4e97f01c98b61868ff1eb4a6", null ],
    [ "displayCost", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a740e0d857551a9e6de959b4d1b177c7e", null ],
    [ "endpointIP", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a550cf8d30e4c3df2f3eaf2c91c0ad57d", null ],
    [ "endpointPort", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ae001bc70a75a7af6e32b94a7789152cc", null ],
    [ "flagDisplay", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a5307c10040f52b5624a425e29658cff8", null ],
    [ "flagFilterMeasurements", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a4e12baad99547f522a429771920dd27c", null ],
    [ "flagVerbose", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ac7ed4852ce412456f19434a1c59bc443", null ],
    [ "logFile", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ab417caa6c8ffdbc42a2f1927c2632a4c", null ],
    [ "nThreads", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a227cba1ea068f7f6a6c70b74e7d88035", null ],
    [ "orbParameters", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a5688757e44c4a1703ce1cc8b2edd401b", null ],
    [ "outputPath", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#a5498f920230ea00644802b173b880517", null ],
    [ "recordedFeedFile", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ac8e12a18a7264c4b054788dc9754299b", null ],
    [ "referenceCameraFile", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#af55feb7f218ccea98524d7804009846c", null ],
    [ "trackerParameters", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ab770cb965a82ddd30e84e87cbc56e173", null ],
    [ "videoConfig", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#ac3f1d3193248ecce5d5611d3562a5d84", null ],
    [ "worldFile", "structVisualMediaN_1_1AppRoamingCameraTrackerN_1_1AppRoamingCameraTrackerParametersC.html#aa6efe6e958b84edec4bc682c6881a87f", null ]
];