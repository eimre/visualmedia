var structVisualMediaN_1_1UtilityN_1_1KAZEParametersC =
[
    [ "diffusivity", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#a7c2cd3989e1da815a925797a1e4a3d97", null ],
    [ "flagExtended", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#a9be429bf956ce1d1bb2c1b55e02e2a42", null ],
    [ "flagUpright", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#a83b0257c44adc271a1c423260948955f", null ],
    [ "nOctaveLayers", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#a49213b8a7cbc7c41362ef2c733f98b59", null ],
    [ "nOctaves", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#abfab5ed797b14c5d2e6802a53b4b31a1", null ],
    [ "threshold", "structVisualMediaN_1_1UtilityN_1_1KAZEParametersC.html#a6018b5676eae8bb5f0339865ee407c47", null ]
];