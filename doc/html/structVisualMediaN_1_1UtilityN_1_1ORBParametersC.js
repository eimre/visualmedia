var structVisualMediaN_1_1UtilityN_1_1ORBParametersC =
[
    [ "edgeThreshold", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a51552b11e31a05a9bafabf6c676d2ce9", null ],
    [ "fastThreshold", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a811468639cb9af98dcf8e3a5337ac234", null ],
    [ "firstLevel", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a6d5509361904fdc5f7385a9d7a9d2485", null ],
    [ "nFeatures", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a6a3e3f329a925782e96b5af84005d64a", null ],
    [ "nLevels", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a54596e7ec62ae17dd26327586ab8eab0", null ],
    [ "patchSize", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a2a061977f9182c1d0b4eef8bec40dcc4", null ],
    [ "scaleFactor", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a169d50c07c3fe0656a2485ead4effdf5", null ],
    [ "scoreType", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#a0dfd2d725c936c07c953c9246e786eb3", null ],
    [ "WTA_K", "structVisualMediaN_1_1UtilityN_1_1ORBParametersC.html#aa823b35a2f17fc1ed2423db2a2f13d8f", null ]
];