var structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC =
[
    [ "colourMode", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#a8aa34773fc20cba22098e0cc4a65c1c2", null ],
    [ "colourSpace", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#ae1fb422ca271d1a414d9669fb07f3935", null ],
    [ "deviceID", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#ab650fffba23a1d0fcdcb25fed9f02182", null ],
    [ "displayMode", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#aed042a65403055971050c5799973c922", null ],
    [ "downsamplingFactor", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#af66ba8b0a6f28842f8b8ca18d3c1b419", null ],
    [ "flagInterlaced", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#a61948f411bd9018b1735d81913177c0b", null ],
    [ "pixelFormat", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#aedbdf35a1b4f03807fa98356805e4ff1", null ],
    [ "sourceType", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#a1dd25d134d1f947edbd96eb792f40957", null ],
    [ "videoFilename", "structVisualMediaN_1_1UtilityN_1_1VideoAppConfigC.html#a3a9b006f1ba4114f5dfba2d1a520fd27", null ]
];