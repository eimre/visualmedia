/**
 * @file AppImageFeatureExtractor.cpp Implementation of imageFeatureExtractor
 * @author Evren Imre
 * @date 10 Apr 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/format.hpp>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <ostream>
#include <stdio.h>
#include <chrono>
#include <cstddef>
#include <map>
#include <tuple>

#ifdef HAVE_CUDA
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudawarping.hpp>
#endif	//HAVE_CUDA

namespace OpenCVToolsN
{
namespace AppImageFeatureExtractorN
{

using boost::property_tree::ptree;
using boost::property_tree::ptree;
using boost::property_tree::xml_parser::read_xml;
using boost::property_tree::xml_parser::write_xml;
using boost::property_tree::xml_parser::xml_writer_settings;
using boost::format;
using boost::str;
using cv::ORB;
using cv::Mat;
using cv::imread;
using cv::noArray;
using cv::KeyPoint;
using cv::Ptr;
using cv::Feature2D;
using cv::pyrDown;
using cv::Size;
using std::string;
using std::runtime_error;
using std::vector;
using std::locale;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;
using std::cout;
using std::ofstream;
using std::size_t;
using std::map;
using std::tuple;
using std::tie;
using std::get;
using std::make_tuple;

#ifdef HAVE_CUDA
using cv::cuda::GpuMat;
#endif

/**
 * @brief Parameter object for \c AppImageFeatureExtractorImplementationC
 * @ingroup Parameters
 */
struct AppImageFeatureExtractorParametersC
{
	bool flagGPU;	///< If \c true Cuda versions of the algorithms are called

	string imageFilename;	///< Image filename pattern
	unsigned int initialFrame;	///< The id of the first frame
	unsigned int nFrames;	///< Number of frames
	unsigned int decimationFactor;	///< Number of decimation passes. Each pass downsamples the image by 2

	struct ORBParameters
	{
		int nFeatures;	///< Number of features
		float scaleFactor;	///< Upsampling rate per pyramid level
		int nLevels;	/// Number of levels
		int edgeThreshold;	///< Border are to be omitted
		int firstLevel;	///< Fixed to 0
		int WTA_K;	///< Number of points used for generating a BRIEF descriptor
		int scoreType;	///< Strength score type
		int patchSize;	///< Descriptor RoI size
		int fastThreshold;	///< Undocumented

		ORBParameters() : nFeatures(500), scaleFactor(1.2), nLevels(8), edgeThreshold(31), firstLevel(0), WTA_K(2), scoreType(ORB::HARRIS_SCORE), patchSize(31), fastThreshold(20)
		{}
	} orbParameters;

	string outputFilename;	///< Output filename pattern

	AppImageFeatureExtractorParametersC() : flagGPU(false),  initialFrame(0), nFrames(1), decimationFactor(0)
	{}
};	//struct AppImageFeatureExtractorParametersC

/**
 * @brief Implementation of imageFeatureExtractor
 * @ingroup Application
 */
class AppImageFeatureExtractorImplementationC
{
	private:

		/**@name Configuration */ //@{
		AppImageFeatureExtractorParametersC parameters;	///< Parameter object
		bool flagHaveCuda;	///< \c true if Cuda is available
		//@}

		/**@name State */ //@{
		map<string, double> logger;	///< Logger
		//@}

		/** @name Implementation details */ //@{
		Mat DecimateImage(const Mat& image);	///< Decimates an image
		tuple<vector<KeyPoint>, Mat> ExtractORB(const Mat& image);	///< Extracts ORB features

	#ifdef HAVE_CUDA
		GpuMat DecimateImage(const GpuMat& image);	///< Decimates an image- Cuda variant
		tuple<vector<KeyPoint>, Mat> ExtractORB(const GpuMat& image);	///< Extracts ORB features- Cuda variant
	#endif //HAVE_CUDA

		void Save(const vector<KeyPoint>& keypoints, const Mat& descriptors, size_t index);	//Saves the output
		//@}

	public:

		static void GenerateConfigFile(const string& filename);	///< Generates an example config file
		AppImageFeatureExtractorImplementationC(const string& configFile);	///< Constructor

		bool Run();	///< Runs the application
};	//class AppImageFeatureExtractorImplementationC

/**
 * @brief Decimates an image
 * @param[in] image Image to be decimated
 * @return Decimated image
 */
Mat AppImageFeatureExtractorImplementationC::DecimateImage(const Mat& image)
{
	if(parameters.decimationFactor==0)
		return image;

	Mat output;
	pyrDown(image, output);

	size_t nIteration=parameters.decimationFactor-1;
	for(size_t c=0; c<nIteration; ++c)
	{
		Mat resized;
		pyrDown(output, resized);
		output=resized;
	}

	return output;
}	//Mat ResizeIDecimateImagemage(const Mat& image)

/**
 * @brief Extracts ORB features
 * @param[in] image Source image
 * @return A tuple: A vector of keypoints; Descriptors
 */
tuple<vector<KeyPoint>, Mat> AppImageFeatureExtractorImplementationC::ExtractORB(const Mat& image)
{
	//Initialise the feature extractor
	Ptr<Feature2D> pEngine=ORB::create(parameters.orbParameters.nFeatures, parameters.orbParameters.scaleFactor, parameters.orbParameters.nLevels, parameters.orbParameters.edgeThreshold, 0, parameters.orbParameters.WTA_K, parameters.orbParameters.scoreType, parameters.orbParameters.patchSize, parameters.orbParameters.fastThreshold);
	//Ptr<Feature2D> pEngine=cv::KAZE::create(true, false);
	//Ptr<Feature2D> pEngine=cv::AKAZE::create();

	//Extract features
	vector<KeyPoint> keypoints;
	Mat descriptors;
	if(parameters.decimationFactor==0)
		pEngine->detectAndCompute(image, noArray(), keypoints, descriptors, false);
	else
	{
		Mat decimated=DecimateImage(image);	//Decimate
		pEngine->detectAndCompute(decimated, noArray(), keypoints, descriptors, false);
	}	//if(parameters.decimationFactor==0)

	return make_tuple(keypoints, descriptors);
}	//tuple<vector<KeyPoint>, Map> ExtractORB(const Mat& image)

#ifdef HAVE_CUDA
/**
 * @brief Decimates an image- Cuda variant
 * @param[in] image Image to be decimated
 * @return Decimated image
 */
GpuMat AppImageFeatureExtractorImplementationC::DecimateImage(const GpuMat& image)
{
	if(parameters.decimationFactor==0)
		return image;

	GpuMat output;
	cv::cuda::pyrDown(image, output, cv::cuda::Stream::Null());

	size_t nIteration=parameters.decimationFactor-1;
	for(size_t c=0; c<nIteration; ++c)
	{
		GpuMat resized;

		cv::cuda::pyrDown(output, resized, cv::cuda::Stream::Null());

		output=resized;
	}

	return output;
}	//Mat ResizeIDecimateImagemage(const Mat& image)

/**
 * @brief Extracts ORB features- Cuda variant
 * @param[in] image Source image
 * @return A tuple: A vector of keypoints; Descriptors
 */
tuple<vector<KeyPoint>, Mat> AppImageFeatureExtractorImplementationC::ExtractORB(const GpuMat& image)
{
	//Initialise the feature extractor
	Ptr<Feature2D> pEngine=cv::cuda::ORB::create(parameters.orbParameters.nFeatures, parameters.orbParameters.scaleFactor, parameters.orbParameters.nLevels, parameters.orbParameters.edgeThreshold, 0, parameters.orbParameters.WTA_K, parameters.orbParameters.scoreType, parameters.orbParameters.patchSize, parameters.orbParameters.fastThreshold);

	//Extract features
	vector<KeyPoint> keypoints;
	GpuMat descriptors;

	if(parameters.decimationFactor==0)
		pEngine->detectAndCompute(image, noArray(), keypoints, descriptors, false);
	else
	{
		GpuMat decimated=DecimateImage(image);	//Decimate
		pEngine->detectAndCompute(decimated, noArray(), keypoints, descriptors, false);
	}	//if(parameters.decimationFactor==0)

	Mat cpuDescriptors;
	descriptors.download(cpuDescriptors);

	return make_tuple(keypoints, cpuDescriptors);
}	//tuple<vector<KeyPoint>, Map> ExtractORB(const Mat& image)

#endif //HAVE_CUDA

/**
 * @brief Saves the output
 * @param[in] keypoints Keypoints
 * @param[in] descriptors Descriptors
 * @param[in] index Frame index
 */
void AppImageFeatureExtractorImplementationC::Save(const vector<KeyPoint>& keypoints, const Mat& descriptors, size_t index)
{
	if(parameters.outputFilename.empty())
		return;

	string filename= str(format(parameters.outputFilename) % index);

	ofstream ofile(filename);

	if(ofile.fail())
		throw(runtime_error(string("AppImageFeatureExtractorImplementationC::Save : Cannot write at ") +filename ));

	ofile<<descriptors.cols * CHAR_BIT<<"\n";
	ofile<<descriptors.rows<<"\n";

	size_t nKeypoints=keypoints.size();
	size_t sDescriptor=descriptors.cols;

	for(size_t c=0; c<nKeypoints; ++c)
	{
		ofile<<keypoints[c].pt.x<<" "<<keypoints[c].pt.y<<" ";

		for(size_t c2=0; c2<sDescriptor; ++c2)
			ofile<<(int)descriptors.row(c).at<uchar>(c2)<<" ";

		ofile<<"\n";
	}
}	//void Save(const vector<Keypoint>& keypoints, const Mat& descriptors)

/**
 * @brief Constructor
 * @param[in] configFile Filename for the config file
 */
AppImageFeatureExtractorImplementationC::AppImageFeatureExtractorImplementationC(const string& configFile)
{
	flagHaveCuda=false;
#ifdef HAVE_CUDA
	flagHaveCuda=true;
#endif	//HAVE_CUDA

	ptree tree;
	read_xml(configFile, tree, boost::property_tree::xml_parser::no_comments | boost::property_tree::xml_parser::trim_whitespace);

	AppImageFeatureExtractorParametersC defaultParameters;

	//General
	parameters.flagGPU=tree.get<bool>("General.FlagGPU", defaultParameters.flagGPU);

	//ORB
	parameters.orbParameters.nFeatures=tree.get<int>("ORB.NoFeatures", defaultParameters.orbParameters.nFeatures);
	parameters.orbParameters.scaleFactor=tree.get<float>("ORB.ScaleFactor", defaultParameters.orbParameters.scaleFactor);
	parameters.orbParameters.nLevels=tree.get<int>("ORB.NoLevels", defaultParameters.orbParameters.nLevels);
	parameters.orbParameters.edgeThreshold=tree.get<int>("ORB.EdgeThreshold", defaultParameters.orbParameters.edgeThreshold);
	parameters.orbParameters.WTA_K=tree.get<int>("ORB.WTA_K", defaultParameters.orbParameters.WTA_K);
	parameters.orbParameters.scoreType=tree.get<int>("ORB.ScoreType", defaultParameters.orbParameters.scoreType);
	parameters.orbParameters.patchSize=tree.get<int>("ORB.PatchSize", defaultParameters.orbParameters.patchSize);
	parameters.orbParameters.fastThreshold=tree.get<int>("ORB.FastThreshold", defaultParameters.orbParameters.fastThreshold);

	//Input
	parameters.imageFilename=tree.get<string>("Input.Filename");
	parameters.initialFrame=tree.get<unsigned int>("Input.InitialFrame", defaultParameters.initialFrame);
	parameters.nFrames=tree.get<unsigned int>("Input.NoFrames", defaultParameters.nFrames);
	parameters.decimationFactor=tree.get<unsigned int>("Input.DecimationFactor", defaultParameters.decimationFactor);

	//Output
	parameters.outputFilename=tree.get<string>("Output.Filename","");
}	//ImageFeatureExtractorAppImplementationC()

/**
 * @brief Generates an example config file
 * @param filename Destination filename
 */
void AppImageFeatureExtractorImplementationC::GenerateConfigFile(const string& filename)
{
	AppImageFeatureExtractorParametersC defaultParameters;

	ptree tree; //Options tree

	tree.put("xmlcomment", "imageFeatureExtractor configuration file");

	tree.put("General","");
	tree.put("ORB","");
	tree.put("Input","");
	tree.put("Output","");

	//General
	tree.put("General.FlagGPU", defaultParameters.flagGPU);

	//ORB
	tree.put("ORB.xmlcomment", "See the OpenCV documentation for ORB");
	tree.put("ORB.NoFeatures", defaultParameters.orbParameters.nFeatures);
	tree.put("ORB.ScaleFactor", defaultParameters.orbParameters.scaleFactor);
	tree.put("ORB.NoLevels", defaultParameters.orbParameters.nLevels);
	tree.put("ORB.EdgeThreshold", defaultParameters.orbParameters.edgeThreshold);
	tree.put("ORB.WTA_K", defaultParameters.orbParameters.WTA_K);
	tree.put("ORB.ScoreType", defaultParameters.orbParameters.scoreType);
	tree.put("ORB.PatchSize", defaultParameters.orbParameters.patchSize);
	tree.put("ORB.FastThreshold", defaultParameters.orbParameters.fastThreshold);

	//Input
	tree.put("Input.Filename","/%05d.png");
	tree.put("Input.InitialFrame", defaultParameters.initialFrame);
	tree.put("Input.NoFrames", defaultParameters.nFrames);
	tree.put("Input.DecimationFactor", defaultParameters.decimationFactor);

	//Output
	tree.put("Output.Filename","/%05d.bft2");

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename)

/**
 * @brief Runs the application
 * @return \c true
 */
bool AppImageFeatureExtractorImplementationC::Run()
{
	auto t0=high_resolution_clock::now();   //Start the timer

	if(! (flagHaveCuda && parameters.flagGPU) )
		cout<<"CPU variant\n";
#ifdef HAVE_CUDA
	else
	{
		cout<<"GPU variant\n";
		Mat tmp1(1,1,0);
		GpuMat tmp2(tmp1);	//First call to GPU initialises the device
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s GPU initialised"<<"\n";
	}
#endif //HAVE_CUDA

	format filenamePattern=format(parameters.imageFilename);

	for(size_t c=0; c<parameters.nFrames; ++c)
	{
		//Load the image
		string filename=str(filenamePattern % (c+parameters.initialFrame) );
		Mat img=imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

		if(!img.data)
			throw(runtime_error( string("File not found ") + filename ));

		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s Image "<<c<<" read"<<"\n";

		vector<KeyPoint> keypoints;
			Mat descriptors;

		if(!flagHaveCuda || !parameters.flagGPU)
			tie(keypoints, descriptors)=ExtractORB(img);
		#ifdef HAVE_CUDA
			else
			{
				GpuMat gpuImg(img);
				tie(keypoints, descriptors)=ExtractORB(gpuImg);
			}
		#endif //HAVE_CUDA

		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s "<<keypoints.size()<<" features extracted"<<"\n";

		Save(keypoints, descriptors, c+parameters.initialFrame);
	}	//for(size_t c=0; c<parameters.nFrames; ++c)

	//Output
/*	Mat outImage;
	cv::drawKeypoints(img, keypoints, outImage);
	cv::imshow("Keypoints 1", outImage );
	cv::waitKey(0);
*/


	return true;
}	//bool Run()

}	//AppImageFeatureExtractorN
}	//OpenCVToolsN

int main ( int argc, char* argv[] )
{
	using boost::program_options::options_description;
	using boost::program_options::positional_options_description;
	using boost::program_options::value;
	using boost::program_options::variables_map;
	using boost::program_options::store;
	using boost::program_options::notify;
	using boost::program_options::command_line_parser;
	using std::string;

	//Options
	options_description commandLineOptions ( "Parameters" );
	commandLineOptions.add_options()
	( "generate-config,g", value<string>(), "Generates a sample config file" )
	( "run,r", value<string>(), "Runs the configuration file");

    positional_options_description positionalOptions;
    positionalOptions.add ( "run", -1 );

    //Parse the command line
    variables_map commandLineParameters;
    store(command_line_parser(argc,argv).options(commandLineOptions).positional(positionalOptions).run(), commandLineParameters );
    notify(commandLineParameters);

    if(commandLineParameters.count("generate-config")!=0)
    	OpenCVToolsN::AppImageFeatureExtractorN::AppImageFeatureExtractorImplementationC::GenerateConfigFile(commandLineParameters["generate-config"].as<string>());

    if(commandLineParameters.count("run")!=0)
    {
    	OpenCVToolsN::AppImageFeatureExtractorN::AppImageFeatureExtractorImplementationC application(commandLineParameters["run"].as<string>());
    	return application.Run();
    }

	return true;
}   //int main ( int argc, char* argv[] )
