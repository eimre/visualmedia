/**
 * @file AppNodalCameraTracker.cpp Implementation of nodalCameraTracker
 * @author Evren Imre
 * @date 28 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifdef _MSC_VER
#include <WinSock2.h>   //For Windows. This defines the interface keyword, which is required by "DeckLinkAPI.h" . Why panoramaBuilder and sceneModelBuilder3d do not need this, I have no idea
#endif

#include <iostream>
#include <string>
#include <memory>
#include <map>
#include <functional>
#include <chrono>
#include <future>
#include <thread>
#include <tuple>
#include <array>
#include <cmath>
#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/asio.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/highgui.hpp>
#include <SeeSaw/Application/Application.h>
#include <SeeSaw/ApplicationInterface/InterfaceUtility.h>
#include <SeeSaw/ApplicationInterface/InterfaceNodalCameraTrackingPipeline.h>
#include <SeeSaw/Elements/Feature.h>
#include <SeeSaw/Elements/Camera.h>
#include <SeeSaw/GeometryEstimationPipeline/NodalCameraTrackingPipeline.h>
#include <SeeSaw/GeometryEstimationComponents/P2PComponents.h>
#include <SeeSaw/GeometryEstimationComponents/P2PfComponents.h>
#include <SeeSaw/Geometry/Projection32.h>
#include <SeeSaw/Matcher/SceneImageFeatureMatchingProblem.h>
#include <SeeSaw/Metrics/GeometricConstraint.h>
#include <SeeSaw/Metrics/Distance.h>
#include <SeeSaw/IO/SceneFeatureIO.h>
#include <SeeSaw/IO/CameraIO.h>
#include <SeeSaw/Wrappers/BoostFilesystem.h>
#include "../Utility/OCVFeatures2d.h"
#include "../Utility/VideoAppConfigInterface.h"
#include "../Utility/VideoInputDevice.h"
#include "../Utility/BrainstormRemoteMaps.h"
#include "../Utility/PoseFilter.h"

#include <SeeSaw/Elements/FeatureUtility.h>

namespace VisualMediaN
{
namespace AppNodalCameraTrackerN
{

using namespace SeeSawN::ApplicationN;

using std::stringstream;
using std::string;
using std::unique_ptr;
using std::map;
using std::bind;
using std::greater;
using std::greater_equal;
using std::ref;
using std::cref;
using std::tuple;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::chrono::time_point;
using std::this_thread::sleep_for;
using std::this_thread::sleep_until;
using std::async;
using std::future;
using std::max;
using std::min;
using boost::optional;
using boost::for_each;
using boost::iota;
using boost::lexical_cast;
using boost::asio::io_service;
using boost::asio::ip::udp;
using boost::asio::ip::address;
using boost::math::constants::pi;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using cv::VideoCapture;
using cv::VideoWriter;
using cv::Mat;
using cv::KeyPoint;
using cv::Ptr;
using cv::noArray;
using cv::imshow;
using cv::namedWindow;
using cv::waitKey;
using cv::destroyAllWindows;
using cv::Size;
using cv::putText;
using cv::Scalar;
using cv::circle;
using cv::rectangle;
using cv::line;
using cv::Point;
using cv::TermCriteria;
using cv::cuda::GpuMat;
using cv::cuda::ORB;
using cv::cuda::cvtColor;
using cv::cuda::pyrDown;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineC;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineDiagnosticsC;
using SeeSawN::GeometryN::NodalCameraTrackingPipelineParametersC;
using SeeSawN::GeometryN::P2PEstimatorProblemT;
using SeeSawN::GeometryN::P2PfEstimatorProblemT;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::GeometryN::P2PfSolverC;
using SeeSawN::MatcherN::BinarySceneImageFeatureMatchingProblemC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::ComputeFoV;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::MetricsN::InverseHammingSceneImageFeatureDistanceConverterT;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::WrappersN::IsWriteable;
using VisualMediaN::UtilityN::ORBParametersC;
using VisualMediaN::UtilityN::ConvertToBinaryImageFeature;
using VisualMediaN::UtilityN::InterfaceORBC;
using VisualMediaN::UtilityN::PerformSubpixelRefinement;
using VisualMediaN::UtilityN::VideoAppConfigC;
using VisualMediaN::UtilityN::VideoAppConfigInterfaceC;
using VisualMediaN::UtilityN::VideoInputDeviceC;
using VisualMediaN::UtilityN::BrainstormRemoteMapsC;
using VisualMediaN::UtilityN::PoseFilterC;

//TODO Camera tracker interface
//1. Dynamic objects: Dynamically create maps of dynamic regions- GPU. Temporarily remove landmarks in these areas
//2. If a landmark is consistently missed, check it less often, or temporarily disable it
//3. A visual way to monitor the calibration- say, overlay the camera image with mosaic?

/**
 * @brief Parameters for nodalCameraTracker
 * @ingroup Application
 */
struct AppNodalCameraTrackerParametersC
{
	unsigned int nThreads=2;	///< Number of threads available to the application. >0
	bool flagFilterMeasurements=true;	///< If \c true, the measurements are filtered. Adds a delay of one frame

	ORBParametersC orbParameters;	///< Parameters for the feature extractor

	NodalCameraTrackingPipelineParametersC trackerParameters;	///< Parameters for the camera tracker

	//Input
	VideoAppConfigC videoConfig;	///< Video input configuration
									///< videoConfig.downsamplingFactor must match that of the model builder

	string worldFile;	///< Filename for the world map
	string referenceCameraFile;	///< Filename for the reference camera, with the known camera parameters

	//Output

	bool flagDisplay=true;	///< If \c true, the interface is turned on
	unsigned int displayCost=5;	///< Cost of displaying the interface, in ms. The interface is skipped if there is not enough time

	string outputPath;	///< Root for the output directory
	string logFile;	///< Log
	string cameraTrackFile;	///< Camera track
	bool flagVerbose=true;	///< \c true if verbose mode is on

	string endpointIP;	///< IP of the endpoint device
	unsigned short endpointPort=65535;	///< Port no on the endpoint device

	string cameraId="1";   ///< Id of the camera

	string recordedFeedFile;  ///< Saves the incoming camera feed, if non-empty

	//FIXME Below are a set of kludges and experimental parameters for validation
    float minRotation=0;   ///< Orientation changes below this value are not passed to Infinity Set. Norm of the interframe rotation vector. Radians. >=0.
    bool flagMask=false;    ///< If \c true feature extraction is limited to a search region of the predicted landmark projections
    double searchRegionScale=1; ///< Scales the search regions on the mask >0
    size_t subpixelRefinementWindowSize=5;  ///< Window size for subpixel refinement >2

    AppNodalCameraTrackerParametersC()
    {
        orbParameters.nFeatures=750;

        trackerParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold=8e-3;
        trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=3;
        trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=3;
        trackerParameters.pipelineParameters.matcherParameters.flagBucketFilter=false;
        trackerParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration=10;
        trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=0.5;
        trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration=100;
        trackerParameters.pipelineParameters.maxIteration=3;
        trackerParameters.pipelineParameters.maxObservationDensity=1;
    }
};	//struct AppNodalCameraTrackerParametersC

/**
 * @brief Implementation of nodalCameraTracker
 * @ingroup Application
 * @nosubgrouping
 */
class AppNodalCameraTrackerImplementationC
{
	private:

		/** @name Configuration */ //@{
		unique_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppNodalCameraTrackerParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logger

		Mat colourFrame;	///< Current colour frame, on the host
		Mat currentFrameBGR;   ///< Current BGR colour frame, on the host
		Mat grayscaleFrame;	///< Current grayscale frame, on the host
		array<Mat,2> hImageG;	///< Processed grayscale image buffer on the host
		GpuMat dImageG;	///< Grayscale image on the device
		GpuMat dDescriptors;	///< Feature descriptors, device
		Mat hDescriptors;	///< Feature descriptors, host

		bool flagRegistrationComplete=false;	///< Indicates whether the last registration task is completed
		long long int lastCompletedFrameId=-1;	///< Indicates the id of the last completed frame

		bool flagORBComplete=false; ///< Indicates whether the feature extraction operation is completed
		long long int lastORBFrameId=-1;    ///< Indicates the id of the last frame for which the ORB features are extracted

		VideoInputDeviceC videoDevice;	///< Video input device
		VideoWriter recorder;   ///< Device recording the camera feed for troubleshooting

		PoseFilterC<P2PfSolverC> poseFilter;	///< Pose filter
		//@}

		/** @name Implementation details */ //@{
		typedef time_point<high_resolution_clock> TimePointT;	///<Time point type

		static ptree PruneTrackerTree(const ptree& src);	///< Removes the redundant elements from the nodal camera tracker tree
		static ptree InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters);  ///< Injects the application-specific default parameters into the parameter tree

		CameraC LoadCamera() const;	///< Loads the reference camera
		vector<OrientedBinarySceneFeatureC> LoadWorld(const CameraC& referenceCamera) const;	///< Loads a world map
		Matrix3d Initialise(CameraC& referenceCamera);	///< Initialisation

		void ProcessFrame(vector<GpuMat>& dBuffer, Mat currentFrame);	///< Processes the current frame

		typedef vector<KeyPoint> KeypointContainerT;	///< Keypoint container type
		typedef Mat DescriptorStackT;	///< Descriptor stack type
		typedef tuple<KeypointContainerT, DescriptorStackT> FeatureExtractorOutputT;	///< Output type for feature extractors
		vector<BinaryImageFeatureC> ExtractORB( vector<GpuMat>& dBuffer, const Matrix3d& normaliser, const GpuMat& dMask, bool flagMask, unsigned int cFrame);	///< Extracts ORB features

		typedef BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> MatchingProblemT;
		typedef NodalCameraTrackingPipelineC<P2PEstimatorProblemT, P2PfEstimatorProblemT, MatchingProblemT> TrackerT;
		NodalCameraTrackingPipelineDiagnosticsC Register(TrackerT& tracker, vector<BinaryImageFeatureC> imageFeatureSet, unsigned int seed);	///< Registers the current frame to the world map

		void Display(const NodalCameraTrackingPipelineDiagnosticsC& diagnostics, const vector<OrientedBinarySceneFeatureC>& world, const TrackerT::TrackerStateC& trackerState, const CameraC& referenceCamera, unsigned int cFrame);	///< Displays the current image and the diagnostics

		TrackerT::TrackerStateC ApplyFilter(const TrackerT::TrackerStateC& measurement, const CameraC& referenceCamera, unsigned int support);	///< Applies a smoothing filter to the measurements

		void SaveLog(const list< tuple<unsigned int, bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack );	///< Saves the application log
		void SaveOutput(const list< tuple<unsigned int, bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack, const CameraC& referenceCamera);	///< Saves the output

		void UpdateMask(Mat& mask, GpuMat& dMask, const TrackerT::TrackerStateC& state, const vector<OrientedBinarySceneFeatureC>& world, const CameraC& referenceCamera, const TrackerT& maskPropagator);  ///< Updates the feature extraction mask
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppNodalCameraTrackerImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& treeO);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//AppNodalCameraTrackerImplementationC

/********** AppNodalCameraTrackerImplementationC **********/

/**
 * @brief Removes the redundant elements from the nodal camera tracker tree
 * @param[in] src Source
 * @return Pruned tree
 */
ptree AppNodalCameraTrackerImplementationC::PruneTrackerTree(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Problem.FocalLength"))
		output.get_child("Problem").erase("FocalLength");

	if(src.get_child_optional("Problem.ImageDiagonal"))
		output.get_child("Problem").erase("ImageDiagonal");

	if(src.get_child_optional("Tracking.GeometryEstimationPipeline.Main.FlagCovariance"))
		output.get_child("Tracking.GeometryEstimationPipeline.Main.").erase("FlagCovariance");

	if(src.get_child_optional("Tracking.GeometryEstimationPipeline.CovarianceEstimation"))
		output.get_child("Tracking.GeometryEstimationPipeline").erase("CovarianceEstimation");

	return output;
}	//ptree PruneTrackerTree(const ptree& src)

/**
 * @brief Injects the application-specific default parameters into the parameter tree
 * @param[in] src Parameter tree
 * @param[in] defaultParameters Default parameters
 * @return Updated tree
 */
ptree AppNodalCameraTrackerImplementationC::InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters)
{
    ptree output(src);

    if(!src.get_child_optional("FeatureExtraction.NoFeatures"))
        output.put("FeatureExtraction.NoFeatures", defaultParameters.orbParameters.nFeatures);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.kNN.MinSimilarity"))
        output.put("Tracking.GeometryEstimationPipeline.Matching.kNN.MinSimilarity", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality"))
        output.put("Tracking.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable"))
        output.put("Tracking.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.flagBucketFilter);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration"))
        output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio"))
        output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration"))
        output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Main.MaxIteration"))
         output.put("Tracking.GeometryEstimationPipeline.Main.MaxIteration", defaultParameters.trackerParameters.pipelineParameters.maxIteration);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Decimation.MaxObservationDensity"))
        output.put("Tracking.GeometryEstimationPipeline.Decimation.MaxObservationDensity", defaultParameters.trackerParameters.pipelineParameters.maxObservationDensity);

   return output;
}   //ptree InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters)

/**
 * @brief Loads the reference camera
 * @return Reference camera
 */
CameraC AppNodalCameraTrackerImplementationC::LoadCamera() const
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.referenceCameraFile);

	if(cameraList.size()!=1)
		throw(invalid_argument(string("AppNodalCameraTrackerImplementationC::LoadCamera : The camera file must have exactly one element. Value=") +lexical_cast<string>(cameraList.size())) );

	if( !cameraList[0].Extrinsics() || !cameraList[0].Extrinsics()->position)
		throw(invalid_argument("AppNodalCameraTrackerImplementationC::LoadCamera: Camera centre must be defined"));

	if(!cameraList[0].Intrinsics())
		cameraList[0].Intrinsics()=IntrinsicC();

	return cameraList[0];
}	//CameraC LoadCamera()

/**
 * @brief Loads a world map
 * @param[in] referenceCamera Reference camera
 * @return World map
 */
vector<OrientedBinarySceneFeatureC> AppNodalCameraTrackerImplementationC::LoadWorld(const CameraC& referenceCamera) const
{
	vector<OrientedBinarySceneFeatureC> output=OrientedBinarySceneFeatureIOC::ReadFeatureFile(parameters.worldFile, true);
	for_each(output, [&](OrientedBinarySceneFeatureC& current){current.Coordinate()-=*referenceCamera.Extrinsics()->position;} );	//Shift the origin to the camera centre
	return output;
}	//vector<OrientedBinarySceneFeatureC> LoadWorld(const CameraC& referenceCamera)

/**
 * @brief Initialisation
 * @param[in, out] referenceCamera Reference camera
 * @remarks Starts the frame acquisition as well
 * @return Normaliser
 */
Matrix3d AppNodalCameraTrackerImplementationC::Initialise(CameraC& referenceCamera)
{
	videoDevice.Initialise(parameters.videoConfig);

	//Initialise the video recorder
    if(!parameters.recordedFeedFile.empty())
        recorder.open(parameters.outputPath+"/"+parameters.recordedFeedFile, VideoWriter::fourcc('H','2','6','4'), videoDevice.GetFrameRate(), Size(videoDevice.GetWidth(), videoDevice.GetHeight()) );

	//Reference camera
	unsigned int height=videoDevice.GetHeight();
	unsigned int width=videoDevice.GetWidth();

	referenceCamera.FrameBox()=FrameT(Coordinate2DT(0,0), Coordinate2DT(width-1, height-1));

	if(!referenceCamera.Intrinsics())
		referenceCamera.Intrinsics()=IntrinsicC();

	if(!referenceCamera.Intrinsics()->principalPoint)	//If no principal point for the camera, set as the image centre
		referenceCamera.Intrinsics()->principalPoint=referenceCamera.FrameBox()->center();

	//Parameters
	parameters.trackerParameters.imageDiagonal=hypot(height, width);

	//Normaliser
	Matrix3d normaliser=Matrix3d::Identity();
	normaliser(0,1)=referenceCamera.Intrinsics()->skewness;
	normaliser(1,1)*=referenceCamera.Intrinsics()->aspectRatio;
	normaliser.col(2).segment(0,2)=*referenceCamera.Intrinsics()->principalPoint;
	normaliser=normaliser.inverse();

	return normaliser;
}	//Matrix3d Initialise(CameraC& referenceCamera, const VideoCapture& captureDevice)

/**
 * @brief Processes the current frame
 * @param[in, out] dBuffer Buffers on the device
 * @param[in] currentFrame Ingested frame
 */
void AppNodalCameraTrackerImplementationC::ProcessFrame(vector<GpuMat>& dBuffer, Mat currentFrame)
{
    //Recording the feed?
    if(recorder.isOpened())
    {
        //BGR conversion
        int colorConversionCode=videoDevice.GetBGRConversionCode();
        if(colorConversionCode!=-1)
            cv::cvtColor(currentFrame, currentFrameBGR, colorConversionCode);
        else
            currentFrameBGR=currentFrame;

        //Record
        recorder<<currentFrameBGR;
    }   // if(recorder.isOpened())

    int grayscaleConversionCode=videoDevice.GetGrayscaleConversionCode();
    if(grayscaleConversionCode!=-1)
        cv::cvtColor(currentFrame, grayscaleFrame,grayscaleConversionCode);

	dBuffer[0].upload(grayscaleFrame);

	//Pick the even lines
	if(parameters.videoConfig.flagInterlaced)
		cv::cuda::resize(dBuffer[0], dBuffer[1], Size(), 1, 0.5, cv::INTER_NEAREST);
	else
		dBuffer[1]=dBuffer[0];

	//Downsample
	if(parameters.videoConfig.downsamplingFactor>0)
	{
		for(size_t c=0; c<parameters.videoConfig.downsamplingFactor-1; ++c)
			cv::cuda::pyrDown(dBuffer[c+1], dBuffer[c+2]);

		cv::cuda::pyrDown(*dBuffer.rbegin(), dImageG);
	}	//if(parameters.videoConfig.downsamplingFactor>0)
	else
        dImageG=dBuffer[1];

	//Update the grayscale image buffer on the host
	hImageG[1]=hImageG[0].clone();
	dImageG.download(hImageG[0]);
}	//void ProcessFrame(vector<GpuMat>& dBuffer, vector<Mat>& hBuffer, Mat currentFrame)

/**
 * @brief Extracts ORB features
 * @param[in, out] dBuffer Device buffers for various images
 * @param[in] normaliser Normalising matrix
 * @param[in] dMask Image mask, on the device
 * @param[in] flagMask \c true if the mask is to be applied
 * @param[in] cFrame Id of the frame received by the funciton
 * @return Keypoints and descriptors
 */
vector<BinaryImageFeatureC> AppNodalCameraTrackerImplementationC::ExtractORB(vector<GpuMat>& dBuffer, const Matrix3d& normaliser, const GpuMat& dMask, bool flagMask, unsigned int cFrame)
{
    TimePointT t0=high_resolution_clock::now();

    flagORBComplete=false;

	//Read the frame into the gpu
	videoDevice.GetFrame(colourFrame);

	if(recorder.isOpened())
	    recorder<<colourFrame;

	ProcessFrame(dBuffer, colourFrame);

	//Initialise the feature extractor
	Ptr<ORB> pEngine=ORB::create(parameters.orbParameters.nFeatures, parameters.orbParameters.scaleFactor, parameters.orbParameters.nLevels, parameters.orbParameters.edgeThreshold, 0, parameters.orbParameters.WTA_K, parameters.orbParameters.scoreType, parameters.orbParameters.patchSize, parameters.orbParameters.fastThreshold);

	//Extract features
	vector<KeyPoint> keypoints;
	if(flagMask)
	    pEngine->detectAndCompute(dImageG, dMask, keypoints, dDescriptors, false);
	else
	    pEngine->detectAndCompute(dImageG, noArray(), keypoints, dDescriptors, false);
	dDescriptors.download(hDescriptors);

	size_t windowSize= (parameters.flagMask && !flagMask) ? parameters.subpixelRefinementWindowSize-1 : parameters.subpixelRefinementWindowSize;    //A smaller window compansates for the increased number of features due to the unavailability of a mask

	keypoints=PerformSubpixelRefinement(keypoints, hImageG[0], Size(windowSize, windowSize), Size(-1,-1), TermCriteria(TermCriteria::MAX_ITER+TermCriteria::EPS, 5, 1e-2));

	double scaleX=pow(2, parameters.videoConfig.downsamplingFactor);
	double scaleY=pow(2, parameters.videoConfig.downsamplingFactor) * ( (parameters.videoConfig.flagInterlaced) ? 2:1);
	for_each(keypoints, [&](KeyPoint& current){current.pt.x*=scaleX; current.pt.y*=scaleY;});

	vector<BinaryImageFeatureC> featureList=ConvertToBinaryImageFeature(keypoints, hDescriptors, true);

	for_each(featureList, [&](BinaryImageFeatureC& current){current.Coordinate() = (normaliser* (current.Coordinate().homogeneous())).eval().hnormalized();} );

	logger["tF3"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e6;

    lastORBFrameId=cFrame;
    flagORBComplete=true;

	return featureList;
}	//tuple<vector<KeyPoint>, Map> ExtractORB(const Mat& image)

/**
 * @brief Registers the current frame to the world map
 * @param[in, out] tracker Tracker
 * @param[in] imageFeatureSet Features for the current image. Pass by value on purpose
 * @param[in] seed Seed for the RNG
 * @return Diagnostics object
 */
NodalCameraTrackingPipelineDiagnosticsC AppNodalCameraTrackerImplementationC::Register(TrackerT& tracker, vector<BinaryImageFeatureC> imageFeatureSet, unsigned int seed)
{
	flagRegistrationComplete=false;
	logger["tR"]=numeric_limits<double>::infinity();
	TimePointT t0=high_resolution_clock::now();

	TrackerT::rng_type rng(seed);

	NodalCameraTrackingPipelineDiagnosticsC diagnostics=tracker.Update(rng, imageFeatureSet);
	flagRegistrationComplete=true;
	lastCompletedFrameId=seed;

	logger["tR"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e6;

	return diagnostics;
}	//NodalCameraTrackingPipelineDiagnosticsC Register(const vector<BinaryImageFeatureC>& imageFeatureSet, const vector<OrientedBinarySceneFeatureC>& world, unsigned int seed)

/**
 * @brief Displays the current image and the diagnostics
 * @param[in] diagnostics Diagnostics
 * @param[in] world World map
 * @param[in] trackerState Tracker state
 * @param[in] referenceCamera Reference camera
 * @param[in] cFrame Frame id
 */
void AppNodalCameraTrackerImplementationC::Display(const NodalCameraTrackingPipelineDiagnosticsC& diagnostics, const vector<OrientedBinarySceneFeatureC>& world, const TrackerT::TrackerStateC& trackerState, const CameraC& referenceCamera, unsigned int cFrame)
{
	//Tracking mode
	Point modeCentre=Point(10,hImageG[1].rows-10);

	if(diagnostics.flagSuccess)
		circle(hImageG[1], modeCentre, 5, Scalar(255), (diagnostics.flagRelocalise ? 2:-1));
	else
		circle(hImageG[1], modeCentre, 5, Scalar(0), 2);

	//Tracking quality
	if(diagnostics.flagSuccess)
	{
		double meanError=sqrt(diagnostics.geDiagnostics.geometryEstimatorDiagnostics.rbegin()->error/diagnostics.nInliers);	//Pseudo-huber returns pixel^2
		if(diagnostics.flagNodal)
			meanError*=trackerState.focalLength;

		double maxError=ReprojectionErrorConstraintT::error_type::ComputeOutlierThreshold(parameters.trackerParameters.inlierRejectionProbability, parameters.trackerParameters.imageNoiseVariance);

		unsigned int lineHeight=40;
		Point lowerLeft(modeCentre.x+10, modeCentre.y);
		Point upperRight(modeCentre.x+20, modeCentre.y - lineHeight*sqrt(meanError)/maxError);
		rectangle(hImageG[1], lowerLeft, upperRight, Scalar(255), -1);
		line(hImageG[1], Point(lowerLeft.x, modeCentre.y-lineHeight), Point(upperRight.x, modeCentre.y-lineHeight), Scalar(255), 2, 8);
	}	//if(diagnostics.flagSuccess)

	//Support
	stringstream displayString;
	displayString<<diagnostics.nInliers<<" "<<cFrame;
	putText(hImageG[1], displayString.str(), Point(modeCentre.x+30, modeCentre.y),cv:: FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255),1 );

	size_t nLandmarks=world.size();
	vector<bool> supporters(nLandmarks, false);   //If true, the landmark is detected as an inlier supporting the most recent registration
	for(const auto& current : diagnostics.supportSet)
	    supporters[current.left]=true;

	//Camera matrix
	CameraC currentCamera(referenceCamera);
	currentCamera.Extrinsics()->position=Coordinate3DT::Zero();
	currentCamera.Extrinsics()->orientation=trackerState.orientation;
	currentCamera.Intrinsics()->focalLength=trackerState.focalLength;
	CameraMatrixT mP=*currentCamera.MakeCameraMatrix();

	//World
	double scaleX = 1.0/pow(2, parameters.videoConfig.downsamplingFactor);
	double scaleY = scaleX/(parameters.videoConfig.flagInterlaced ? 2:1);

	Projection32C projector(mP);
	for(size_t c=0; c<nLandmarks; ++c)
	{
		Coordinate2DT projected=projector(world[c].Coordinate());

		if(!currentCamera.FrameBox()->contains(projected))
		    continue;

		if(supporters[c])
		    circle(hImageG[1], Point(projected[0]*scaleX, projected[1]*scaleY), 2, Scalar(255), -1);    //An inlier is marked with a filled circle
		else
		    circle(hImageG[1], Point(projected[0]*scaleX, projected[1]*scaleY), 2, Scalar(255), 1); //A landmark that is not detected is marked with a hollow circle
	}	//for(const auto& current : world)

	//If the support gets too low, draw a frame around the image
	if((float)diagnostics.nInliers/nLandmarks < 0.33)
	    rectangle(hImageG[1], Point(0,0), Point(hImageG[1].cols-1, hImageG[1].rows-1), Scalar(255), 20);

	imshow("Video feed", hImageG[1]);
}	//void Display(const NodalCameraTrackingPipelineDiagnosticsC& diagnostics)

/**
 * @brief Applies a smoothing filter to the measurements
 * @param[in] measurement Orientation measurement for the current frame
 * @param[in] referenceCamera Reference camera, holds the known camera parameters
 * @param[in] support Support for the measurement
 * @return Estimated orientation for the previous frame
 */
auto AppNodalCameraTrackerImplementationC::ApplyFilter(const TrackerT::TrackerStateC& measurement, const CameraC& referenceCamera, unsigned int support) -> TrackerT::TrackerStateC
{
	//Convert to camera matrix
	CameraC currentCamera(referenceCamera);
	currentCamera.Extrinsics()->orientation=measurement.orientation;
	currentCamera.Intrinsics()->focalLength=measurement.focalLength;
	CameraMatrixT mP=*currentCamera.MakeCameraMatrix();

	//Filter
	CameraC filteredCamera(poseFilter.Apply(mP, support));

	TrackerT::TrackerStateC filteredState(measurement);
	filteredState.orientation=*filteredCamera.Extrinsics()->orientation;
	filteredState.focalLength=*filteredCamera.Intrinsics()->focalLength;

	return filteredState;
}	//TrackerT::TrackerStateC ApplyFilter(const TrackerT::TrackerStateC& measurements, unsigned int support)

/**
 * @brief Saves the output
 * @param[in] cameraTrack Camera track
 * @param[in] referenceCamera Reference camera
 */
void AppNodalCameraTrackerImplementationC::SaveOutput(const list< tuple<unsigned int, bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack, const CameraC& referenceCamera)
{
	if(parameters.cameraTrackFile.empty())
		return;

	vector<CameraC> output(cameraTrack.size(), referenceCamera);

	size_t index=0;
	for(const auto& current : cameraTrack)
	{
		output[index].Tag()=lexical_cast<string>(get<0>(current));

		output[index].Extrinsics()->orientation=get<2>(current).orientation;
		output[index].Intrinsics()->focalLength=get<2>(current).focalLength;
		++index;
	}	//for(const auto& current : cameraTrack)

	CameraIOC::WriteCameraFile(output, parameters.outputPath+parameters.cameraTrackFile);
}	//void SaveOutput(const list< tuple<bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack)

/**
 * @brief Saves the application log
 * @param[in] cameraTrack Camera track
 */
void AppNodalCameraTrackerImplementationC::SaveLog(const list< tuple<unsigned int, bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack )
{
	if(parameters.logFile.empty())
		return;

	ofstream logFile(parameters.outputPath+parameters.logFile);

	if(logFile.fail())
		throw(runtime_error("AppNodalCameraTrackerImplementationC::SaveLog : Cannot open file at "+string(parameters.outputPath+parameters.logFile)));

	string timeString= to_simple_string(second_clock::universal_time());
	logFile<<"#nodalCameraTracker log file created on "<<timeString<<"\n";

	size_t nSuccess=0;
	for(const auto& current : cameraTrack)
		if(get<1>(current))
			++nSuccess;

	logFile<<"#Tracked frames: "<<logger["FrameCount"]<<"\n";
	logFile<<"#Registered frames: "<<nSuccess<<"\n";
	logFile<<"#Execution time: "<<logger["ExecutionTime"]<<"s \n";

	logFile<<"#Frame  Registered? Relocalised?  Z-deviation    Focal length    Support    Error    Features(msec) Registration(msec) Interface(msec) \n";

	for(const auto& current : cameraTrack)
	{
	    double zDeviation=acos(-get<2>(current).orientation.matrix()(2,0))-pi<double>()/2;      //Dot product with the z-axis does not yield a signed deviation
	                                                                                            //Dot product with -x axis - 90 degrees -> right of the z-axis is positive, and the left, negative

		if(get<3>(current).nInliers>0 && get<3>(current).flagSuccess)
		{
			double meanError=sqrt(get<3>(current).geDiagnostics.geometryEstimatorDiagnostics.rbegin()->error/get<3>(current).nInliers);	//Pseudo-huber is pixel^2
			meanError*=1.2;    //Compensation for the Huber scale. Between 0 and 5.99, the loss scale is 0.8-0.84. The inverse of 0.82 is ~1.21
			if(get<3>(current).flagNodal)
				meanError*=get<2>(current).focalLength;

			logFile<<get<0>(current)<<" "<<get<1>(current)<<" "<<get<3>(current).flagRelocalise<<" "<<zDeviation<<" "<<get<2>(current).focalLength<<" "<<get<3>(current).nInliers<<" "<<meanError<<" ";
		}
		else	//Usually this means that the registration was not completed in time. So, no inlier-outlier classification available
			logFile<<get<0>(current)<<" "<<get<1>(current)<<" "<<get<3>(current).flagRelocalise<<" "<<zDeviation<<" "<<get<2>(current).focalLength<<" "<<0<<" "<<0<<" ";


		logFile<<get<4>(current)[0].count()/1e6<<" "<<get<4>(current)[1].count()<<" "<<get<4>(current)[2].count()/1e6<<"\n";
	}

}	//void SaveLog(const list< tuple<bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC> >& cameraTrack )

/**
 * @brief Updates the feature extraction mask
 * @param[in, out] mask Mask on the host
 * @param[in, out] dMask Mask on the device
 * @param[in] state Tracker state
 * @param[in] world World
 * @param[in] referenceCamera Reference camera
 * @param[in] maskPropagator
 */
void AppNodalCameraTrackerImplementationC::UpdateMask(Mat& mask, GpuMat& dMask, const TrackerT::TrackerStateC& state, const vector<OrientedBinarySceneFeatureC>& world, const CameraC& referenceCamera, const TrackerT& maskPropagator)
{
    //Scaling factors for the projections
    double scaleX = 1.0/pow(2, parameters.videoConfig.downsamplingFactor);
    double scaleY = scaleX/(parameters.videoConfig.flagInterlaced ? 2:1);

    //Initialisation and reset
    mask=Mat::zeros(videoDevice.GetHeight()*scaleY, videoDevice.GetWidth()*scaleX, CV_8UC1);

    //Propagate the state twice, as the current image is leading the tracker by two frames
    TrackerT::TrackerStateC propagated=maskPropagator.PropagateState(state);
    propagated=maskPropagator.PropagateState(propagated);

    //Camera matrix
    CameraC currentCamera(referenceCamera);
    currentCamera.Extrinsics()->position=Coordinate3DT::Zero();
    currentCamera.Extrinsics()->orientation=propagated.orientation;
    currentCamera.Intrinsics()->focalLength=propagated.focalLength;
    CameraMatrixT mP=*currentCamera.MakeCameraMatrix();

    //Search region dimensions
    double radius=ReprojectionErrorConstraintT::error_type::ComputeOutlierThreshold(parameters.trackerParameters.inlierRejectionProbability, parameters.trackerParameters.imageNoiseVariance+2*parameters.trackerParameters.predictionNoise);

    //Project the landmarks to their predicted locations
    Projection32C projector(mP);

    size_t height=mask.rows;
    size_t width=mask.cols;
    double radiusX = radius*scaleX * parameters.searchRegionScale;
    double radiusY = radius*scaleY * parameters.searchRegionScale;
    for(const auto& current : world)
    {
        Coordinate2DT projected=projector(current.Coordinate());
        projected[0]*=scaleX;
        projected[1]*=scaleY;

        if(projected[0]-radiusX>=0 && projected[0]+radiusX<width && projected[1]-radiusY>=0 && projected[1]+radiusY<height)
            mask(cv::Rect(projected[0]-radiusX, projected[1]-radiusY, 2*radiusX, 2*radiusY))=Scalar(255);
    }   //for(const auto& current : world)

    dMask.upload(mask);
}   //void UpdateMask(Mat& mask, GpuMat& dMask, const TrackerT::TrackerStateC& state)

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppNodalCameraTrackerImplementationC::AppNodalCameraTrackerImplementationC()
{
	AppNodalCameraTrackerParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
}	//AppSparseUnitSphereBuilderImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppNodalCameraTrackerImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppNodalCameraTrackerImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppNodalCameraTrackerParametersC defaultParameters;

	tree.put("General","");
	tree.put("FeatureExtraction","");
	tree.put("Tracking","");
	tree.put("Input","");
	tree.put("Input.Video","");
	tree.put("Output","");
	tree.put("Output.Troubleshooting","");

	if(detailLevel>0)
	{
		tree.put("General.NoThreads", defaultParameters.nThreads);

		tree.put("Input.WorldFile", "/home/world.bft3");
		tree.put("Input.ReferenceCameraFile", "/home/reference.cam");

		tree.put("Output.FlagFilterMeasurements", defaultParameters.flagFilterMeasurements);
		tree.put("Output.FlagDisplay", defaultParameters.flagDisplay);
		tree.put("Output.DisplayCost", defaultParameters.displayCost);
		tree.put("Output.Root","/home/");
		tree.put("Output.Log", "log.txt");
		tree.put("Output.CameraTrack", "CameraTrack.cam");
		tree.put("Output.FlagVerbose", defaultParameters.flagVerbose);

		tree.put("Output.EndpointIP", defaultParameters.endpointIP);
		tree.put("Output.EndpointPort", defaultParameters.endpointPort);

        tree.put("Output.CameraId", defaultParameters.cameraId);

		tree.put("Output.Troubleshooting.RecordedFeedFile", defaultParameters.recordedFeedFile);

		tree.put("Output.Troubleshooting.MinRotation", defaultParameters.minRotation);
		tree.put("Output.Troubleshooting.FlagMask", defaultParameters.flagMask);
		tree.put("Output.Troubleshooting.SearchRegionScale", defaultParameters.searchRegionScale);
		tree.put("Output.Troubleshooting.SubpixelRefinementWindowSize", defaultParameters.subpixelRefinementWindowSize);
	}	//if(detailLevel>0)

	VideoAppConfigInterfaceC videoAppInterface;
	tree.put_child("Input.Video", videoAppInterface.MakeParameterTree(VideoAppConfigC(), detailLevel));

	tree.put_child("FeatureExtraction", InterfaceORBC::MakeParameterTree(defaultParameters.orbParameters, detailLevel));

	tree.put_child("Tracking", PruneTrackerTree(InterfaceNodalCameraTrackingPipelineC::MakeParameterTree(defaultParameters.trackerParameters, detailLevel)));

    if(!(detailLevel>1))
        if(tree.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            tree.get_child("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");


	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] treeO Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppNodalCameraTrackerImplementationC::SetParameters(const ptree& treeO)
{

    ptree tree=InjectDefaultValues(treeO, parameters);

	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	parameters.nThreads=ReadParameter(tree, "General.NoThreads", gt0, "is not positive", optional<double>(parameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	if(tree.get_child_optional("FeatureExtraction"))
		parameters.orbParameters=InterfaceORBC::MakeParameterObject(tree.get_child("FeatureExtraction"));

	if(tree.get_child_optional("Tracking"))
		parameters.trackerParameters=InterfaceNodalCameraTrackingPipelineC::MakeParameterObject(PruneTrackerTree(tree.get_child("Tracking")));

	parameters.trackerParameters.nThreads=max((int)parameters.nThreads-2, 1);

	parameters.worldFile=tree.get<string>("Input.WorldFile", parameters.worldFile);
	parameters.referenceCameraFile=tree.get<string>("Input.ReferenceCameraFile", parameters.referenceCameraFile);

	VideoAppConfigInterfaceC videoAppInterface;
	parameters.videoConfig=videoAppInterface.MakeParameterObject(tree.get_child("Input.Video"));

	parameters.flagFilterMeasurements=tree.get<bool>("Output.FlagFilterMeasurements", parameters.flagFilterMeasurements);
	parameters.flagDisplay=tree.get<bool>("Output.FlagDisplay", parameters.flagDisplay);
	parameters.displayCost=ReadParameter(tree, "Output.DisplayCost", geq0, "is not >=0", optional<double>(parameters.displayCost));

	parameters.outputPath=tree.get<string>("Output.Root", parameters.outputPath);
	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("AppNodalCameraTrackerImplementationC::SetParameters: Cannot write the path at ") + parameters.outputPath) );

	parameters.cameraTrackFile=tree.get<string>("Output.CameraTrack", parameters.cameraTrackFile);
	parameters.logFile=tree.get<string>("Output.Log", parameters.logFile);

	parameters.endpointIP=tree.get<string>("Output.EndpointIP", parameters.endpointIP);
	parameters.endpointPort=tree.get<unsigned short>("Output.EndpointPort", parameters.endpointPort);

	parameters.cameraId=tree.get<string>("Output.CameraId", parameters.cameraId);

	parameters.recordedFeedFile = tree.get<string>("Output.Troubleshooting.RecordedFeedFile","");

	parameters.minRotation = ReadParameter(tree, "Output.Troubleshooting.MinRotation", geq0, "is not >=0", optional<double>(parameters.minRotation));

	parameters.flagMask = tree.get<bool>("Output.Troubleshooting.FlagMask", parameters.flagMask);
	parameters.searchRegionScale = ReadParameter(tree, "Output.Troubleshooting.SearchRegionScale", gt0, "is not >0", optional<double>(parameters.searchRegionScale));

    auto gt2=bind(greater<double>(),_1,2);
	parameters.subpixelRefinementWindowSize = ReadParameter(tree, "Output.Troubleshooting.SubpixelRefinementWindowSize", gt2, "is not >2", optional<double>(parameters.subpixelRefinementWindowSize) );

	return true;
}	//bool SetParameters(const ptree& tree)


/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppNodalCameraTrackerImplementationC::Run()
{
	TimePointT t0=high_resolution_clock::now();

	//Network connection
	BrainstormRemoteMapsC remoteMapsConverter; //Remote maps protocol for Infinity Set
	bool flagUDP=!parameters.endpointIP.empty();	//If true, UDP connection is active
	io_service ioService;	// IO service for the udp connection
	udp::socket socket(ioService);	//IO socket
	udp::endpoint endpoint;

	if(flagUDP)
	{
		socket.open(udp::v4());
		endpoint=udp::endpoint(address::from_string(parameters.endpointIP), parameters.endpointPort);
	}	//if(flagUDP)

	//Load the world data
	CameraC referenceCamera=LoadCamera();
	referenceCamera.Tag()=parameters.cameraId;

	vector<OrientedBinarySceneFeatureC> world=LoadWorld(referenceCamera);

	if(referenceCamera.Intrinsics() && referenceCamera.Intrinsics()->focalLength)
		parameters.trackerParameters.focalLength=*referenceCamera.Intrinsics()->focalLength;

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Loaded "<<world.size()<<" scene points. Initialising the tracker\n";

	//Initialisation
	Matrix3d normaliser=Initialise(referenceCamera);

	double framePeriod= 1e9/videoDevice.GetFrameRate();	//in nanoseconds
	double framePeriodMsec= 1e3/videoDevice.GetFrameRate();	//in miliseconds

	dImageG.download(hImageG[0]);	//Initialise the buffer
	hImageG[1]=hImageG[0].clone();

	//Initialise the tracker
	vector<GpuMat> dBuffer(2+ (parameters.videoConfig.downsamplingFactor==0 ? 0 : parameters.videoConfig.downsamplingFactor-1 ) );
	TrackerT tracker(world, parameters.trackerParameters);
	NodalCameraTrackingPipelineDiagnosticsC trackerDiagnosticsI;

	//Loop until the camera is successfully localised
	long long int cFrame=0;
	while(!trackerDiagnosticsI.flagSuccess)
	{
		vector<BinaryImageFeatureC> imageFeatures=ExtractORB(dBuffer, normaliser, GpuMat(), false, cFrame);
		trackerDiagnosticsI=Register(tracker, imageFeatures, cFrame);	//This also initialises the gpu and the buffers
		++cFrame;
	}

	//Build the mask
	bool flagRegistered=trackerDiagnosticsI.flagSuccess;    //True if the tracker state belongs to a call that resulted in a successful registration
	Mat mask;
	GpuMat dMask;
	TrackerT maskPropagator(world, parameters.trackerParameters);
	if(parameters.flagMask)
	    UpdateMask(mask,dMask, tracker.GetState(), world, referenceCamera, maskPropagator);

	//Loop state
	bool flagTerminate=false;
	TrackerT::TrackerStateC trackerState(tracker.GetState());	//State of the tracker=tracker.GetState();

	if(parameters.flagFilterMeasurements)
		ApplyFilter(trackerState, referenceCamera, trackerDiagnosticsI.nInliers);	//Initialise the filter

	vector<BinaryImageFeatureC> prevImageFeatureSet=ExtractORB(dBuffer, normaliser, dMask, flagRegistered && parameters.flagMask, cFrame);	//Initialise the feature buffer. Registration should follow within a few ms
	++cFrame;

	//Initialise the user interface
	if(parameters.flagDisplay)
	{
		namedWindow("Video feed");
		imshow("Video feed", hImageG[0]);
		waitKey(1);
	}

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Initialisation complete. Tracking..."<<"\n";

	//Futures wait on destruction, so in order to implement the frame drop behaviour correctly, they should be defined here
	future<NodalCameraTrackingPipelineDiagnosticsC> trackerHandle;
	future<vector<BinaryImageFeatureC> > orbHandle;

	//Tracker loop
	list< tuple<unsigned int, bool, TrackerT::TrackerStateC, NodalCameraTrackingPipelineDiagnosticsC, vector<nanoseconds> >> cameraTrack;

	//Wait until a new frame so that the iterations are aligned with the frame boundary
	while(!videoDevice.QueryNewFrame()){}

	TimePointT t1=high_resolution_clock::now();
	while(!flagTerminate)
	{
		TimePointT tLoopStart=high_resolution_clock::now();
		vector<nanoseconds> timingMonitor(3);

		//Launch the feature extraction thread on the gpu

        if(flagORBComplete)
            orbHandle=async(std::launch::async,&AppNodalCameraTrackerImplementationC::ExtractORB, this, ref(dBuffer), normaliser, cref(dMask), flagRegistered&&parameters.flagMask, cFrame);

		TrackerT::TrackerStateC currentState=tracker.GetState();

		//Launch the camera localisation thread on the cpu
		//The first iteration will fail as the buffer is not initialised yet!

		//If the last call is completed, launch a new one
		if(flagRegistrationComplete)
			trackerHandle=async(std::launch::async, &AppNodalCameraTrackerImplementationC::Register, this, std::ref(tracker), prevImageFeatureSet, cFrame);

		//Mask: This operation makes use of the free CPU time, while waiting for the asynchronous calls to complete. So, it has to be performed here
		if(parameters.flagMask && flagRegistered)
		    UpdateMask(mask,dMask, currentState, world, referenceCamera, maskPropagator);

		//Wait until there is still time for display
		sleep_until(tLoopStart + milliseconds((unsigned long long)(framePeriodMsec- parameters.displayCost-3) ) );

		//If there is a registration at this point, display
		bool flagReady=(lastCompletedFrameId==cFrame);	// Conditions:
																		//No ongoing Register calls- but the last call could be initiated by a previous frame!
																		//Valid handle: flagRegistrationComplete is set by a call in this iteration
		if(flagReady)
			timingMonitor[1]=nanoseconds((unsigned long long)logger["tR"]);

		optional<NodalCameraTrackingPipelineDiagnosticsC> trackerDiagnostics;
		milliseconds timeRemaining=milliseconds((unsigned int)framePeriodMsec)-duration_cast<milliseconds>(high_resolution_clock::now()-tLoopStart);
		if(flagReady && parameters.flagDisplay && timeRemaining > milliseconds(parameters.displayCost+2) )
		{
			TimePointT tDisplayStart=high_resolution_clock::now();

			//Display if the registration task is completed, even if it failed
			trackerDiagnostics=trackerHandle.get();	//This invalidates the future!
			Display(*trackerDiagnostics, world, tracker.GetState(), referenceCamera, cFrame);

			timingMonitor[2]=duration_cast<nanoseconds>(high_resolution_clock::now()-tDisplayStart);
		}	//if(flagReady && parameters.flagDisplay && timeRemaining > milliseconds(parameters.displayCost+2) )

		int pressedKey=waitKey(1);

		//Wait until ~1ms to the next frame
		sleep_until(tLoopStart + nanoseconds((unsigned long long)(framePeriod-1e6)) );

		//Do as little as possible after here, we have 1ms

		//If the future is not read yet, try again
		if(!trackerDiagnostics)
		{
			flagReady=(lastCompletedFrameId==cFrame);
			trackerDiagnostics = flagReady ? trackerHandle.get() : NodalCameraTrackingPipelineDiagnosticsC();
		}	//if(!trackerDiagnostics)

		flagRegistered = trackerDiagnostics && trackerDiagnostics->flagSuccess;

		trackerState= trackerDiagnostics->flagSuccess ? tracker.GetState() : tracker.PropagateState(trackerState);	//If no registration available, prediction
																										//Prediction does not update the internal tracker state, but even a late registration does!
																										//If the camera is lost, the tracker will keep relocalising, until it sees something familiar, and update the internal state. Then the tracking will begin
		//If the filter is active, smooth
		if(parameters.flagFilterMeasurements)
			trackerState=ApplyFilter(trackerState, referenceCamera, trackerDiagnostics->nInliers);

		//If the ORB features are ready, capture the time elapsed
		if(lastORBFrameId==cFrame)
		    timingMonitor[0]=nanoseconds((unsigned int)(logger["tF3"]*1e6));

		if(!parameters.cameraTrackFile.empty())
			cameraTrack.emplace_back(cFrame-2, trackerDiagnostics->flagSuccess, trackerState, *trackerDiagnostics, timingMonitor);	//If no registration available, pass the prediction instead

		if(flagUDP)
		{
			CameraC currentCamera(referenceCamera);
			currentCamera.Extrinsics()->orientation=trackerState.orientation;
			currentCamera.Intrinsics()->focalLength=trackerState.focalLength;
			vector<char> udpPacket=remoteMapsConverter.GenerateMessage(currentCamera, parameters.minRotation);
			socket.send_to(boost::asio::buffer(udpPacket, udpPacket.size()), endpoint);
		}

		//If the ORB features are ready, get them. Else, clear the feature buffer
		if(lastORBFrameId==cFrame)
	        prevImageFeatureSet=orbHandle.get();
		else
		    prevImageFeatureSet.clear();

		//End of the loop
		++cFrame;
		flagTerminate= (pressedKey==81 || pressedKey==113) || videoDevice.IsEoF();


		//Run the clock until the next frame
		if( (cFrame-1)%100000==0)   //cFrame>1 as the count starts from initialisation
			t1=high_resolution_clock::now();	//Reset epoch- in the unlikely event that the tracker runs until framePeriod*cFrame overflows

		sleep_until(t1 + nanoseconds((unsigned long long)( ((cFrame-1)%100000) *framePeriod)) );

	}	//while(!flagTerminate)(

	logger["FrameCount"]=cFrame-1;
	logger["ExecutionTime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Tracking finished. Saving the output..."<<"\n";

	//Output
	SaveOutput(cameraTrack, referenceCamera);
	SaveLog(cameraTrack);

	destroyAllWindows();
	return true;
}	//bool Run()

}	//AppNodalCameraTrackerN
}	//VisualMediaN

int main ( int argc, char* argv[] )
{
    std::string version("160928");
    std::string header("nodalCameraTracker: Tracks a nodal camera");

   return SeeSawN::ApplicationN::ApplicationC<VisualMediaN::AppNodalCameraTrackerN::AppNodalCameraTrackerImplementationC>::Run(argc, argv, header, version) ? EXIT_SUCCESS: EXIT_FAILURE;
}   //int main ( int argc, char* argv[] )
