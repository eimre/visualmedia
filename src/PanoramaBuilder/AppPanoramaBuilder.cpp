/**
 * @file AppPanoramaBuilder.cpp Implementation of panoramaBuilder
 * @author Evren Imre
 * @date 19 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <string>
#include <memory>
#include <tuple>
#include <map>
#include <vector>
#include <cstddef>
#include <random>
#include <functional>
#include <utility>
#include <algorithm>
#include <omp.h>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/density.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/weighted_density.hpp>
#include <SeeSaw/Modified/boost_dynamic_bitset.hpp>
#include <Eigen/Dense>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/features2d.hpp>
#include <SeeSaw/Application/Application.h>
#include <SeeSaw/ApplicationInterface/InterfaceUtility.h>
#include <SeeSaw/ApplicationInterface/InterfacePfMPipeline.h>
#include <SeeSaw/ApplicationInterface/InterfaceSparseUnitSphereReconstruction.h>
#include <SeeSaw/Elements/Feature.h>
#include <SeeSaw/Elements/GeometricEntity.h>
#include <SeeSaw/Elements/Coordinate.h>
#include <SeeSaw/Elements/Camera.h>
#include <SeeSaw/Metrics/Distance.h>
#include <SeeSaw/GeometryEstimationPipeline/PfMPipeline.h>
#include <SeeSaw/GeometryEstimationPipeline/SparseUnitSphereReconstructionPipeline.h>
#include <SeeSaw/Geometry/P4PSolver.h>
#include <SeeSaw/Geometry/Projection32.h>
#include <SeeSaw/UncertaintyEstimation/MultivariateGaussian.h>
#include <SeeSaw/Wrappers/BoostFilesystem.h>
#include <SeeSaw/IO/CameraIO.h>
#include <SeeSaw/IO/SceneFeatureIO.h>
#include <SeeSaw/IO/ArrayIO.h>
#include "../Utility/OCVFeatures2d.h"
#include "../Utility/VideoAppConfigInterface.h"
#include "../Utility/VideoInputDevice.h"



namespace VisualMediaN
{
namespace AppPanoramaBuilderN
{

using namespace SeeSawN::ApplicationN;

using std::string;
using std::unique_ptr;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::chrono::time_point;
using std::tuple;
using std::forward_as_tuple;
using std::tie;
using std::move;
using std::map;
using std::vector;
using std::size_t;
using std::mt19937_64;
using std::bind;
using std::greater;
using std::greater_equal;
using std::pair;
using std::minmax_element;
using std::prev;
using std::next;
using std::advance;
using boost::dynamic_bitset;
using boost::lexical_cast;
using boost::for_each;
using boost::accumulators::density;
using boost::accumulators::accumulator_set;
using boost::accumulators::features;
using Eigen::RowVector3d;
using cv::namedWindow;
using cv::waitKey;
using cv::destroyAllWindows;
using cv::VideoCapture;
using cv::VideoWriter;
using cv::Mat;
using cv::cuda::ORB;
using cv::cuda::GpuMat;
using cv::cuda::norm;
using cv::cuda::resize;
using cv::cuda::pyrDown;
using cv::cuda::cvtColor;
using cv::KeyPoint;
using cv::Ptr;
using cv::Feature2D;
using cv::noArray;
using cv::KAZE;
using cv::Size;
using cv::Matx33d;
using cv::threshold;
using cv::Scalar;
using cv::circle;
using cv::Point;
using cv::imwrite;
using cv::cornerSubPix;
using cv::TermCriteria;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::RotationMatrix3DT;
using SeeSawN::ElementsN::Homography2DT;
using SeeSawN::ElementsN::IntrinsicCalibrationMatrixT;
using SeeSawN::ElementsN::DecomposeCamera;
using SeeSawN::MetricsN::InverseHammingBIDConverterT;
using SeeSawN::GeometryN::PfMPipelineC;
using SeeSawN::GeometryN::PfMPipelineParametersC;
using SeeSawN::GeometryN::PfMPipelineDiagnosticsC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineParametersC;
using SeeSawN::GeometryN::SparseUnitSphereReconstructionPipelineDiagnosticsC;
using SeeSawN::GeometryN::P4PSolverC;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::WrappersN::IsWriteable;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::ION::ArrayIOC;
using VisualMediaN::UtilityN::ConvertToBinaryImageFeature;
using VisualMediaN::UtilityN::ConvertToImageFeature;
using VisualMediaN::UtilityN::KAZEParametersC;
using VisualMediaN::UtilityN::ORBParametersC;
using VisualMediaN::UtilityN::InterfaceKAZEC;
using VisualMediaN::UtilityN::InterfaceORBC;
using VisualMediaN::UtilityN::PerformSubpixelRefinement;
using VisualMediaN::UtilityN::VideoAppConfigC;
using VisualMediaN::UtilityN::VideoAppConfigInterfaceC;
using VisualMediaN::UtilityN::VideoInputDeviceC;
using VisualMediaN::UtilityN::ColourSpaceT;

//TODO Interface
//2. Ask the user to identify the dynamic elements. Remove the corresponding landmarks from the map
//Sequential operation was discussed, but not practical

/**
 * @brief Parameters for panoramaBuilder
 * @ingroup Application
 */
struct AppPanoramaBuilderParametersC
{
	unsigned int nThreads=1;	///< Number of threads available to the application. >0

	optional<unsigned int> seed;	///< RNG seed. If not defined, default seed

	ORBParametersC orbParameters;	///< Parameters for ORB
	KAZEParametersC kazeParameters;	///< Parameters for KAZE

	//PfM
	PfMPipelineParametersC pfmParameters;	///< PfM pipeline parameters

	//Model builder
	SparseUnitSphereReconstructionPipelineParametersC susParameters;	///< Sparse unit sphere reconstruction pipeline parameters

	//Input
	VideoAppConfigC videoConfig;	///< Video input configuration

	unsigned int captureDelay=2000;	///< Delay before the camera starts capturing
	unsigned int minSeparation=1000;	///< Minimum separation between consecutive keyframes, in ms
	double minIntensityChange=25;	///< A new keyframe is captures if the mean of the difference image between the current frame and the last keyframe is above this value. >=0
	unsigned int nKeyrames=20;	///< Number of keyframes to be captured

	//optional<double> focalLength;	///< Camera focal length, if known (and fixed)

	//Output
	string outputPath;	///< Output path
	string cameraFile;	///< Estimated cameras
	string pointCloudFile;	///< Point cloud
	string sceneFeatureFile;	///< Scene features
	string panoramaImage;	///< Panorama image
	string logFile;	///< Filename for the log file
	bool flagVerbose=true;  ///< Verbose output?

	string recordedFeedFile;  ///< Saves the incoming camera feed, if non-empty

	AppPanoramaBuilderParametersC()
	{
	    orbParameters.nFeatures=1000;
	    kazeParameters.threshold=0.001;

	    pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration=100000;    //Vision graph often deals with utterly hopeless pairs. No need to waste time on such low-inlier ratio cases
	    pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration=1000;
	    pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=0.5;
	    pfmParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=0.5;
	    pfmParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration=1000;

	    susParameters.matcherParameters.matcherParameters.nnParameters.similarityTestThreshold=8e-3;
	    susParameters.noiseVariance=1;
	    susParameters.flagCompact=true;
	    susParameters.panoramaParameters.maxAngularDeviation=0.0025;
	}
};	//struct AppPanoramaBuilderParametersC


/**
 * @brief Implementation of panoramaBuilder
 * @ingroup Application
 * @nosubgrouping
 */
class AppPanoramaBuilderImplementationC
{
	private:

		/** @name Configuration */ //@{
		unique_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppPanoramaBuilderParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logger

		VideoInputDeviceC videoDevice;	///< Video input device
		//@}

		/** @name Implementation details */ //@{

		static ptree PrunePfMTree(const ptree& src);	///< Removes the redundant elements from the PfM tree
		static ptree PruneSUSTree(const ptree& src);	///< Removes the redundant elements from the sparse unit sphere reconstruction tree
		static ptree InjectDefaultValues(const ptree& src, const AppPanoramaBuilderParametersC& defaultParameters);  ///< Injects the application-specific default parameters into the parameter tree

		typedef time_point<high_resolution_clock> TimePointT;	//Time point type

		typedef vector<KeyPoint> KeypointConainerT;	///< Keypoint container type
		typedef Mat DescriptorStackT;	///< Descriptor stack type
		typedef tuple<KeypointConainerT, DescriptorStackT> FeatureExtractorOutputT;	///< Output type for feature extractors

		void PreprocessImage(GpuMat& dImageG, Mat& hImageC, vector<GpuMat>& dImageBuffers, vector<Mat>& hImageBuffers);	///< Preprocesses an input image
		FeatureExtractorOutputT ExtractORB(GpuMat image) const;	///< Extracts ORB features
		FeatureExtractorOutputT ExtractKAZE(Mat image) const;	///< Extracts KAZE features
		tuple<vector<FeatureExtractorOutputT>, vector<FeatureExtractorOutputT>, vector<CameraC>, map<size_t, Mat> >  ProcessKeyframes();	///< Acquires and processes keyframes

		typedef vector<ImageFeatureC> ImageFeatureContainerT;	///< An image feature container
		typedef vector<ImageFeatureContainerT> ImageFeatureStackT;	///< A stack of image feature containers

		typedef vector<BinaryImageFeatureC> BinaryImageFeatureContainerT;	///< An image feature container
		typedef vector<BinaryImageFeatureContainerT> BinaryImageFeatureStackT;	///< A stack of image feature containers

		void MakeCompositeImage(Mat& composite, const vector<CameraMatrixT>& cameraList, const vector<Mat>& keyframeList, const vector<Coordinate3DT>& pointCloud, const FrameT& frame);	///< Makes a composite image from the calibrated keyframes

		typedef accumulator_set<double, features<boost::accumulators::tag::density>, unsigned int> HistogramT;
		HistogramT ComputeLandmarkDistribution(const vector<CameraMatrixT>& cameraList, const vector<OrientedBinarySceneFeatureC>& landmarks) const;   ///< Computes the landmark histogram across zoom levels
		void SaveOutput(const vector<CameraMatrixT>& cameraList, const CameraC& cameraTemplate, const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& worldMap );	///< Saves the output to disk
		void SaveLog(const PfMPipelineDiagnosticsC& pfmDiagnostics, const SparseUnitSphereReconstructionPipelineDiagnosticsC& susDiagnostics);	///< Saves the log

		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppPanoramaBuilderImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& treeO);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//class AppPanoramaBuilderImplementationC

/********** IMPLEMENTATION STARTS HERE **********/

/**
 * @brief Removes the redundant elements from the PfM tree
 * @param[in] src Source
 * @return Pruned tree
 */
ptree AppPanoramaBuilderImplementationC::PrunePfMTree(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	return output;
}	//ptree PrunePfMTree(const ptree& src) const

/**
 * @brief Removes the redundant elements from the sparse unit sphere reconstruction tree
 * @param[in] src Source
 * @return Pruned tree
 */
ptree AppPanoramaBuilderImplementationC::PruneSUSTree(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("FeatureMatching.Main.NoThreads"))
		output.get_child("FeatureMatching.Main").erase("NoThreads");

	return output;
}	//ptree PrunePfMTree(const ptree& src) const

/**
 * @brief Injects the application-specific default parameters into the parameter tree
 * @param[in] src Parameter tree
 * @param[in] defaultParameters Default parameters
 * @return Updated tree
 */
ptree AppPanoramaBuilderImplementationC::InjectDefaultValues(const ptree& src, const AppPanoramaBuilderParametersC& defaultParameters)
{
    ptree output(src);
    if(!src.get_child_optional("FeatureExtractors.ORB.NoFeatures"))
      output.put("FeatureExtractors.ORB.NoFeatures", defaultParameters.orbParameters.nFeatures);

    if(!src.get_child_optional("FeatureExtractors.KAZE.Threshold"))
      output.put("FeatureExtractors.KAZE.Threshold", defaultParameters.kazeParameters.threshold);

    if(!src.get_child_optional("CameraCalibration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.EligibilityRatio" ) )
        output.put("CameraCalibration.HypothesisGeneration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.EligibilityRatio", defaultParameters.pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio);

    if(!src.get_child_optional("CameraCalibration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration" ) )
        output.put("CameraCalibration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration", defaultParameters.pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration);

    if(!src.get_child_optional("CameraCalibration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MaxIteration" ) )
        output.put("CameraCalibration.VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MaxIteration", defaultParameters.pfmParameters.visionGraphParameters.geometryEstimationPipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.maxIteration);

    if(!src.get_child_optional("CameraCalibration.EstimateRefinement.GeometryEstimation.RANSAC.EligibilityRatio" ) )
        output.put("CameraCalibration.EstimateRefinement.GeometryEstimationPipeline.GeometryEstimation.RANSAC.EligibilityRatio", defaultParameters.pfmParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio);

    if(!src.get_child_optional("CameraCalibration.EstimateRefinement.GeometryEstimation.RANSAC.Stage1.MinIteration" ) )
        output.put("CameraCalibration.EstimateRefinement.GeometryEstimation.RANSAC.Stage1.MinIteration", defaultParameters.pfmParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration);

    if(!src.get_child_optional("ModelBuilder.Main.NoiseVariance"))
            output.put("ModelBuilder.Main.NoiseVariance", defaultParameters.susParameters.noiseVariance);

    if(!src.get_child_optional("ModelBuilder.Main.FlagCompact"))
        output.put("ModelBuilder.Main.FlagCompact", defaultParameters.susParameters.flagCompact);

    if(!src.get_child_optional("ModelBuilder.FeatureMatching.Matcher.kNN.MinSimilarity"))
        output.put("ModelBuilder.FeatureMatching.Matcher.kNN.MinSimilarity", defaultParameters.susParameters.matcherParameters.matcherParameters.nnParameters.similarityTestThreshold );

    if(!src.get_child_optional("ModelBuilder.PanoramaBuilder.Main.MaxAngularDeviation"))
        output.put("ModelBuilder.PanoramaBuilder.Main.MaxAngularDeviation", defaultParameters.susParameters.panoramaParameters.maxAngularDeviation );

    return output;
}

/**
 * @brief Preprocesses an input image
 * @param[out] dImageG Grayscale image on the device
 * @param[out] hImageC Colour image on the host. Can be a reference to \c currentImage
 * @param[in,out] dImageBuffers A vector of image buffers for intermediate results
 * @param[in, out] hImageBuffers Image buffers on the host. [Original; BGR]
 */
void AppPanoramaBuilderImplementationC::PreprocessImage(GpuMat& dImageG, Mat& hImageC, vector<GpuMat>& dImageBuffers, vector<Mat>& hImageBuffers)
{
	int colorConversionCode=videoDevice.GetBGRConversionCode();
	if(colorConversionCode!=-1)
		cv::cvtColor(hImageBuffers[0], hImageBuffers[1],colorConversionCode);
	else
		hImageBuffers[1]=hImageBuffers[0];

	dImageBuffers[0].upload(hImageBuffers[1]);

	//Path 1: No downsampling
	if(! (parameters.videoConfig.flagInterlaced || parameters.videoConfig.downsamplingFactor!=0) )
	{
		hImageC=hImageBuffers[1];
		cv::cuda::cvtColor(dImageBuffers[0], dImageG, cv::COLOR_BGR2GRAY);
		return;
	}	//if(! (parameters.flagInterlaced || parameters.downsamplingFactor!=0) )

	//Pick the even lines
	if(parameters.videoConfig.flagInterlaced)
		cv::cuda::resize(dImageBuffers[0], dImageBuffers[1], Size(), 1, 0.5, cv::INTER_NEAREST);
	else
		dImageBuffers[1]=dImageBuffers[0];

	//Downsample
	for(size_t c=0; c<parameters.videoConfig.downsamplingFactor; ++c)
		cv::cuda::pyrDown(dImageBuffers[c+1], dImageBuffers[c+2]);

	dImageBuffers.rbegin()->download(hImageC);

	//Grayscale conversion
	cv::cuda::cvtColor(*dImageBuffers.rbegin(), dImageG, cv::COLOR_BGR2GRAY);
}	//void PreprocessImage(GpuMat& dImageG, Mat& hImageC, const Mat& currentImage)

/**
 * @brief Extracts ORB features
 * @param[in] image Source image
 * @return Keypoints and descriptors
 */
auto AppPanoramaBuilderImplementationC::ExtractORB(GpuMat image) const -> FeatureExtractorOutputT
{
	//Initialise the feature extractor
	Ptr<ORB> pEngine=ORB::create(parameters.orbParameters.nFeatures, parameters.orbParameters.scaleFactor, parameters.orbParameters.nLevels, parameters.orbParameters.edgeThreshold, 0, parameters.orbParameters.WTA_K, parameters.orbParameters.scoreType, parameters.orbParameters.patchSize, parameters.orbParameters.fastThreshold);

	FeatureExtractorOutputT output;

	//Extract features
	GpuMat descriptors;
	pEngine->detectAndCompute(image, noArray(), get<KeypointConainerT>(output), descriptors, false);

	descriptors.download(get<DescriptorStackT>(output));

	return output;
}	//tuple<vector<KeyPoint>, Map> ExtractORB(const Mat& image)

/**
 * @brief Extracts KAZE features
 * @param[in] image Source image
 * @return Keypoints and descriptors
 */
auto  AppPanoramaBuilderImplementationC::ExtractKAZE(Mat image) const -> FeatureExtractorOutputT
{
	Ptr<KAZE> pEngine=cv::KAZE::create(parameters.kazeParameters.flagExtended, parameters.kazeParameters.flagUpright, parameters.kazeParameters.threshold, parameters.kazeParameters.nOctaves, parameters.kazeParameters.nOctaveLayers, parameters.kazeParameters.diffusivity);

	FeatureExtractorOutputT output;
	pEngine->detectAndCompute(image, noArray(), get<KeypointConainerT>(output), get<DescriptorStackT>(output), false);

	return output;
}	//tuple<vector<KeyPoint>, Mat> ExtractKAZE(const Mat& image)

/**
 * @brief Acquires and processes keyframes
 * @return ORB and KAZE features; camera template; keyframes
 */
auto AppPanoramaBuilderImplementationC::ProcessKeyframes() -> tuple<vector<FeatureExtractorOutputT>, vector<FeatureExtractorOutputT>, vector<CameraC>, map<size_t, Mat>  >
{
	//Window
	const char* windowName="Video Feed";
	namedWindow(windowName , cv::WINDOW_AUTOSIZE);

	//Initialise the video capture
	videoDevice.Initialise(parameters.videoConfig);

	//Initialise the video recorder
	VideoWriter recorder;
	if(!parameters.recordedFeedFile.empty())
	    recorder.open(parameters.outputPath+"/"+parameters.recordedFeedFile, VideoWriter::fourcc('H','2','6','4'), videoDevice.GetFrameRate(), Size(videoDevice.GetWidth(), videoDevice.GetHeight()) );

	//Known camera parameters
	CameraC defaultCamera;
	defaultCamera.FrameBox()=FrameT(Coordinate2DT(0,0), Coordinate2DT(videoDevice.GetWidth()-1, videoDevice.GetHeight()-1));
	defaultCamera.Intrinsics()=IntrinsicC();
	defaultCamera.Intrinsics()->principalPoint=defaultCamera.FrameBox()->center();
	if(defaultCamera.Intrinsics()->focalLength)
		defaultCamera.Intrinsics()->focalLength=*defaultCamera.Intrinsics()->focalLength;
    /*if(parameters.focalLength)
        defaultCamera.Intrinsics()->focalLength=*parameters.focalLength;
*/

	//Initialise the frame buffers
	vector<Mat> hFrameBuffers(2);	//Frame buffers on the host
	GpuMat gCurrentFrame;	//Current frame, grayscale, on GPU
	GpuMat gPrevKeyframe;	//Previous keyframe, grayscale, on GPU
	Mat hImageC;	//Preprocessed colour image on the host
	Mat hImageG;	//Processed grayscale image, on the host

	vector<GpuMat> dImageBuffers(2 + parameters.videoConfig.downsamplingFactor);

	videoDevice.GetFrame(hFrameBuffers[0]);
	PreprocessImage(gPrevKeyframe, hImageC, dImageBuffers, hFrameBuffers);

	if(recorder.isOpened()) //If we are recording the feed
        recorder<<hFrameBuffers[1]; //BGR, cannot record YUV

	size_t nPixels=hImageC.total();

	//Keyframe loop

	vector< FeatureExtractorOutputT > orbFeatures; orbFeatures.reserve(parameters.nKeyrames);
	vector< FeatureExtractorOutputT > kazeFeatures; kazeFeatures.reserve(parameters.nKeyrames);
	map<size_t, Mat> keyframeStack;

	unsigned int nCaptured=0;

	//Live video capture
	TimePointT tLastKeyframe=high_resolution_clock::now();	//Timepoint for the last keyframe
	TimePointT tLastStatic=tLastKeyframe;		//Timepoint for the last static frame
	TimePointT t0=tLastKeyframe;

	//Canned video file
	int fLastKeyframe=0;	//Current position in milliseconds
	int fLastStatic=fLastKeyframe;
	int f0=fLastKeyframe;

	double scaleX=pow(2, parameters.videoConfig.downsamplingFactor);
	double scaleY=pow(2, parameters.videoConfig.downsamplingFactor) * ( (parameters.videoConfig.flagInterlaced) ? 2:1);

	while(nCaptured < parameters.nKeyrames)
	{
		bool flagForceKeyframe = (nCaptured==0);

		//Capture
		videoDevice.GetFrame(hFrameBuffers[0]);

		PreprocessImage(gCurrentFrame, hImageC, dImageBuffers, hFrameBuffers);
		imshow(windowName, hImageC);

	    if(recorder.isOpened()) //If we are recording the feed
	        recorder<<hFrameBuffers[1]; //BGR, cannot record YUV

		//Capture a keyframe?
		TimePointT tNow=high_resolution_clock::now();
		int fNow=videoDevice.GetPosition();

		unsigned int d1 = videoDevice.IsLive() ? (duration_cast<milliseconds>(tNow-tLastKeyframe).count()) : fNow-fLastKeyframe;
		unsigned int d2 = videoDevice.IsLive() ? duration_cast<milliseconds>(tNow-t0).count() :fNow-f0;

		bool flagKeyframe= d1 > parameters.minSeparation;	//Time elapsed since the last keyframe
		flagKeyframe= flagKeyframe && (d2 > parameters.captureDelay);	//Time elapsed since the beginning of the process
		flagKeyframe = flagKeyframe || flagForceKeyframe;

		if(flagKeyframe)
		{
			//If we are here the only reason the flag could be reset is a static camera
			flagKeyframe = flagKeyframe && (cv::cuda::norm(gCurrentFrame, gPrevKeyframe, cv::NORM_L1)/nPixels > parameters.minIntensityChange);	//Minimum intensity change
			flagKeyframe = flagKeyframe || flagForceKeyframe;

			if(!flagKeyframe)
			{
				tLastStatic=high_resolution_clock::now();
				fLastStatic=videoDevice.GetPosition();
			}
			else
			{
				//If the camera started moving, the first few frames will exhibit motion blur. So wait for a bit
				unsigned int d3= videoDevice.IsLive() ? (duration_cast<milliseconds>(tNow-tLastStatic).count()) : fNow-fLastStatic;
				flagKeyframe = flagKeyframe && (d3 > parameters.minSeparation);
				flagKeyframe = flagKeyframe || flagForceKeyframe;
			}
		}	//if(flagKeyframe)

		if(flagKeyframe)
		{
			tLastKeyframe=tNow;
			fLastKeyframe=fNow;

			//ORB
			orbFeatures.push_back(ExtractORB(gCurrentFrame));

			//Subpixel refinement. ORB uses FAST corners
			gCurrentFrame.download(hImageG);
			get<KeypointConainerT>(orbFeatures[nCaptured])=PerformSubpixelRefinement(get<KeypointConainerT>(orbFeatures[nCaptured]), hImageG, Size(5,5), Size(-1,-1), TermCriteria(TermCriteria::MAX_ITER+TermCriteria::EPS, 5, 1e-2));

			for_each(get<KeypointConainerT>(orbFeatures[nCaptured]), [&](KeyPoint& current){current.pt.x*=scaleX; current.pt.y*=scaleY;} );	//Revert to the original coordinates

			//KAZE
			//KAZE interest points are not necessarily corners, so no refinement.
			kazeFeatures.push_back(ExtractKAZE(hImageC));
			for_each(get<KeypointConainerT>(kazeFeatures[nCaptured]), [&](KeyPoint& current){current.pt.x*=scaleX; current.pt.y*=scaleY;} );	//Revert to the original coordinates

			if(parameters.flagVerbose)
				cout<<"Frame "<<" "<<nCaptured<<"/"<<parameters.nKeyrames<<" ORB "<<get<vector<KeyPoint>>(orbFeatures[nCaptured]).size()<<" KAZE "<<get<0>(kazeFeatures[nCaptured]).size()<<" "<< (videoDevice.IsLive() ? (int)duration_cast<seconds>(tNow-t0).count() : fNow)<<"\n";

			gPrevKeyframe=gCurrentFrame.clone();	//Update the previous keyframe
			keyframeStack[nCaptured]=hFrameBuffers[1].clone();

			++nCaptured;
		}	//if(duration_cast<milliseconds>(tNow-tLastKeyframe).count()/1e-3 < parameters.delay)

		int pressedKey=waitKey(1);

		//Terminate the program
		if(pressedKey==81 || pressedKey==113)
		{
		    destroyAllWindows();
		    abort();
		}

		//We have enough keyframes
		if(pressedKey==87 || pressedKey==119)
		    break;
	}	//while(nCaptured < parameters.nKeyrames)

	destroyAllWindows();

	return make_tuple(move(orbFeatures), move(kazeFeatures), vector<CameraC>(nCaptured, defaultCamera), move(keyframeStack) );
}	//vector< tuple<vector<KeypointConainerT>, vector<KeypointConainerT> > > ProcessKeyframes() const

/**
 * @brief Makes a composite image from the calibrated keyframes
 * @param[out] composite Composite image
 * @param[in] cameraList Cameras
 * @param[in] keyframeList Keyframes
 * @param[in] pointCloud Point cloud
 * @param[in] frameBox Image frame
 */
void AppPanoramaBuilderImplementationC::MakeCompositeImage(Mat& composite, const vector<CameraMatrixT>& cameraList, const vector<Mat>& keyframeList, const vector<Coordinate3DT>& pointCloud, const FrameT& frameBox)
{
	//Reference camera as the mean of all cameras
	size_t nFrames=keyframeList.size();
	P4PSolverC::uncertainty_type sampleStatistics=P4PSolverC::ComputeSampleStatistics(cameraList, vector<double>(nFrames, 1.0/nFrames), vector<double>(nFrames, 1.0/nFrames)  );
	CameraMatrixT referenceCamera=P4PSolverC::MakeModelFromMinimal(sampleStatistics.GetMean());

	//Compute the maps to the reference, and the convex hull for the composite image
	FrameT compositeFrame;
	vector<Homography2DT> homographies(nFrames);
	for(size_t c=0; c<nFrames; ++c)
	{
		homographies[c]=referenceCamera.leftCols(3)*cameraList[c].leftCols(3).inverse();

		compositeFrame.extend( (homographies[c]*(frameBox.corner(FrameT::CornerType::BottomLeft).homogeneous())).eval().hnormalized() );
		compositeFrame.extend( (homographies[c]*(frameBox.corner(FrameT::CornerType::BottomRight).homogeneous())).eval().hnormalized() );
		compositeFrame.extend( (homographies[c]*(frameBox.corner(FrameT::CornerType::TopLeft).homogeneous())).eval().hnormalized() );
		compositeFrame.extend( (homographies[c]*(frameBox.corner(FrameT::CornerType::TopRight).homogeneous())).eval().hnormalized() );
	}	//for(size_t c=0; c<nFrames; ++c)

	//Shift the homographies so that the images map into the composite frame
	double displayScale=0.25;
	Coordinate2DT offset=-compositeFrame.min();
	IntrinsicCalibrationMatrixT shifter=IntrinsicCalibrationMatrixT::Identity(); shifter.col(2).segment(0,2)=offset;
	shifter*=displayScale; shifter(2,2)=1;

	for(size_t c=0; c<nFrames; ++c)
		homographies[c] = shifter*homographies[c];

	//Warp the keyframes to the composite frame
	size_t width=ceil(compositeFrame.sizes()[0])*displayScale;
	size_t height=ceil(compositeFrame.sizes()[1])*displayScale;
	GpuMat dComposite(height, width, keyframeList[0].type(), cv::Scalar(0) );
	vector<GpuMat> dBuffers(4);

	for(size_t c=0; c<nFrames; ++c)
	{
		Matx33d mH; mH<<homographies[c](0,0), homographies[c](0,1), homographies[c](0,2),
						 homographies[c](1,0), homographies[c](1,1), homographies[c](1,2),
						 homographies[c](2,0), homographies[c](2,1), homographies[c](2,2);

		dBuffers[0].upload(keyframeList[c]);
		cv::cuda::warpPerspective(dBuffers[0], dBuffers[1], mH, dComposite.size());
		cv::cuda::cvtColor(dBuffers[1], dBuffers[2], cv::COLOR_BGR2GRAY);
		cv::cuda::threshold(dBuffers[2], dBuffers[3], 1, 255, 0);
		dBuffers[1].copyTo(dComposite, dBuffers[3]);
	}	//for(size_t c=0; c<nFrames; ++c)

	dComposite.download(composite);

	//Project the landmarks to the composite image
	Projection32C projector(shifter*referenceCamera);	//Apply the image plane shift to the reference camera as well
	vector<Coordinate2DT> projected; projected.reserve(pointCloud.size());
	for_each(pointCloud, [&](const Coordinate3DT current){projected.push_back(projector(current));} );

	for(const auto& current : projected)
		circle(composite, Point(current[0], current[1]), 5, Scalar(0,255,0), 2, 8, 0);
}	//void MakeCompositeImage(Mat composite, const vector<CameraMatrixT>& cameraList, const vector<Mat>& keyframeList, const vector<Coordinate3DT>& pointCloud)

/**
 * @brief Computes the landmark histogram across zoom levels
 * @param cameraList Camera matrices for the keyframes
 * @param landmarks Landmarks
 * @return A histogram of keyframs and landmarks across zoom levels
 */
auto AppPanoramaBuilderImplementationC::ComputeLandmarkDistribution(const vector<CameraMatrixT>& cameraList, const vector<OrientedBinarySceneFeatureC>& landmarks) const -> HistogramT
{
    //Extract the zoom values

    size_t nKeyframes=cameraList.size();
    vector<double> fList; fList.reserve(nKeyframes);
    for_each(cameraList, [&](const CameraMatrixT& current){ fList.push_back(*CameraC(current).Intrinsics()->focalLength);} );

    auto fInterval=minmax_element(fList.begin(), fList.end());

    //Landmark count histogram
    vector<unsigned int> landmarkHistogram(nKeyframes, 0);
    for(const auto& currentLandmark : landmarks)
        for(const auto& current : currentLandmark.RoI())
            ++landmarkHistogram[current];

    //Zoom support histogram
    size_t nBins=max(1, (int)ceil((*fInterval.second - *fInterval.first)/(*fInterval.first * 0.1))); //Each bin represents a 10% increase in zoom
    HistogramT output(boost::accumulators::tag::density::num_bins=nBins, boost::accumulators::tag::density::cache_size=nKeyframes);
    for(size_t c=0; c<nKeyframes; ++c)
        output(fList[c]/(*fInterval.first), boost::accumulators::weight=landmarkHistogram[c]);

    return output;
}   //map<double, pair<unsigned int, unsigned int> > ComputeLandmarkDistribution(const vector<CameraMatrixT>& cameraList, const vector<OrientedBinarySceneFeatureC>& landmarks)s

/**
 * @brief Saves the output to disk
 * @param[in] cameraList Estimated cameras
 * @param[in] cameraTemplate Common camera parameters
 * @param[in] pointCloud Unit sphere, as a point cloud
 * @param[in] worldMap Unit sphere, with appearance descriptors
 */
void AppPanoramaBuilderImplementationC::SaveOutput(const vector<CameraMatrixT>& cameraList, const CameraC& cameraTemplate, const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& worldMap )
{
	if(!parameters.cameraFile.empty())
	{
		vector<CameraC> cameras; cameras.reserve(cameraList.size());
		for(const auto& current : cameraList)
		{
			Coordinate3DT vC;
			RotationMatrix3DT mR;
			IntrinsicCalibrationMatrixT mK;
			tie(mK, vC, mR)=DecomposeCamera(current);

			CameraC currentCamera(cameraTemplate);
			currentCamera.SetExtrinsics(vC, mR);
			currentCamera.SetIntrinsics(mK);

			cameras.push_back(currentCamera);
		}	//for(const auto& current : cameraList)

		CameraIOC::WriteCameraFile(cameras, parameters.outputPath+parameters.cameraFile);
	}	//if(!parameters.cameraFile.empty())

	if(!parameters.sceneFeatureFile.empty())
		OrientedBinarySceneFeatureIOC::WriteFeatureFile(worldMap, parameters.outputPath+parameters.sceneFeatureFile);

	if(!parameters.pointCloudFile.empty())
	{
		string filename=parameters.outputPath+"/"+parameters.pointCloudFile;
		ofstream pointCloudFile(filename);

		if(pointCloudFile.fail())
			throw(runtime_error(string("AppPanoramaBuilderImplementationC::SaveOutput : Failed to open the file for writing at ")+filename));

		for(const auto& current : pointCloud)
			ArrayIOC<RowVector3d>::WriteArray(pointCloudFile, current.transpose());
	}	//if(parameters.pointCloudFile)

}	//void SaveOutput(const vector<CameraMatrixT>& cameraList, const CameraC& cameraTemplate, const vector<Coordinate3DT>& pointCloud, const vector<OrientedBinarySceneFeatureC>& worldMap )

/**
 * @brief Saves the log
 * @param[in] pfmDiagnostics PfM diagnostics object
 * @param[in] susDiagnostics Sparse unit sphere reconstruction diagnostics object
 */
void AppPanoramaBuilderImplementationC::SaveLog(const PfMPipelineDiagnosticsC& pfmDiagnostics, const SparseUnitSphereReconstructionPipelineDiagnosticsC& susDiagnostics)
{
	if(parameters.logFile.empty())
		return;

	string filename=parameters.outputPath+"/"+parameters.logFile;
	ofstream logFile(filename);

	if(logFile.fail())
		throw(runtime_error(string("AppPanoramaBuilderImplementationC::SaveLog : Failed to open the file for writing at ")+filename));

	string timeString= to_simple_string(second_clock::universal_time());
	logFile<<"panoramaBuilder log file created on "<<timeString<<"\n";

	logFile<<"Number of keyframes: "<<parameters.nKeyrames<<"\n";
	logFile<<"Number of registered keyframes: "<<pfmDiagnostics.nRegistered<<"\n";
	logFile<<"Number of inlier edges on the vision graph: "<<pfmDiagnostics.nInlierEdge<<"\n";
	logFile<<"Number of points on the unit sphere: "<<susDiagnostics.panoramaDiagnostics.nPoints<<"\n";
	logFile<<"Number of observed correspondences: "<<susDiagnostics.matcherDiagnostics.nObserved<<"\n";
	logFile<<"Number of inferred correspondences: "<<susDiagnostics.matcherDiagnostics.nImplied<<"\n";
	logFile<<"Execution time: "<<logger["ExecutionTime"]<<"s\n";

}	//void SaveLog()

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppPanoramaBuilderImplementationC::AppPanoramaBuilderImplementationC()
{
	AppPanoramaBuilderParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
}	//AppSparseUnitSphereBuilderImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppPanoramaBuilderImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppPanoramaBuilderImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppPanoramaBuilderParametersC defaultParameters;

	tree.put("xmlcomment", "panoramaBuilder configuration file");

	tree.put("General","");
	tree.put("FeatureExtractors","");
	tree.put("FeatureExtractors.ORB","");
	tree.put("FeatureExtractors.KAZE","");
	tree.put("CameraCalibration","");
	tree.put("ModelBuilder","");
	tree.put("Video","");
	tree.put("Output","");
	tree.put("Output.Troubleshooting","");

	if(detailLevel>0)
	{
		tree.put("General.NoThreads", defaultParameters.nThreads);
		tree.put("General.Seed", 0);

		tree.put("Input.CaptureDelay", defaultParameters.captureDelay);
		tree.put("Input.MinSeparation", defaultParameters.minSeparation);
		tree.put("Input.MinIntensityChange", defaultParameters.minIntensityChange);
		tree.put("Input.NoKeyrames", defaultParameters.nKeyrames);

		tree.put("Output.Root", "/home/");
		tree.put("Output.CameraFile", "camera.cam");
		tree.put("Output.PointCloud","PointCloud.asc");
		tree.put("Output.SceneFeatures","SceneFeatures.bft3");
		tree.put("Output.PanoramaImage", "panorama.png");
		tree.put("Output.Log","log.log");
		tree.put("Output.Verbose", defaultParameters.flagVerbose);

		tree.put("Output.Troubleshooting.RecordedFeedFile", defaultParameters.recordedFeedFile);
	}	//if(detailLevel>0)
/*
	if(detailLevel>1)
	    tree.put("Input.FocalLength", 1700);    //Optional, just for illustration purposes
*/

	VideoAppConfigInterfaceC videoAppInterface;
	tree.put_child("Input.Video", videoAppInterface.MakeParameterTree(VideoAppConfigC(), detailLevel));


	tree.put_child("FeatureExtractors.ORB", InterfaceORBC::MakeParameterTree(defaultParameters.orbParameters, detailLevel));
	tree.put_child("FeatureExtractors.KAZE", InterfaceKAZEC::MakeParameterTree(defaultParameters.kazeParameters, detailLevel));

	ptree pfmTree=PrunePfMTree(InterfacePfMPipelineC::MakeParameterTree(defaultParameters.pfmParameters, detailLevel));


    if(!(detailLevel>1))
        if(pfmTree.get_child_optional("VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            pfmTree.get_child("VisionGraphPipeline.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

    if(!(detailLevel>1))
        if(pfmTree.get_child_optional("EstimateRefinement.RANSAC.Stage2.MORANSAC"))
            pfmTree.get_child("EstimateRefinement.RANSAC.Stage2").erase("MORANSAC");

    tree.put_child("CameraCalibration", pfmTree);

	ptree susTree=PruneSUSTree(InterfaceSparseUnitSphereReconstructionC::MakeParameterTree(defaultParameters.susParameters, detailLevel));
	tree.put_child("ModelBuilder", susTree);

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] treeO Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppPanoramaBuilderImplementationC::SetParameters(const ptree& treeO)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	ptree tree=InjectDefaultValues(treeO, parameters);
	//General

	parameters.nThreads=ReadParameter(tree, "General.NoThreads", gt0, "is not positive", optional<double>(parameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	if(tree.get_optional<unsigned int>("General.Seed"))
		parameters.seed=ReadParameter(tree, "General.Seed", geq0, "is not >=0", optional<double>(0));

	//Video

	parameters.captureDelay=ReadParameter(tree, "Input.CaptureDelay", geq0, "is not >=0", optional<double>(parameters.captureDelay));
	parameters.minSeparation=ReadParameter(tree, "Input.MinSeparation", geq0, "is not >=0", optional<double>(parameters.minSeparation));
	parameters.minIntensityChange=ReadParameter(tree, "Input.MinIntensityChange", geq0, "is not >=0", optional<double>(parameters.minIntensityChange));
	parameters.nKeyrames=ReadParameter(tree, "Input.NoKeyrames", geq0, "is not >=0", optional<double>(parameters.nKeyrames));
/*
	if(tree.get_optional<double>("Input.FocalLength"))
		parameters.focalLength=ReadParameter(tree, "Input.FocalLength", gt0, "is not positive", parameters.focalLength);
*/
	VideoAppConfigInterfaceC videoAppInterface;
	parameters.videoConfig=videoAppInterface.MakeParameterObject(tree.get_child("Input.Video"));

	//Feature extractors

	//ORB
	if(tree.get_child_optional("FeatureExtractors.ORB"))
		parameters.orbParameters=InterfaceORBC::MakeParameterObject(tree.get_child("FeatureExtractors.ORB"));

	if(tree.get_child_optional("FeatureExtractors.KAZE"))
		parameters.kazeParameters=InterfaceKAZEC::MakeParameterObject(tree.get_child("FeatureExtractors.KAZE"));

	//PfM
	if(tree.get_child_optional("CameraCalibration"))
		parameters.pfmParameters=InterfacePfMPipelineC::MakeParameterObject( PrunePfMTree(tree.get_child("CameraCalibration")));

	parameters.pfmParameters.nThreads=parameters.nThreads;


	//SuS
	if(tree.get_child_optional("ModelBuilder"))
		parameters.susParameters=InterfaceSparseUnitSphereReconstructionC::MakeParameterObject( PruneSUSTree(tree.get_child("ModelBuilder")));

	parameters.susParameters.matcherParameters.nThreads=parameters.nThreads;

	//Output

	parameters.outputPath=tree.get<string>("Output.Root");
	if(!IsWriteable(parameters.outputPath))
		throw(invalid_argument(string("AppPanoramaBuilderImplementationC::SetParameters : Output path is not writeable. Value=")+parameters.outputPath));

	parameters.cameraFile=tree.get<string>("Output.CameraFile");
	parameters.pointCloudFile=tree.get<string>("Output.PointCloud","");
	parameters.sceneFeatureFile=tree.get<string>("Output.SceneFeatures","");
	parameters.panoramaImage=tree.get<string>("Output.PanoramaImage","");
	parameters.logFile=tree.get<string>("Output.Log","");
	parameters.flagVerbose=tree.get<bool>("Output.Verbose", parameters.flagVerbose);

	parameters.recordedFeedFile = tree.get<string>("Output.Troubleshooting.RecordedFeedFile","");

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppPanoramaBuilderImplementationC::Run()
{
	TimePointT t0=high_resolution_clock::now();

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: Capturing keyframes..."<<"\n";

	//Keyframes
	vector<FeatureExtractorOutputT> orbOutput;
	vector<FeatureExtractorOutputT> kazeOutput;
	vector<CameraC> knownCameraParameters;
	map<size_t, Mat> keyframeStack;
	tie(orbOutput, kazeOutput, knownCameraParameters, keyframeStack)=ProcessKeyframes();

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: "<<orbOutput.size()<<" keyframes captured. Estimating the calibration parameters..."<<"\n";

	//Feature stack, SeeSaw format
    size_t nKeyframes=keyframeStack.size();
    ImageFeatureStackT kazeFeatureStack(nKeyframes);
    for(size_t c=0; c<nKeyframes; ++c)
        kazeFeatureStack[c]=ConvertToImageFeature(get<KeypointConainerT>(kazeOutput[c]), get<DescriptorStackT>(kazeOutput[c]), true);

	//Camera calibration
	mt19937_64 rng;
	if(parameters.seed)
		rng.seed(*parameters.seed);

	map<size_t, CameraMatrixT> cameraList;
	vector<Coordinate3DT> dummy;
	PfMPipelineDiagnosticsC pfmDiagnostics=PfMPipelineC::Run(cameraList, dummy, rng, kazeFeatureStack, knownCameraParameters, parameters.pfmParameters);

	if(!pfmDiagnostics.flagSuccess)
		throw(runtime_error("AppPanoramaBuilderImplementationC::Run: Camera calibration failed"));

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: "<<cameraList.size()<<" cameras calibrated. Building the world map..."<<"\n";

	//World builder
	size_t nCalibrated=cameraList.size();
	vector<CameraMatrixT> calibrated; calibrated.reserve(nCalibrated);
	BinaryImageFeatureStackT orbFeatureStack; orbFeatureStack.reserve(nCalibrated);
	vector<Mat> calibratedKeyframes; calibratedKeyframes.reserve(nCalibrated);
	for(const auto& current : cameraList)
	{
		calibrated.push_back(current.second);
		orbFeatureStack.emplace_back(ConvertToBinaryImageFeature(get<KeypointConainerT>(orbOutput[current.first]),get<DescriptorStackT>(orbOutput[current.first]),true));
		calibratedKeyframes.push_back(keyframeStack[current.first]);
	}	//for(const auto& current : cameraList)

	keyframeStack.clear();	//Release unused memory

	InverseHammingBIDConverterT similarityMetric;
	vector<Coordinate3DT> pointCloud;
	vector<OrientedBinarySceneFeatureC> sceneFeatures;
	SparseUnitSphereReconstructionPipelineDiagnosticsC susDiagnostics=SparseUnitSphereReconstructionPipelineC::Run(pointCloud, sceneFeatures, calibrated, orbFeatureStack, similarityMetric, parameters.susParameters);

	if(!susDiagnostics.flagSuccess)
		throw(runtime_error("AppPanoramaBuilderImplementationC::Run: World map construction failed"));

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9<<"s: "<<"Found "<<sceneFeatures.size()<<" points. Saving the output..."<<"\n";

	logger["ExecutionTime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	//Serialise the output
	SaveOutput(calibrated, knownCameraParameters[0], pointCloud, sceneFeatures);
	SaveLog(pfmDiagnostics, susDiagnostics);

	if(!parameters.panoramaImage.empty())
	{
		Mat compositeImage;
		FrameT imageFrame(Coordinate2DT(0,0), Coordinate2DT(calibratedKeyframes[0].cols-1, calibratedKeyframes[0].rows-1));
		MakeCompositeImage(compositeImage, calibrated, calibratedKeyframes, pointCloud, imageFrame);

		namedWindow("Composite Image" , cv::WINDOW_NORMAL);
		imshow("Composite Image", compositeImage);
		waitKey();

		imwrite(parameters.outputPath+parameters.panoramaImage, compositeImage);
	}	//if(!parameters.panoramaImage.empty())

	destroyAllWindows();

	//TODO To be tidied up and saved with the log
	auto landmarkDistribution=density(ComputeLandmarkDistribution(calibrated, sceneFeatures));
	auto itE=prev(landmarkDistribution.end(),1);
	for(auto it=next(landmarkDistribution.begin(),1); it!=itE; advance(it,1))
	    if(it->second!=0)   //Only non-empty bins
	        std::cout<<"x"<<it->first<<"-x"<< next(it,1)->first<<" "<<it->second<<"\n";   //Bin boundaries; percentage

	return true;
}	//bool Run()

}	//namespace AppPanoramaBuilderN
}	//namespace VisualMediaN

int main ( int argc, char* argv[] )
{
    std::string version("160919");
    std::string header("panoramaBuilder: Builds a panoramic world map for nodal camera tracking");

   return SeeSawN::ApplicationN::ApplicationC<VisualMediaN::AppPanoramaBuilderN::AppPanoramaBuilderImplementationC>::Run(argc, argv, header, version) ? EXIT_SUCCESS: EXIT_FAILURE;
}   //int main ( int argc, char* argv[] )
