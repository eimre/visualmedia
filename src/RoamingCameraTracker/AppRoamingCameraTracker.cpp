/**
 * @file AppRoamingCameraTracker.cpp Implementation of roamingCameraTracker
 * @author Evren Imre
 * @date 28 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#ifdef _MSC_VER
#include <WinSock2.h>   //For Windows. This defines the interface keyword, which is required by "DeckLinkAPI.h" . Why panoramaBuilder and sceneModelBuilder3d do not need this, I have no idea
#endif


#include <iostream>
#include <string>
#include <memory>
#include <map>
#include <functional>
#include <chrono>
#include <future>
#include <thread>
#include <tuple>
#include <array>
#include <cmath>
#include <boost/optional.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/asio.hpp>
#include <boost/math/constants/constants.hpp>
#include <Eigen/Dense>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/highgui.hpp>
#include <SeeSaw/Application/Application.h>
#include <SeeSaw/ApplicationInterface/InterfaceUtility.h>
#include <SeeSaw/ApplicationInterface/InterfaceRoamingCameraTrackingPipeline.h>
#include <SeeSaw/Elements/Feature.h>
#include <SeeSaw/Elements/Camera.h>
#include <SeeSaw/GeometryEstimationPipeline/RoamingCameraTrackingPipeline.h>
#include <SeeSaw/GeometryEstimationComponents/P3PComponents.h>
#include <SeeSaw/GeometryEstimationComponents/P4PComponents.h>
#include <SeeSaw/Geometry/Projection32.h>
#include <SeeSaw/Matcher/SceneImageFeatureMatchingProblem.h>
#include <SeeSaw/Metrics/GeometricConstraint.h>
#include <SeeSaw/Metrics/Distance.h>
#include <SeeSaw/IO/SceneFeatureIO.h>
#include <SeeSaw/IO/CameraIO.h>
#include <SeeSaw/Wrappers/BoostFilesystem.h>
#include "../Utility/OCVFeatures2d.h"
#include "../Utility/VideoAppConfigInterface.h"
#include "../Utility/VideoInputDevice.h"
#include "../Utility/BrainstormRemoteMaps.h"
#include "../Utility/PoseFilter.h"

namespace VisualMediaN
{
namespace AppRoamingCameraTrackerN
{

using namespace SeeSawN::ApplicationN;

using std::stringstream;
using std::string;
using std::unique_ptr;
using std::map;
using std::bind;
using std::greater;
using std::greater_equal;
using std::ref;
using std::cref;
using std::tuple;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::chrono::time_point;
using std::this_thread::sleep_for;
using std::this_thread::sleep_until;
using std::async;
using std::future;
using std::max;
using std::min;
using std::array;
using std::pair;
using boost::optional;
using boost::for_each;
using boost::lexical_cast;
using boost::asio::io_service;
using boost::asio::ip::udp;
using boost::asio::ip::address;
using boost::math::constants::pi;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using cv::VideoCapture;
using cv::VideoWriter;
using cv::Mat;
using cv::KeyPoint;
using cv::Ptr;
using cv::noArray;
using cv::imshow;
using cv::namedWindow;
using cv::waitKey;
using cv::destroyAllWindows;
using cv::Size;
using cv::putText;
using cv::Scalar;
using cv::circle;
using cv::rectangle;
using cv::line;
using cv::Point;
using cv::TermCriteria;
using cv::cuda::GpuMat;
using cv::cuda::ORB;
using cv::cuda::cvtColor;
using cv::cuda::pyrDown;
using SeeSawN::ION::CameraIOC;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::GeometryN::RoamingCameraTrackingPipelineC;
using SeeSawN::GeometryN::RoamingCameraTrackingPipelineDiagnosticsC;
using SeeSawN::GeometryN::RoamingCameraTrackingPipelineParametersC;
using SeeSawN::GeometryN::P3PEstimatorProblemT;
using SeeSawN::GeometryN::P4PEstimatorProblemT;
using SeeSawN::GeometryN::Projection32C;
using SeeSawN::GeometryN::P4PSolverC;
using SeeSawN::MatcherN::BinarySceneImageFeatureMatchingProblemC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ElementsN::OrientedBinarySceneFeatureC;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::FrameT;
using SeeSawN::ElementsN::IntrinsicC;
using SeeSawN::ElementsN::ExtrinsicC;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::ElementsN::Coordinate2DT;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::ComposeCameraMatrix;
using SeeSawN::ElementsN::ComputeFoV;
using SeeSawN::ION::OrientedBinarySceneFeatureIOC;
using SeeSawN::MetricsN::InverseHammingSceneImageFeatureDistanceConverterT;
using SeeSawN::MetricsN::ReprojectionErrorConstraintT;
using SeeSawN::WrappersN::IsWriteable;
using VisualMediaN::UtilityN::ORBParametersC;
using VisualMediaN::UtilityN::ConvertToBinaryImageFeature;
using VisualMediaN::UtilityN::InterfaceORBC;
using VisualMediaN::UtilityN::PerformSubpixelRefinement;
using VisualMediaN::UtilityN::VideoAppConfigC;
using VisualMediaN::UtilityN::VideoAppConfigInterfaceC;
using VisualMediaN::UtilityN::VideoInputDeviceC;
using VisualMediaN::UtilityN::BrainstormRemoteMapsC;
using VisualMediaN::UtilityN::PoseFilterC;

//TODO Camera tracker interface
//1. Dynamic objects: Dynamically create maps of dynamic regions- GPU. Temporarily remove landmarks in these areas
//2. If a landmark is consistently missed, check it less often, or temporarily disable it
//3. A visual way to monitor the calibration- say, overlay the camera image with mosaic?

/**
 * @brief Parameters for roamingCameraTracker
 * @ingroup Application
 */
struct AppRoamingCameraTrackerParametersC
{
	unsigned int nThreads=2;	///< Number of threads available to the application. >0
	bool flagFilterMeasurements=true;	///< If \c true, the measurements are filtered. Adds a delay of one frame

	ORBParametersC orbParameters;	///< Parameters for the feature extractor

	RoamingCameraTrackingPipelineParametersC trackerParameters;	///< Parameters for the camera tracker

	//Input
	VideoAppConfigC videoConfig;	///< Video input configuration
									///< videoConfig.downsamplingFactor must match that of the model builder

	string worldFile;	///< Filename for the world map
	string referenceCameraFile;	///< Filename for the reference camera, with the known camera parameters

	//Output

	bool flagDisplay=true;	///< If \c true, the interface is turned on
	unsigned int displayCost=5;	///< Cost of displaying the interface, in ms. The interface is skipped if there is not enough time

	string outputPath;	///< Root for the output directory
	string logFile;	///< Log
	string cameraTrackFile;	///< Camera track
	bool flagVerbose=true;	///< \c true if verbose mode is on

	string endpointIP;	///< IP of the endpoint device
	unsigned short endpointPort=65535;	///< Port no on the endpoint device

	string cameraId="1";   ///< Id of the camera

	string recordedFeedFile;  ///< Saves the incoming camera feed, if non-empty

	AppRoamingCameraTrackerParametersC()
	{
	    orbParameters.nFeatures=750;

	    trackerParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold=8e-3;
	    trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize12=3;
	    trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21=3;
	    trackerParameters.pipelineParameters.matcherParameters.flagBucketFilter=false;
	    trackerParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration=10;
	    trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio=0.5;
	    trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration=100;
	    trackerParameters.pipelineParameters.maxObservationDensity=1;
	    trackerParameters.pipelineParameters.maxIteration=3;
	}

};	//struct AppRoamingCameraTrackerParametersC

/**
 * @brief Implementation of roamingCameraTracker
 * @ingroup Application
 * @nosubgrouping
 */
class AppRoamingCameraTrackerImplementationC
{
	private:

		/** @name Configuration */ //@{
		unique_ptr<options_description> commandLineOptions; ///< Command line options
														  // Option description line cannot be set outside of the default constructor. That is why a pointer is needed

		AppRoamingCameraTrackerParametersC parameters;  ///< Application parameters
		//@}

		/** @name State */ //@{
		map<string, double> logger;	///< Logger

		Mat colourFrame;	///< Current colour frame, on the host
		Mat currentFrameBGR;   ///< Current BGR colour frame, on the host
		Mat grayscaleFrame;	///< Current grayscale frame, on the host
		array<Mat,2> hImageG;	///< Processed grayscale image buffer on the host
		GpuMat dImageG;	///< Grayscale image on the device
		GpuMat dDescriptors;	///< Feature descriptors, device
		Mat hDescriptors;	///< Feature descriptors, host

		bool flagRegistrationComplete=false;	///< Indicates whether the last registration task is completed
		long long int lastCompletedFrameId=-1;	///< Indicates the id of the last completed frame

		VideoInputDeviceC videoDevice;	///< Video input device
		VideoWriter recorder;   ///< Device recording the camera feed for troubleshooting

		PoseFilterC<P4PSolverC> poseFilter;	///< Pose filter
		//@}

		/** @name Implementation details */ //@{
		typedef time_point<high_resolution_clock> TimePointT;	///<Time point type

		static ptree PruneTrackerTree(const ptree& src);	///< Removes the redundant elements from the nodal camera tracker tree
	    static ptree InjectDefaultValues(const ptree& src, const AppRoamingCameraTrackerParametersC& defaultParameters);  ///< Injects the application-specific default parameters into the parameter tree

		CameraC LoadCamera() const;	///< Loads the reference camera
		Matrix3d Initialise(CameraC& referenceCamera);	///< Initialisation

		void ProcessFrame(vector<GpuMat>& dBuffer, Mat currentFrame);	///< Processes the current frame

		typedef vector<KeyPoint> KeypointConainerT;	///< Keypoint container type
		typedef Mat DescriptorStackT;	///< Descriptor stack type
		typedef tuple<KeypointConainerT, DescriptorStackT> FeatureExtractorOutputT;	///< Output type for feature extractors
		vector<BinaryImageFeatureC> ExtractORB(vector<GpuMat>& dBuffer, const Matrix3d& normaliser);	///< Extracts ORB features

		typedef BinarySceneImageFeatureMatchingProblemC<InverseHammingSceneImageFeatureDistanceConverterT, ReprojectionErrorConstraintT> MatchingProblemT;
		typedef RoamingCameraTrackingPipelineC<P3PEstimatorProblemT, P4PEstimatorProblemT, MatchingProblemT> TrackerT;
		RoamingCameraTrackingPipelineDiagnosticsC Register(TrackerT& tracker, vector<BinaryImageFeatureC> imageFeatureSet, unsigned int seed);	///< Registers the current frame to the world map

		void Display(const RoamingCameraTrackingPipelineDiagnosticsC& diagnostics, const vector<OrientedBinarySceneFeatureC>& world, const TrackerT::TrackerStateC& trackerState, const CameraC& referenceCamera, unsigned int cFrame);	///< Displays the current image and the diagnostics

		TrackerT::TrackerStateC ApplyFilter(const TrackerT::TrackerStateC& measurement, const CameraC& referenceCamera, unsigned int support);	///< Applies a smoothing filter to the measurements

		void SaveLog(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack );	///< Saves the application log
		void SaveOutput(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack, const CameraC& referenceCamera);	///< Saves the output
		//@}

	public:

		/**@name ApplicationImplementationConceptC interface */ //@{
		AppRoamingCameraTrackerImplementationC();    ///< Constructor
		const options_description& GetCommandLineOptions() const;   ///< Returns the command line options description
		static void GenerateConfigFile(const string& filename, int detailLevel);  ///< Generates a configuration file with default parameters
		bool SetParameters(const ptree& treeO);  ///< Sets and validates the application parameters
		bool Run(); ///< Runs the application
		//@}
};	//AppRoamingCameraTrackerImplementationC

/********** AppRoamingCameraTrackerImplementationC **********/

/**
 * @brief Removes the redundant elements from the nodal camera tracker tree
 * @param[in] src Source
 * @return Pruned tree
 */
ptree AppRoamingCameraTrackerImplementationC::PruneTrackerTree(const ptree& src)
{
	ptree output(src);

	if(src.get_child_optional("Main.NoThreads"))
		output.get_child("Main").erase("NoThreads");

	if(src.get_child_optional("Problem.FocalLength"))
		output.get_child("Problem").erase("FocalLength");

	if(src.get_child_optional("Problem.ImageDiagonal"))
		output.get_child("Problem").erase("ImageDiagonal");

	if(src.get_child_optional("Tracking.GeometryEstimationPipeline.Main.FlagCovariance"))
		output.get_child("Tracking.GeometryEstimationPipeline.Main.").erase("FlagCovariance");

	if(src.get_child_optional("Tracking.GeometryEstimationPipeline.CovarianceEstimation"))
		output.get_child("Tracking.GeometryEstimationPipeline").erase("CovarianceEstimation");

	return output;
}	//ptree PruneTrackerTree(const ptree& src)


/**
 * @brief Injects the application-specific default parameters into the parameter tree
 * @param[in] src Parameter tree
 * @param[in] defaultParameters Default parameters
 * @return Updated tree
 */
ptree AppRoamingCameraTrackerImplementationC::InjectDefaultValues(const ptree& src, const AppRoamingCameraTrackerParametersC& defaultParameters)
{
    ptree output(src);

    if(!src.get_child_optional("FeatureExtraction.NoFeatures"))
      output.put("FeatureExtraction.NoFeatures", defaultParameters.orbParameters.nFeatures);

    if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.kNN.MinSimilarity"))
      output.put("Tracking.GeometryEstimationPipeline.Matching.kNN.MinSimilarity", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.nnParameters.similarityTestThreshold);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality"))
      output.put("Tracking.GeometryEstimationPipeline.Matching.kNN.NeighbourhoodCardinality", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.nnParameters.neighbourhoodSize21);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable"))
      output.put("Tracking.GeometryEstimationPipeline.Matching.SpatialConsistency.Enable", defaultParameters.trackerParameters.pipelineParameters.matcherParameters.flagBucketFilter);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration"))
      output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.Refinement.Main.MaxIteration", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.pdlParameters.maxIteration);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio"))
      output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Main.EligibilityRatio", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.eligibilityRatio);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration"))
      output.put("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage1.MinIteration", defaultParameters.trackerParameters.pipelineParameters.geometryEstimatorParameters.ransacParameters.parameters1.minIteration);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Main.MaxIteration"))
       output.put("Tracking.GeometryEstimationPipeline.Main.MaxIteration", defaultParameters.trackerParameters.pipelineParameters.maxIteration);

  if(!src.get_child_optional("Tracking.GeometryEstimationPipeline.Decimation.MaxObservationDensity"))
      output.put("Tracking.GeometryEstimationPipeline.Decimation.MaxObservationDensity", defaultParameters.trackerParameters.pipelineParameters.maxObservationDensity);

   return output;
}   //ptree InjectDefaultValues(const ptree& src, const AppNodalCameraTrackerParametersC& defaultParameters)

/**
 * @brief Loads the reference camera
 * @return Reference camera
 */
CameraC AppRoamingCameraTrackerImplementationC::LoadCamera() const
{
	vector<CameraC> cameraList=CameraIOC::ReadCameraFile(parameters.referenceCameraFile);

	if(cameraList.size()!=1)
		throw(invalid_argument(string("AppRoamingCameraTrackerImplementationC::LoadCamera : The camera file must have exactly one element. Value=") + lexical_cast<string>(cameraList.size())) );

	if(!cameraList[0].Intrinsics())
		cameraList[0].Intrinsics()=IntrinsicC();

	return cameraList[0];
}	//CameraC LoadCamera()

/**
 * @brief Initialisation
 * @param[in, out] referenceCamera Reference camera
 * @remarks Starts the frame acquisition as well
 * @return Normaliser
 */
Matrix3d AppRoamingCameraTrackerImplementationC::Initialise(CameraC& referenceCamera)
{
	videoDevice.Initialise(parameters.videoConfig);

    //Initialise the video recorder
    if(!parameters.recordedFeedFile.empty())
        recorder.open(parameters.outputPath+"/"+parameters.recordedFeedFile, VideoWriter::fourcc('H','2','6','4'), videoDevice.GetFrameRate(), Size(videoDevice.GetWidth(), videoDevice.GetHeight()) );

	//Reference camera
	unsigned int height=videoDevice.GetHeight();
	unsigned int width=videoDevice.GetWidth();

	referenceCamera.FrameBox()=FrameT(Coordinate2DT(0,0), Coordinate2DT(width-1, height-1));

	if(!referenceCamera.Intrinsics())
		referenceCamera.Intrinsics()=IntrinsicC();

	if( !referenceCamera.Intrinsics()->principalPoint)	//If no principal point for the camera, set as the image centre
		referenceCamera.Intrinsics()->principalPoint=referenceCamera.FrameBox()->center();

	if(!referenceCamera.Extrinsics())
		referenceCamera.Extrinsics()=ExtrinsicC();

	//Parameters
	parameters.trackerParameters.imageDiagonal=hypot(height, width);

	//Normaliser
	Matrix3d normaliser=Matrix3d::Identity();

	normaliser(0,1)=referenceCamera.Intrinsics()->skewness;
	normaliser(1,1)*=referenceCamera.Intrinsics()->aspectRatio;
	normaliser.col(2).segment(0,2)=*referenceCamera.Intrinsics()->principalPoint;
	normaliser=normaliser.inverse();

	return normaliser;
}	//Matrix3d Initialise(CameraC& referenceCamera, const VideoCapture& captureDevice)

/**
 * @brief Processes the current frame
 * @param[in, out] dBuffer Buffers on the device
 * @param[in] currentFrame Current RGB frame
 */
void AppRoamingCameraTrackerImplementationC::ProcessFrame(vector<GpuMat>& dBuffer, Mat currentFrame)
{
    //Recording the feed?
    if(recorder.isOpened())
    {
        //BGR conversion
        int colorConversionCode=videoDevice.GetBGRConversionCode();
        if(colorConversionCode!=-1)
            cv::cvtColor(currentFrame, currentFrameBGR, colorConversionCode);
        else
            currentFrameBGR=currentFrame;

        //Record
        recorder<<currentFrameBGR;
    }   // if(recorder.isOpened())

	int grayscaleConversionCode=videoDevice.GetGrayscaleConversionCode();
	if(grayscaleConversionCode!=-1)
		cv::cvtColor(currentFrame, grayscaleFrame,grayscaleConversionCode);

	dBuffer[0].upload(grayscaleFrame);

	//Pick the even lines
	if(parameters.videoConfig.flagInterlaced)
		cv::cuda::resize(dBuffer[0], dBuffer[1], Size(), 1, 0.5, cv::INTER_NEAREST);
	else
		dBuffer[1]=dBuffer[0];

	//Downsample
	if(parameters.videoConfig.downsamplingFactor>0)
	{
		for(size_t c=0; c<parameters.videoConfig.downsamplingFactor-1; ++c)
			cv::cuda::pyrDown(dBuffer[c+1], dBuffer[c+2]);

		cv::cuda::pyrDown(*dBuffer.rbegin(), dImageG);
	}	//if(parameters.videoConfig.downsamplingFactor>0)
	else
		dImageG=dBuffer[1];

	//Update the grayscale image buffer on the host
	hImageG[1]=hImageG[0].clone();
	dImageG.download(hImageG[0]);
}	//void ProcessFrame(vector<GpuMat>& dBuffer, vector<Mat>& hBuffer, Mat currentFrame)

/**
 * @brief Extracts ORB features
 * @param[in] dBuffer Frame buffer, on the device
 * @param[in] normaliser Normalising matrix
 * @return Keypoints and descriptors
 */
vector<BinaryImageFeatureC> AppRoamingCameraTrackerImplementationC::ExtractORB(vector<GpuMat>& dBuffer, const Matrix3d& normaliser)
{
    TimePointT t0=high_resolution_clock::now();

    //Read the frame into the gpu
	videoDevice.GetFrame(colourFrame);

    if(recorder.isOpened())
        recorder<<colourFrame;

	ProcessFrame(dBuffer, colourFrame);

	//Initialise the feature extractor
	Ptr<ORB> pEngine=ORB::create(parameters.orbParameters.nFeatures, parameters.orbParameters.scaleFactor, parameters.orbParameters.nLevels, parameters.orbParameters.edgeThreshold, 0, parameters.orbParameters.WTA_K, parameters.orbParameters.scoreType, parameters.orbParameters.patchSize, parameters.orbParameters.fastThreshold);

	//Extract features
	vector<KeyPoint> keypoints;
	pEngine->detectAndCompute(dImageG, noArray(), keypoints, dDescriptors, false);
	dDescriptors.download(hDescriptors);

	keypoints=PerformSubpixelRefinement(keypoints, hImageG[0], Size(5,5), Size(-1,-1), TermCriteria(TermCriteria::MAX_ITER+TermCriteria::EPS, 5, 1e-2));

	double scaleX=pow(2, parameters.videoConfig.downsamplingFactor);
	double scaleY=pow(2, parameters.videoConfig.downsamplingFactor) * ( (parameters.videoConfig.flagInterlaced) ? 2:1);
	for_each(keypoints, [&](KeyPoint& current){current.pt.x*=scaleX; current.pt.y*=scaleY;});

	vector<BinaryImageFeatureC> featureList=ConvertToBinaryImageFeature(keypoints, hDescriptors, true);
	for_each(featureList, [&](BinaryImageFeatureC& current){current.Coordinate() = (normaliser* (current.Coordinate().homogeneous())).eval().hnormalized();} );

	logger["tF3"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e6;

	return featureList;
}	//tuple<vector<KeyPoint>, Map> ExtractORB(const Mat& image)

/**
 * @brief Registers the current frame to the world map
 * @param[in, out] tracker Tracker
 * @param[in] imageFeatureSet Features for the current image. Pass by value on purpose
 * @param[in] seed Seed for the RNG
 * @return Diagnostics object
 */
RoamingCameraTrackingPipelineDiagnosticsC AppRoamingCameraTrackerImplementationC::Register(TrackerT& tracker, vector<BinaryImageFeatureC> imageFeatureSet, unsigned int seed)
{
	flagRegistrationComplete=false;

	logger["tR"]=numeric_limits<double>::infinity();
	TimePointT t0=high_resolution_clock::now();

	TrackerT::rng_type rng(seed);

	RoamingCameraTrackingPipelineDiagnosticsC diagnostics=tracker.Update(rng, imageFeatureSet);
	flagRegistrationComplete=true;
	lastCompletedFrameId=seed;

	logger["tR"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e6;

	return diagnostics;
}	//RoamingCameraTrackingPipelineDiagnosticsC Register(const vector<BinaryImageFeatureC>& imageFeatureSet, const vector<OrientedBinarySceneFeatureC>& world, unsigned int seed)

/**
 * @brief Displays the current image and the diagnostics
 * @param[in] diagnostics Diagnostics
 * @param[in] world World map
 * @param[in] trackerState Tracker state
 * @param[in] referenceCamera Reference camera
 * @param[in] cFrame Frame id
 */
void AppRoamingCameraTrackerImplementationC::Display(const RoamingCameraTrackingPipelineDiagnosticsC& diagnostics, const vector<OrientedBinarySceneFeatureC>& world, const TrackerT::TrackerStateC& trackerState, const CameraC& referenceCamera, unsigned int cFrame)
{
	//Tracking mode
	Point modeCentre=Point(10,hImageG[1].rows-10);

	if(diagnostics.flagSuccess)
		circle(hImageG[1], modeCentre, 5, Scalar(255), (diagnostics.flagRelocalise ? 2:-1));
	else
		circle(hImageG[1], modeCentre, 5, Scalar(0), 2);

	//Tracking quality
	if(diagnostics.flagSuccess)
	{
		double meanError=sqrt(diagnostics.geDiagnostics.geometryEstimatorDiagnostics.rbegin()->error/diagnostics.nInliers);	//Pseudo-huber returns pixel^2
		if(diagnostics.flagPose)
			meanError*=trackerState.focalLength;

		double maxError=ReprojectionErrorConstraintT::error_type::ComputeOutlierThreshold(parameters.trackerParameters.inlierRejectionProbability, parameters.trackerParameters.imageNoiseVariance);

		unsigned int lineHeight=40;
		Point lowerLeft(modeCentre.x+10, modeCentre.y);
		Point upperRight(modeCentre.x+20, modeCentre.y - lineHeight*sqrt(meanError)/maxError);
		rectangle(hImageG[1], lowerLeft, upperRight, Scalar(255), -1);
		line(hImageG[1], Point(lowerLeft.x, modeCentre.y-lineHeight), Point(upperRight.x, modeCentre.y-lineHeight), Scalar(255), 2, 8);
	}	//if(diagnostics.flagSuccess)

	//Support
	stringstream displayString;
	displayString<<diagnostics.nInliers<<" "<<cFrame;
	putText(hImageG[1], displayString.str(), Point(modeCentre.x+30, modeCentre.y),cv:: FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255),1 );

	size_t nLandmarks=world.size();
    vector<bool> supporters(nLandmarks, false);   //If true, the landmark is detected as an inlier supporting the most recent registration
    for(const auto& current : diagnostics.supportSet)
        supporters[current.left]=true;

	//Camera matrix
	CameraC currentCamera(referenceCamera);

	currentCamera.Extrinsics()->position=trackerState.position;
	currentCamera.Extrinsics()->orientation=trackerState.orientation;
	currentCamera.Intrinsics()->focalLength=trackerState.focalLength;
	CameraMatrixT mP=*currentCamera.MakeCameraMatrix();

	//World
	double scaleX = 1.0/pow(2, parameters.videoConfig.downsamplingFactor);
	double scaleY = scaleX/(parameters.videoConfig.flagInterlaced ? 2:1);

	//Coordinate axis as the AR object as an indicator of the stability of the tracker
    auto a = *SeeSawN::GeometryN::CoordinateStatistics3DT::ComputeMeanAndStD(MakeCoordinateVector(world) );
    vector<Coordinate3DT> markers(7, get<0>(a));
    for(size_t c=0; c<3; ++c)
    {
        Coordinate3DT offset(0,0,0);
        offset[c]+=get<1>(a)[c]*0.2;
        markers[2*c+1]+=offset;
        markers[2*c+2]-=offset;
    }

	Projection32C projector(mP);

	for(size_t c=0; c<3; ++c)
	{
		Coordinate2DT projected1=projector(markers[2*c+1]);
		Coordinate2DT projected2=projector(markers[2*c+2]);
		line(hImageG[1], Point(projected1[0]*scaleX, projected1[1]*scaleY), Point(projected2[0]*scaleX, projected2[1]*scaleY), Scalar(255), 1 );
		circle(hImageG[1], Point(projected1[0]*scaleX, projected1[1]*scaleY), 1, Scalar(255), 1);
		circle(hImageG[1], Point(projected2[0]*scaleX, projected2[1]*scaleY), 1, Scalar(255), 1);
	}

	//If the support gets too low, draw a frame around the image
    if((float)diagnostics.nInliers/nLandmarks < 0.33)
        rectangle(hImageG[1], Point(0,0), Point(hImageG[1].cols-1, hImageG[1].rows-1), Scalar(255), 20);

	imshow("Video feed", hImageG[1]);
}	//void Display(const RoamingCameraTrackingPipelineDiagnosticsC& diagnostics)

/**
 * @brief Applies a smoothing filter to the measurements
 * @param[in] measurement Orientation measurement for the current frame
 * @param[in] referenceCamera Reference camera, holds the known camera parameters
 * @param[in] support Support for the measurement
 * @return Estimated orientation for the previous frame
 */
auto AppRoamingCameraTrackerImplementationC::ApplyFilter(const TrackerT::TrackerStateC& measurement, const CameraC& referenceCamera, unsigned int support) -> TrackerT::TrackerStateC
{
	//Convert to camera matrix
	CameraC currentCamera(referenceCamera);
	currentCamera.Extrinsics()->position=measurement.position;
	currentCamera.Extrinsics()->orientation=measurement.orientation;
	currentCamera.Intrinsics()->focalLength=measurement.focalLength;
	CameraMatrixT mP=*currentCamera.MakeCameraMatrix();

	//Filter
	CameraC filteredCamera(poseFilter.Apply(mP, support));

	TrackerT::TrackerStateC filteredState(measurement);
	filteredState.position = *filteredCamera.Extrinsics()->position;
	filteredState.orientation=*filteredCamera.Extrinsics()->orientation;
	filteredState.focalLength=*filteredCamera.Intrinsics()->focalLength;

	return filteredState;
}	//TrackerT::TrackerStateC ApplyFilter(const TrackerT::TrackerStateC& measurements, unsigned int support)

/**
 * @brief Saves the output
 * @param[in] cameraTrack Camera track
 * @param[in] referenceCamera Reference camera
 */
void AppRoamingCameraTrackerImplementationC::SaveOutput(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack, const CameraC& referenceCamera)
{
	if(parameters.cameraTrackFile.empty())
		return;

	vector<CameraC> output(cameraTrack.size(), referenceCamera);

	size_t index=0;
	for(const auto& current : cameraTrack)
	{
		output[index].Tag()=lexical_cast<string>(index);
		output[index].Extrinsics()->position = get<1>(current).position;
		output[index].Extrinsics()->orientation=get<1>(current).orientation;
		output[index].Intrinsics()->focalLength=get<1>(current).focalLength;
		++index;
	}	//for(const auto& current : cameraTrack)

	CameraIOC::WriteCameraFile(output, parameters.outputPath+parameters.cameraTrackFile);
}	//void SaveOutput(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC> >& cameraTrack)

/**
 * @brief Saves the application log
 * @param[in] cameraTrack Camera track
 */
void AppRoamingCameraTrackerImplementationC::SaveLog(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC, vector<nanoseconds>> >& cameraTrack )
{
	if(parameters.logFile.empty())
		return;

	ofstream logFile(parameters.outputPath+parameters.logFile);

	if(logFile.fail())
		throw(runtime_error("AppRoamingCameraTrackerImplementationC::SaveLog : Cannot open file at "+string(parameters.outputPath+parameters.logFile)));

	string timeString= to_simple_string(second_clock::universal_time());
	logFile<<"nodalCameraTracker log file created on "<<timeString<<"\n";

	size_t nSuccess=0;
	for(const auto& current : cameraTrack)
		if(get<0>(current))
			++nSuccess;

	logFile<<"#Tracked frames: "<<logger["FrameCount"]<<"\n";
	logFile<<"#Registered frames: "<<nSuccess<<"\n";
	logFile<<"Execution time: "<<logger["ExecutionTime"]<<"s \n";

	logFile<<"Frame  Registered? Relocalised?  Distance to origin    Z-deviation    Focal length    Support    Error    Features(msec) Registration(msec) Interface(msec) \n";

	size_t index=0;
	for(const auto& current : cameraTrack)
	{
		if(get<2>(current).nInliers>0 && get<2>(current).flagSuccess)
		{
			double meanError=sqrt(get<2>(current).geDiagnostics.geometryEstimatorDiagnostics.rbegin()->error/get<2>(current).nInliers);	//Pseudo-huber is pixel^2
			if(get<2>(current).flagPose)
				meanError*=get<1>(current).focalLength;

			logFile<<index<<" "<<get<0>(current)<<" "<<get<2>(current).flagRelocalise<<" "<<get<1>(current).position.norm()<<" "<<acos(get<1>(current).orientation.matrix()(2,2))<<" "<<get<1>(current).focalLength<<" "<<get<2>(current).nInliers<<" "<<meanError<<" ";
		}
		else	//Usually this means that the registration was not completed in time. So, no inlier-outlier classification available
			logFile<<index<<" "<<get<0>(current)<<" "<<get<2>(current).flagRelocalise<<" "<<get<1>(current).position.norm()<<" "<<acos(get<1>(current).orientation.matrix()(2,2))<<" "<<get<1>(current).focalLength<<" "<<0<<" "<<0<<" ";


		logFile<<get<3>(current)[0].count()/1e6<<" "<<get<3>(current)[1].count()<<" "<<get<3>(current)[2].count()/1e6<<"\n";
		++index;
	}

}	//void SaveLog(const list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC> >& cameraTrack )

/**
 * @brief Constructor
 * @remarks All values are string, as the options will be fed into a ptree
 */
AppRoamingCameraTrackerImplementationC::AppRoamingCameraTrackerImplementationC()
{
	AppRoamingCameraTrackerParametersC defaultParameters;

	commandLineOptions.reset(new(options_description)("Application command line options"));
}	//AppSparseUnitSphereBuilderImplementationC

/**
 * @brief Returns the command line options description
 * @return A constant reference to \c commandLineOptions
 */
const options_description& AppRoamingCameraTrackerImplementationC::GetCommandLineOptions() const
{
    return *commandLineOptions;
}   //const options_description& GetCommandLineOptions() const

/**
 * @brief Generates a configuration file with sensible parameters
 * @param[in] filename Filename
 * @param[in] detailLevel Detail level of the interface
 * @throws runtime_error If the write operation fails
 */
void AppRoamingCameraTrackerImplementationC::GenerateConfigFile(const string& filename, int detailLevel)
{
	ptree tree; //Options tree

	AppRoamingCameraTrackerParametersC defaultParameters;

	tree.put("General","");
	tree.put("FeatureExtraction","");
	tree.put("Tracking","");
	tree.put("Input","");
	tree.put("Input.Video","");
	tree.put("Output","");

	if(detailLevel>0)
	{
		tree.put("General.NoThreads", defaultParameters.nThreads);

		tree.put("Input.WorldFile", "/home/world.bft3");
		tree.put("Input.ReferenceCameraFile", "/home/reference.cam");

		tree.put("Output.FlagFilterMeasurements", defaultParameters.flagFilterMeasurements);
		tree.put("Output.FlagDisplay", defaultParameters.flagDisplay);
		tree.put("Output.DisplayCost", defaultParameters.displayCost);
		tree.put("Output.Root","/home/");
		tree.put("Output.Log", "log.txt");
		tree.put("Output.CameraTrack", "CameraTrack.cam");
		tree.put("Output.FlagVerbose", defaultParameters.flagVerbose);
		tree.put("Output.EndpointIP", defaultParameters.endpointIP);
		tree.put("Output.EndpointPort", defaultParameters.endpointPort);

		tree.put("Output.CameraId", defaultParameters.cameraId);

		tree.put("Output.Troubleshooting.RecordedFeedFile", defaultParameters.recordedFeedFile);
	}	//if(detailLevel>0)

	VideoAppConfigInterfaceC videoAppInterface;
	tree.put_child("Input.Video", videoAppInterface.MakeParameterTree(VideoAppConfigC(), detailLevel));

	tree.put_child("FeatureExtraction", InterfaceORBC::MakeParameterTree(defaultParameters.orbParameters, detailLevel));

	tree.put_child("Tracking", PruneTrackerTree(InterfaceRoamingCameraTrackingPipelineC::MakeParameterTree(defaultParameters.trackerParameters, detailLevel)));

    if(!(detailLevel>1))
        if(tree.get_child_optional("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2.MORANSAC"))
            tree.get_child("Tracking.GeometryEstimationPipeline.GeometryEstimation.RANSAC.Stage2").erase("MORANSAC");

	xml_writer_settings<ptree::key_type> settings(' ', 4);   //An indentation of 4 spaces
	write_xml(filename, tree, locale(), settings);
}	//void GenerateConfigFile(const string& filename, int detailLevel)

/**
 * @brief Sets and validates the application parameters
 * @param[in] treeO Parameters as a property tree
 * @return \c false if the parameter tree is invalid
 * @throws invalid_argument If any precoditions are violated, or the parameter list is incomplete
 * @post \c parameters is modified
 */
bool AppRoamingCameraTrackerImplementationC::SetParameters(const ptree& treeO)
{
	auto gt0=bind(greater<double>(),_1,0);
	auto geq0=bind(greater_equal<double>(),_1,0);

	ptree tree=InjectDefaultValues(treeO, parameters);

	parameters.nThreads=ReadParameter(tree, "General.NoThreads", gt0, "is not positive", optional<double>(parameters.nThreads));
	parameters.nThreads=min((int)parameters.nThreads, omp_get_max_threads());

	if(tree.get_child_optional("FeatureExtraction"))
		parameters.orbParameters=InterfaceORBC::MakeParameterObject(tree.get_child("FeatureExtraction"));

	if(tree.get_child_optional("Tracking"))
		parameters.trackerParameters=InterfaceRoamingCameraTrackingPipelineC::MakeParameterObject(PruneTrackerTree(tree.get_child("Tracking")));

	parameters.trackerParameters.nThreads=max((int)parameters.nThreads-2, 1);

	parameters.worldFile=tree.get<string>("Input.WorldFile", parameters.worldFile);
	parameters.referenceCameraFile=tree.get<string>("Input.ReferenceCameraFile", parameters.referenceCameraFile);

	VideoAppConfigInterfaceC videoAppInterface;
	parameters.videoConfig=videoAppInterface.MakeParameterObject(tree.get_child("Input.Video"));

	parameters.flagFilterMeasurements=tree.get<bool>("Output.FlagFilterMeasurements", parameters.flagFilterMeasurements);
	parameters.flagDisplay=tree.get<bool>("Output.FlagDisplay", parameters.flagDisplay);
	parameters.displayCost=ReadParameter(tree, "Output.DisplayCost", geq0, "is not >=0", optional<double>(parameters.displayCost));

	parameters.outputPath=tree.get<string>("Output.Root", parameters.outputPath);
	if(!IsWriteable(parameters.outputPath))
		throw(runtime_error(string("AppRoamingCameraTrackerImplementationC::SetParameters: Cannot write the path at ") + parameters.outputPath) );

	parameters.cameraTrackFile=tree.get<string>("Output.CameraTrack", parameters.cameraTrackFile);
	parameters.logFile=tree.get<string>("Output.Log", parameters.logFile);

	parameters.endpointIP=tree.get<string>("Output.EndpointIP", parameters.endpointIP);
	parameters.endpointPort=tree.get<unsigned short>("Output.EndpointPort", parameters.endpointPort);

	parameters.cameraId=tree.get<string>("Output.CameraId", parameters.cameraId);

	parameters.recordedFeedFile = tree.get<string>("Output.Troubleshooting.RecordedFeedFile","");

	return true;
}	//bool SetParameters(const ptree& tree)

/**
 * @brief Runs the application
 * @return \c true if the application is successful
 */
bool AppRoamingCameraTrackerImplementationC::Run()
{
	TimePointT t0=high_resolution_clock::now();

	//Network connection
	BrainstormRemoteMapsC remoteMapsConverter; //Remote maps protocol for Infinity Set
	bool flagUDP=!parameters.endpointIP.empty();	//If true, UDP connection is active
	io_service ioService;	// IO service for the udp connection
	udp::socket socket(ioService);	//IO socket
	udp::endpoint endpoint;

	if(flagUDP)
	{
		socket.open(udp::v4());
		endpoint=udp::endpoint(address::from_string(parameters.endpointIP), parameters.endpointPort);
	}	//if(flagUDP)

	//Load the world data
	CameraC referenceCamera=LoadCamera();
	referenceCamera.Tag()=parameters.cameraId;

	vector<OrientedBinarySceneFeatureC> world=OrientedBinarySceneFeatureIOC::ReadFeatureFile(parameters.worldFile, false);

	if(referenceCamera.Intrinsics() && referenceCamera.Intrinsics()->focalLength)
		parameters.trackerParameters.focalLength=*referenceCamera.Intrinsics()->focalLength;

	if(parameters.flagVerbose)
		cout<< duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Loaded "<<world.size()<<" scene points. Initialising the tracker\n";

	//Initialisation
	Matrix3d normaliser=Initialise(referenceCamera);

	double framePeriod= 1e9/videoDevice.GetFrameRate();	//in nanoseconds
	double framePeriodMsec= 1e3/videoDevice.GetFrameRate();	//in miliseconds

	dImageG.download(hImageG[0]);	//Initialise the buffer
	hImageG[1]=hImageG[0].clone();

	//Initialise the tracker
	vector<GpuMat> dBuffer(2+ (parameters.videoConfig.downsamplingFactor==0 ? 0 : parameters.videoConfig.downsamplingFactor-1 ) );
	TrackerT tracker(world, parameters.trackerParameters);
	RoamingCameraTrackingPipelineDiagnosticsC trackerDiagnosticsI;

	//Loop until the camera is successfully localised
	long long int cFrame=0;

	while(!trackerDiagnosticsI.flagSuccess)
	{
		vector<BinaryImageFeatureC> imageFeatures=ExtractORB(dBuffer, normaliser);
		trackerDiagnosticsI=Register(tracker, imageFeatures, cFrame);	//This also initialises the gpu and the buffers
		++cFrame;
	}

	//Loop state
	bool flagTerminate=false;
	TrackerT::TrackerStateC trackerState(tracker.GetState());	//State of the tracker=tracker.GetState();

	if(parameters.flagFilterMeasurements)
		ApplyFilter(trackerState, referenceCamera, trackerDiagnosticsI.nInliers);	//Initialise the filter

	vector<BinaryImageFeatureC> prevImageFeatureSet;//=ExtractORB(dBuffer, normaliser);	//Initialise the feature buffer. Registration should follow within a few ms

	//Initialise the user interface
	if(parameters.flagDisplay)
	{
		namedWindow("Video feed");
		imshow("Video feed", hImageG[0]);
		waitKey(1);
	}

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Initialisation complete. Tracking..."<<"\n";

	//Futures wait on destruction, so in order to implement the frame drop behaviour correctly, they should be defined here
	future<RoamingCameraTrackingPipelineDiagnosticsC> trackerHandle;

	//Tracker loop
	list< tuple<bool, TrackerT::TrackerStateC, RoamingCameraTrackingPipelineDiagnosticsC, vector<nanoseconds> >> cameraTrack;

    //Wait until a new frame so that the iterations are aligned with the frame boundary
    while(!videoDevice.QueryNewFrame()){}

	TimePointT t1=high_resolution_clock::now();
	while(!flagTerminate)
	{
		TimePointT tLoopStart=high_resolution_clock::now();
		vector<nanoseconds> timingMonitor(3);

		//Launch the feature extraction thread on the gpu
		future<vector<BinaryImageFeatureC> > orbHandle=async(std::launch::async,&AppRoamingCameraTrackerImplementationC::ExtractORB, this, ref(dBuffer), normaliser);

		//Launch the camera localisation thread on the cpu
        //The first iteration will fail as the buffer is not initialised yet!

		//If the last call is completed, launch a new one
		if(flagRegistrationComplete)
			trackerHandle=async(std::launch::async, &AppRoamingCameraTrackerImplementationC::Register, this, std::ref(tracker), move(prevImageFeatureSet), cFrame);

		//Wait for the feature extraction thread
		prevImageFeatureSet=orbHandle.get();	//Implicit assumption: feature extraction is always completed in time
		timingMonitor[0]=duration_cast<nanoseconds>(high_resolution_clock::now()-tLoopStart);

		//Wait until there is still time for display
		sleep_until(tLoopStart + milliseconds((unsigned long long)(framePeriodMsec- parameters.displayCost-3) ) );

		//If there is a registration at this point, display
		bool flagReady=(lastCompletedFrameId==cFrame);	// Conditions:
																		//No ongoing Register calls- but the last call could be initiated by a previous frame!
																		//Valid handle: flagRegistrationComplete is set by a call in this iteration
		if(flagReady)
			timingMonitor[1]=nanoseconds((unsigned long long)logger["tR"]);

		optional<RoamingCameraTrackingPipelineDiagnosticsC> trackerDiagnostics;
		milliseconds timeRemaining=milliseconds((unsigned int)framePeriodMsec)-duration_cast<milliseconds>(high_resolution_clock::now()-tLoopStart);
		if(flagReady && parameters.flagDisplay && timeRemaining > milliseconds(parameters.displayCost+2) )
		{
			TimePointT tDisplayStart=high_resolution_clock::now();

			//Display if the registration task is completed, even if it failed
			trackerDiagnostics=trackerHandle.get();	//This invalidates the future!

			Display(*trackerDiagnostics, world, tracker.GetState(), referenceCamera, cFrame);

			timingMonitor[2]=duration_cast<nanoseconds>(high_resolution_clock::now()-tDisplayStart);
		}	//if(flagReady && parameters.flagDisplay && timeRemaining > milliseconds(parameters.displayCost+2) )

		int pressedKey=waitKey(1);

		//Wait until ~1ms to the next frame
		sleep_until(tLoopStart + nanoseconds((unsigned long long)(framePeriod-1e6)) );

		//Do as little as possible after here, we have 1ms

		//If the future is not read yet, try again
		if(!trackerDiagnostics)
		{
			flagReady=(lastCompletedFrameId==cFrame);
			trackerDiagnostics = flagReady ? trackerHandle.get() : RoamingCameraTrackingPipelineDiagnosticsC();
		}	//if(!trackerDiagnostics)

		trackerState= trackerDiagnostics->flagSuccess ? tracker.GetState() : tracker.PropagateState(trackerState);	//If no registration available, prediction
																										//Prediction does not update the internal tracker state, but even a late registration does!
																										//If the camera is lost, the tracker will keep relocalising, until it sees something familiar, and update the internal state. Then the tracking will begin
		//If the filter is active, smooth
		if(parameters.flagFilterMeasurements)
			trackerState=ApplyFilter(trackerState, referenceCamera, trackerDiagnostics->nInliers);


		if(!parameters.cameraTrackFile.empty())
			cameraTrack.emplace_back(trackerDiagnostics->flagSuccess, trackerState, *trackerDiagnostics, timingMonitor);	//If no registration available, pass the prediction instead

		if(flagUDP)
		{
			CameraC currentCamera(referenceCamera);
			currentCamera.Extrinsics()->position=trackerState.position;
			currentCamera.Extrinsics()->orientation=trackerState.orientation;
			currentCamera.Intrinsics()->focalLength=trackerState.focalLength;
			vector<char> udpPacket=remoteMapsConverter.GenerateMessage(currentCamera);
			socket.send_to(boost::asio::buffer(udpPacket, udpPacket.size()), endpoint);
		}

		//End of the loop
		++cFrame;
		flagTerminate= (pressedKey==81 || pressedKey==113) || videoDevice.IsEoF();


		//Run the clock until the next frame
		if( (cFrame-1)%100000==0)   //cFrame>1 as the count starts with registration
			t1=high_resolution_clock::now();	//Reset epoch- in the unlikely event that the tracker runs until framePeriod*cFrame overflows

		sleep_until(t1 + nanoseconds((unsigned long long)( ((cFrame-1)%100000) *framePeriod)) );
	}	//while(!flagTerminate)(

	if(flagUDP)
		socket.close();

	logger["FrameCount"]=cFrame-1;
	logger["ExecutionTime"]=duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9;

	if(parameters.flagVerbose)
		cout<<duration_cast<nanoseconds>(high_resolution_clock::now()-t0).count()/1e9 <<"s: "<<"Tracking finished. Saving the output..."<<"\n";

	//Output
	SaveOutput(cameraTrack, referenceCamera);
	SaveLog(cameraTrack);

	destroyAllWindows();
	return true;
}	//bool Run()

}	//AppRoamingCameraTrackerN
}	//VisualMediaN

int main ( int argc, char* argv[] )
{
    std::string version("170128");
    std::string header("roamingCameraTracker: Tracks a nodal camera");

   return SeeSawN::ApplicationN::ApplicationC<VisualMediaN::AppRoamingCameraTrackerN::AppRoamingCameraTrackerImplementationC>::Run(argc, argv, header, version) ? EXIT_SUCCESS: EXIT_FAILURE;
}   //int main ( int argc, char* argv[] )
