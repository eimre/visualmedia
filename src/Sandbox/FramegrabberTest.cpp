/**
 * @file FramegrabberTest.cpp
 * @author Evren Imre
 * @date 24 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include <BlackmagicSDK/DeckLinkAPI.h>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <atomic>
#include <iostream>

class BlackmagicFrameGrabberCallbackC : public IDeckLinkInputCallback
{
	private:

		std::atomic<ULONG> refCount;

	public:

		BlackmagicFrameGrabberCallbackC();

		virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID *ppv) { return E_NOINTERFACE; }
		virtual ULONG STDMETHODCALLTYPE AddRef(void);
		virtual ULONG STDMETHODCALLTYPE  Release(void);
		virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents, IDeckLinkDisplayMode*, BMDDetectedVideoInputFormatFlags);
		virtual HRESULT VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame);	//Callback
};

class BlackmagicFrameGrabberWrapperC
{
	private:

		IDeckLinkIterator* deckLinkIterator=nullptr;
		IDeckLink* deckLink;
		IDeckLinkInput* deckLinkInput;
		BlackmagicFrameGrabberCallbackC* callbackObject;

	public:

		void Initialise();
		void Start();

		~BlackmagicFrameGrabberWrapperC();
};	//BlackmagicFrameGrabberWrapperC

/**********/

BlackmagicFrameGrabberCallbackC::BlackmagicFrameGrabberCallbackC() : refCount(1)
{
}

ULONG BlackmagicFrameGrabberCallbackC::AddRef(void)
{
//	return __sync_add_and_fetch(&refCount, 1);  //GCC specific
    return (++refCount);
}

ULONG BlackmagicFrameGrabberCallbackC::Release(void)
{
//	int32_t newRefValue = __sync_sub_and_fetch(&refCount, 1);   //GCC specific
    int32_t newRefValue = (--refCount);

	if (newRefValue == 0)
	{
		delete this;
		return 0;
	}
	return newRefValue;
}

HRESULT BlackmagicFrameGrabberCallbackC::VideoInputFormatChanged(BMDVideoInputFormatChangedEvents events, IDeckLinkDisplayMode *mode, BMDDetectedVideoInputFormatFlags formatFlags)
{
	throw("Undefined!");
}

HRESULT BlackmagicFrameGrabberCallbackC::VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame)
{
	if(!videoFrame)
		return S_FALSE;

	//Get the pointer to the data
	void* data;
	videoFrame->GetBytes(&data);

	//Move to an OpenCV matrix
	cv::Mat loadedImage;
	cv::Mat mat = cv::Mat(videoFrame->GetHeight(), videoFrame->GetWidth(), CV_8UC2, data, videoFrame->GetRowBytes());
	cv::cvtColor(mat, loadedImage, CV_YUV2BGR_UYVY);

	//Display
	cv::imshow("DEBUG", loadedImage);
	cv::waitKey(1);
	return S_OK;
}


void BlackmagicFrameGrabberWrapperC::Initialise()
{
#ifdef _MSC_VER
    CoInitialize(NULL);
    CoCreateInstance(CLSID_CDeckLinkIterator, NULL, CLSCTX_ALL, IID_IDeckLinkIterator, (void**)&deckLinkIterator);  //Windows interface
#else
	 deckLinkIterator = CreateDeckLinkIteratorInstance();
#endif
	 //Allocate the callback object
	 callbackObject = new BlackmagicFrameGrabberCallbackC();

	 //Get the DeckLink Inputs
	 deckLinkIterator->Next(&deckLink);
	 deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput);

	 //Register the callback object
	 deckLinkInput->SetCallback(callbackObject);

	 //Enable the video input with the selected format
	 deckLinkInput->EnableVideoInput(bmdModeHD1080p30, bmdFormat8BitYUV, bmdDisplayModeColorspaceRec709);

	 //Disable the audio
	 deckLinkInput->DisableAudioInput();
}

void BlackmagicFrameGrabberWrapperC::Start()
{
	deckLinkInput->StartStreams();
}


BlackmagicFrameGrabberWrapperC::~BlackmagicFrameGrabberWrapperC()
{
	deckLinkIterator->Release();
	deckLink->Release();
	deckLinkInput->Release();
	callbackObject->Release();
}

int main(int argc, char *argv[])
{
	BlackmagicFrameGrabberWrapperC inputDevice;
	inputDevice.Initialise();
	inputDevice.Start();

	while(true)
	{}
}
