/**
 * @file BrainstormRemoteMaps.cpp Implementation of the Brainstorm package generator
 * @author Evren Imre
 * @date 3 Apr 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "BrainstormRemoteMaps.h"


namespace VisualMediaN
{
namespace UtilityN
{

/**
 * @brief Validates a camera
 * @param camera
 * @throws invalid_argument If \c camera is missing any of the necessary components
 */
void BrainstormRemoteMapsC::Validate(const CameraC& camera)
{
	if(!camera.Extrinsics())
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing extrinsics") );

	if(!camera.Extrinsics()->position)
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing position") );

	if(!camera.Extrinsics()->orientation)
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing orientation") );

	if(!camera.Intrinsics())
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing intrinsics") );

	if(!camera.Intrinsics()->focalLength)
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing focal length") );

	if(!camera.FrameBox())
		throw(invalid_argument("BrainstormRemoteMapsC::Validate: Missing frame box") );
//REMOVEME BRA
if(!mBRA)
      mBRA=camera.Extrinsics()->orientation->matrix().inverse();
}	//void Validate(const CameraC& camera)

/**
 * @brief Generates a packet
 * @param[in] name Field name
 * @param[in] data Data
 * @param[in] packetType Packet type
 * @return String corresponding to the data packet
 */
vector<char> BrainstormRemoteMapsC::GeneratePacket(const string& name, const vector<float>& data, int packetType)
{
	ostringstream binaryStream;
	for(float value : data)
	{
		char* p=reinterpret_cast<char*>(&value);
		for(size_t c=0; c<sizeof(float); ++c)
			binaryStream<<p[c];
	}	//for(float value : data)

	string dataString=binaryStream.str();

	size_t packetSize=name.size() + dataString.size() + 4;
	vector<char> output(packetSize);

	output[0]=0xAA;
	output[1]=name.size();
	output[2]=packetType;
	output[3]=dataString.size();

	copy(name, next(output.begin(), 4));
	copy(dataString, next(output.begin(), 4+name.size()));

	return output;
}	//string GeneratePacket(const string& name, const vector<float>& data, int packetType)

/**
 * @brief Converts camera parameters to data items
 * @param[in] camera Camera to be converted
 * @return Data array. [Position; orientation; FoV and distortion]
 */
array<vector<float>,3> BrainstormRemoteMapsC::CameraToData(const CameraC& camera)
{
    //FIXME Figure out the transformation between BRA and UniS
	array<vector<float>,3> output;

	//Position
	//TODO Check whether this is correct
	output[0].resize(3);
	output[0][0]=(*camera.Extrinsics()->position)[0];
	output[0][1]=(*camera.Extrinsics()->position)[2];	//Y and Z axes are swapped
	output[0][2]=(*camera.Extrinsics()->position)[1];

	//Orientation
//	Matrix3d mR=camera.Extrinsics()->orientation->matrix(); //TODO Comment back in when REMOVE BRA is not active
Matrix3d mR= (camera.Extrinsics()->orientation->matrix()) * (*mBRA);    //REMOVEME BRA . The BRA kludge: The first frame defines the coordinate frame for BRA

	//Euler angles
	double cy= hypot(mR(1,1), mR(0,1));

	double h,p,r;
	if(cy > 16*numeric_limits<float>::epsilon())
	{
		h=atan2(mR(2,0), mR(2,2));
		p=atan2(-mR(2,1), cy);
		r=atan2(mR(0,1), mR(1,1));
	}	//	if(cy > 16*numeric_limits<float>::epsilon())
	else
	{
		h=atan2(-mR(0,2), mR(0,0));
		p=atan2(-mR(2,1), cy);
		r=0;
	}	//if(cy > 16*numeric_limits<float>::epsilon())

	output[1].resize(3);
	output[1][0] = -h*180/pi<float>();
	output[1][1] = p*180/pi<float>();
	output[1][2] = r*180/pi<float>();

	//Focal length
	output[2].resize(3);
	output[2][0]=ComputeFoV(camera.FrameBox()->sizes()[1], (*camera.Intrinsics()->focalLength))*180/pi<double>();
	output[2][1]=0;
	output[2][2]=0;

	return output;
}	//array<vector<float>,3> CameraToData(const CameraC& camera)

/**
 * @brief Generates a remote maps message for transmission
 * @param[in] camera Source
 * @param[in] minRotation If the camera rotation is below this value, it is ignored. Norm of the interframe rotation vector. Radian
 * @return Message corresponding to the camera
 */
vector< char> BrainstormRemoteMapsC::GenerateMessage(const CameraC& camera, float minRotation)
{
	//Validate the input
	Validate(camera);

	//Convert to data items
	array<vector<float>,3> dataItems=CameraToData(camera);

	//EXPERIMENTAL Static camera detection
	//FIXME This works only for the nodal case!
	bool flagStatic = false;
	if(previousDataFrame)
	    //Compare the incoming rotation with the previous
	    flagStatic = QuaternionToRotationVector( (*camera.Extrinsics()->orientation) * previousQ.inverse()).norm() < minRotation;

	if(flagStatic)
	    dataItems[1]=(*previousDataFrame)[1];
	else
	{
	    previousDataFrame=dataItems;
	    previousQ = *camera.Extrinsics()->orientation;
	}   //if(flagStatic)

	string cameraTag="cam"+camera.Tag()+"/";
	vector<string> names{"position", "orientation", "intrinsic"};
	for_each(names, [&](string& current){current = cameraTag+current;});    //So, the variable name format is camTAG/variable

	vector< vector<char> > packets(3);
	size_t messageSize=4;
	for(size_t c=0; c<3; ++c)
	{
		packets[c]=GeneratePacket(names[c], dataItems[c], 8);
		messageSize+=packets[c].size();
	}

	vector<char> output; output.reserve(messageSize);
	for(size_t c=0; c<3; ++c)
		copy(packets[c], back_inserter(output));

	output.push_back(0xAA);
	output.push_back(0x00);
	output.push_back(0x00);
	output.push_back(0x00);

	return output;
}	//string GenerateStream(const CameraC& camera)


}	//UtilityN
}	//VisualMediaN

