/**
 * @file BrainstormRemoteMaps.h Public interface for the Brainstorm package generator
 * @author Evren Imre
 * @date 3 Apr 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BRAINSTORM_REMOTE_MAPS_H_6681982
#define BRAINSTORM_REMOTE_MAPS_H_6681982

#include "BrainstormRemoteMaps.ipp"

namespace VisualMediaN
{
namespace UtilityN
{

class BrainstormRemoteMapsC;	///< Remote map package generator

}	//UtilityN
}	//VisualMediaN




#endif /* BRAINSTORM_REMOTE_MAPS_H_6681982 */
