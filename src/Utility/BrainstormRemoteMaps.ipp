/**
 * @file BrainstormRemoteMaps.ipp Implementation details for the Brainstorm package generator
 * @author Evren Imre
 * @date 3 Apr 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef BRAINSTORM_REMOTE_MAPS_IPP_6681982
#define BRAINSTORM_REMOTE_MAPS_IPP_6681982

#include <string>
#include <vector>
#include <tuple>
#include <stdexcept>
#include <cmath>
#include <limits>
#include <memory>
#include <cstddef>
#include <sstream>
#include <boost/math/constants/constants.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <Eigen/Dense>
#include <SeeSaw/Elements/GeometricEntity.h>
#include <SeeSaw/Elements/Camera.h>
#include <SeeSaw/Geometry/Rotation.h>

namespace VisualMediaN
{
namespace UtilityN
{

using std::string;
using std::hypot;
using std::atan2;
using std::fabs;
using std::vector;
using std::array;
using std::invalid_argument;
using std::numeric_limits;
using std::next;
using std::back_inserter;
using std::size_t;
using std::ostringstream;
using boost::math::constants::pi;
using boost::copy;
using boost::for_each;
using boost::lexical_cast;
using boost::optional;
using Eigen::Matrix3d;
using SeeSawN::ElementsN::Coordinate3DT;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::ElementsN::CameraC;
using SeeSawN::ElementsN::ComputeFoV;
using SeeSawN::ElementsN::QuaternionT;
using SeeSawN::GeometryN::QuaternionToRotationVector;

/**
 * @brief Remote maps package generator
 * @remarks The implementation of the protocol is based on D4.2
 * @remarks
 * @ingroup IO
 */
class BrainstormRemoteMapsC
{
	private:

        //FIXME Need to do the same for the moving camera!
//The first frame defines the reference frame in Infinity Set. This sidesteps the requirement that the world model is registered to Infinity Set
optional<SeeSawN::ElementsN::RotationMatrix3DT> mBRA;    ///< REMOVEME BRA
       optional< array<vector<float>,3> >  previousDataFrame;  ///< Previous data frame
QuaternionT previousQ;  ///< Rotation quaternion belonging to the previous frame


		/**@name Implementation details */ //@{
		void Validate(const CameraC& camera);	///< Validates a camera
		vector<char> GeneratePacket(const string& name, const vector<float>& data, int packetType);	///< Generates a packet
		array<vector<float>,3> CameraToData(const CameraC& camera);	///< Converts camera parameters to data items
		//@}

	public:

		vector<char> GenerateMessage(const CameraC& camera, float minRotation=0);		///< Generates a remote maps message for transmission
};	//class BrainstormRemoteMapsC

}	//UtilityN
}	//VisualMediaN




#endif /* BRAINSTORM_REMOTE_MAPS_IPP_6681982 */
