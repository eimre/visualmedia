/**
 * @file DecklinkWrapper.cpp Implementation of the wrapper for the Decklink API
 * @author Evren Imre
 * @date 28 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "DecklinkWrapper.h"
#include <iostream>
namespace VisualMediaN
{
namespace UtilityN
{


#ifdef DECKLINK_SUPPORT
/********** DecklinkInputCallbackC **********/

/**
 * @brief Default constructor
 */
DecklinkInputCallbackC::DecklinkInputCallbackC() : refCount(1)
{}

HRESULT STDMETHODCALLTYPE DecklinkInputCallbackC::QueryInterface(REFIID iid, LPVOID *ppv)
{
	return E_NOINTERFACE;
}

ULONG DecklinkInputCallbackC::AddRef(void)
{
//	return __sync_add_and_fetch(&refCount, 1);  //GCC specific
    return (++refCount);
}

ULONG DecklinkInputCallbackC::Release(void)
{
//	int32_t newRefValue = __sync_sub_and_fetch(&refCount, 1);   //GCC specific
    int32_t newRefValue = (--refCount);
	if (newRefValue == 0)
	{
		delete this;
		return 0;
	}
	return newRefValue;
}


HRESULT DecklinkInputCallbackC::VideoInputFormatChanged(BMDVideoInputFormatChangedEvents events, IDeckLinkDisplayMode *mode, BMDDetectedVideoInputFormatFlags formatFlags)
{
	throw(runtime_error("DecklinkInputCallbackC::VideoInputFormatChanged : Video input format change is not supported yet"));
}

HRESULT DecklinkInputCallbackC::VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame)
{
	if(!videoFrame)
		return S_FALSE;

	//Get the pointer to the data
	void* data;
	videoFrame->GetBytes(&data);

	//Move to an OpenCV matrix
	currentFrame = Mat(videoFrame->GetHeight(), videoFrame->GetWidth(), CV_8UC2, data, videoFrame->GetRowBytes());

	flagUncloned=true;

	return S_OK;
}

/**
 * @brief Returns a clone of \c currentFrame
 */
Mat DecklinkInputCallbackC::CloneCurrentFrame()
{
    flagUncloned=false;
	return currentFrame.clone();
}	//Mat CloneCurrentFrame()

/**
 * @brief Returns \c flagUncloned
 * @returns \c flagUncloned
 * @remarks This can be used to query whether a new frame has arrived
 */
bool DecklinkInputCallbackC::IsUncloned() const
{
    return flagUncloned;
}   //bool IsCloned()

#endif	//DECKLINK_SUPPORT

/********** DecklinkWrapperC **********/

/**
 * @brief Constructor
 * @post The object is invalid
 */
DecklinkWrapperC::DecklinkWrapperC()
{
	flagValid=false;
	#ifdef DECKLINK_SUPPORT
	displayModeMap={ {"HD1080p30", DisplayModeC{bmdModeHD1080p30, 30, 1920, 1080, false}},
					 {"HD1080p25", DisplayModeC{bmdModeHD1080p25, 25, 1920, 1080, false}},
					 {"HD1080i50", DisplayModeC{bmdModeHD1080i50, 25, 1920, 1080, true}},
					 {"PAL", DisplayModeC{bmdModePAL, 25, 720, 576, true}}

						 };

	pixelFormatMap={ {"YUV", bmdFormat8BitYUV}
			//	 {"RGB", bmdFormat8BitARGB}
				};

	colourModeMap={ {"601", bmdDisplayModeColorspaceRec601},
				   {"709", bmdDisplayModeColorspaceRec709}
				};
	#endif	//DECKLINK_SUPPORT
}	//DecklinkWrapperC()

/**
 * @brief Destructor
 */
DecklinkWrapperC::~DecklinkWrapperC()
{

	//FIXME Segfaults, but not sure why
	#ifdef DECKLINK_SUPPORT
/*	deckLinkIterator->Release();
	deckLink->Release();
	deckLinkInput->Release();
	callback->Release();*/
	#endif	//DECKLINK_SUPPORT

}	//~DecklinkWrapperC()

/**
 * @brief Initialisation
 * @param[in] displayModeString Display mode
 * @param[in] pixelFormatString Pixel format
 * @param[in] colourModeString Color mode
 * @pre \c displayMode is a valid display mode
 * @pre \c pixelFormat is a valid pixel format
 * @pre \c colorMode is a valid colour mode
 * @post The object is valid
 */
void DecklinkWrapperC::Initialise(const string& displayModeString, const string& pixelFormatString, const string& colourModeString)
{
	#ifdef DECKLINK_SUPPORT
	auto itDM=displayModeMap.find(displayModeString);
	if(itDM==displayModeMap.end())
		throw(invalid_argument(string("DecklinkWrapperC::Initialise : Invalid display mode. Value=")+displayModeString));
	else
		displayMode=itDM->second;

	auto itPF=pixelFormatMap.find(pixelFormatString);
	if(itPF==pixelFormatMap.end())
		throw(invalid_argument(string("DecklinkWrapperC::Initialise : Invalid pixel format. Value=")+pixelFormatString));
	else
		pixelFormat=itPF->second;

	auto itCM=colourModeMap.find(colourModeString);
	if(itCM==colourModeMap.end())
		throw(invalid_argument(string("DecklinkWrapperC::Initialise : Invalid colour mode. Value=")+colourModeString));
	else
		colourMode=itCM->second;

#if _MSC_VER
	CoInitialize(NULL);
	CoCreateInstance(CLSID_CDeckLinkIterator, NULL, CLSCTX_ALL, IID_IDeckLinkIterator, (void**)&deckLinkIterator);  //Windows interface
#else
	deckLinkIterator = CreateDeckLinkIteratorInstance();
#endif

	//Allocate the callback object
	callback = new DecklinkInputCallbackC;

	//Get the DeckLink Inputs
	deckLinkIterator->Next(&deckLink);
	deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput);

	//Register the callback object
	deckLinkInput->SetCallback(callback);

	//Enable the video input with the selected format
	deckLinkInput->EnableVideoInput(displayMode.mode, pixelFormat, colourMode);

	//Disable the audio
	deckLinkInput->DisableAudioInput();

	flagValid=true;
	#endif	//DECKLINK_SUPPORT

}	//void Initialise()

/**
 * @brief Starts the capture
 * @pre The object is valid
 */
void DecklinkWrapperC::StartCapture()
{
	//Preconditions
	assert(flagValid);
	#ifdef DECKLINK_SUPPORT
	deckLinkInput->StartStreams();

	//Wait until the first frame arrives
	while(GetLastFrame().rows==0)
	    sleep_for(milliseconds((unsigned int)(1e3/GetFrameRate())));

	#endif	//DECKLINK_SUPPORT
}	//void DecklinkWrapperC::StartCapture()

/**
 * @brief Stops the capture
 * @pre The object is valid
 */
void DecklinkWrapperC::StopCapture()
{
	//Preconditions
	assert(flagValid);
	#ifdef DECKLINK_SUPPORT
	deckLinkInput->StopStreams();
	#endif	//DECKLINK_SUPPORT
}	//void DecklinkWrapperC::StopCapture()

/**
 * @brief Returns the last frame captured by the frame grabber
 * @pre The object is valid
 */
Mat DecklinkWrapperC::GetLastFrame() const
{
	return callback->CloneCurrentFrame();
}	//void GetLastFrame()

/**
 * @brief Returns the frame rate
 * @return Frame rate
 */
double DecklinkWrapperC::GetFrameRate() const
{
	//Preconditions
	assert(flagValid);
	#ifdef DECKLINK_SUPPORT
	return displayMode.frameRate;
	#else
	return 0;
	#endif	//DECKLINK_SUPPORT
}	//double GetFrameRate()

/**
 * @brief Returns the frame height
 * @return Frame rate
 */
unsigned int DecklinkWrapperC::GetFrameHeight() const
{
	//Preconditions
	assert(flagValid);
	#ifdef DECKLINK_SUPPORT
	return displayMode.height;
	#else
	return 0;
	#endif	//DECKLINK_SUPPORT
}	//double GetHeight()

/**
 * @brief Returns the frame width
 * @return Frame rate
 */
unsigned int DecklinkWrapperC::GetFrameWidth() const
{
	//Preconditions
	assert(flagValid);
	#ifdef DECKLINK_SUPPORT
	return displayMode.width;
	#else
	return 0;
	#endif	//DECKLINK_SUPPORT
}	//double GetFrameWidth()

/**
 * @brief Returns \c flagValid
 */
bool DecklinkWrapperC::IsValid() const
{
	return flagValid;
}	//bool IsValid()

/**
 * @brief Returns \c true if there is a new (and unread) image exists
 * @return \c true if there is an "uncloned" frame in the buffer
 */
bool DecklinkWrapperC::QueryNewFrame() const
{
    return callback->IsUncloned();
}   //bool QueryNewFrame() const

}	//UtilityN
}	//VisualMediaN


