/**
 * @file DecklinkWrapper.ipp Implementation details for the wrapper for the Decklink API
 * @author Evren Imre
 * @date 28 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef DECKLINK_WRAPPER_IPP_6239090
#define DECKLINK_WRAPPER_IPP_6239090

#ifdef DECKLINK_SUPPORT
#include <BlackmagicSDK/DeckLinkAPI.h>
#endif	//DECKLINK_SUPPORT

#include <cstddef>
#include <stdexcept>
#include <string>
#include <map>
#include <chrono>
#include <thread>
#include <atomic>
#include <opencv2/core.hpp>

namespace VisualMediaN
{
namespace UtilityN
{

using std::runtime_error;
using std::invalid_argument;
using std::string;
using std::map;
using std::this_thread::sleep_for;
using std::chrono::milliseconds;
using std::atomic;
using cv::Mat;

#ifdef DECKLINK_SUPPORT
/**
 * @brief Callback object to be registered with the frame grabber
 * @ingroup IO
 */
class DecklinkInputCallbackC : public IDeckLinkInputCallback
{
	private:

		/** @name State */ //@{
		atomic<ULONG> refCount;
		Mat currentFrame;

		bool flagUncloned=false;  ///< If \c true , \c currentFrame is not yet cloned (read) by another module
		//@}

	public:

		DecklinkInputCallbackC();	///< Default constructor

		//Implementation of the abstract interface
		virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID *ppv);
		virtual ULONG STDMETHODCALLTYPE AddRef(void);
		virtual ULONG STDMETHODCALLTYPE  Release(void);
		virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents, IDeckLinkDisplayMode*, BMDDetectedVideoInputFormatFlags);

		virtual HRESULT VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame);

		//Access
		Mat CloneCurrentFrame();	///< Returns a clone of \c currentFrame
		bool IsUncloned() const;  ///< Returns \c flagUncloned
};	//class DecklinkInputCallbackC
#endif	//DECKLINK_SUPPORT

//TODO Must be singleton

/**
 * @brief Wrapper for the Decklink API
 * @remarks Functionality
 * 	- Device initialisation
 * 	- Start/Stop capture
 * @ingroup IO
 */
class DecklinkWrapperC
{
	private:

		struct DisplayModeC
		{
			BMDDisplayMode mode;	///< Display mode
			double frameRate;	///< Frame rate
			unsigned int width;	///< Frame width
			unsigned int height;	///< Frame height
			bool flagInterlaced;	///< \c true if interlaced
		};

		#ifdef DECKLINK_SUPPORT
		map<string, DisplayModeC> displayModeMap;
		map<string, BMDPixelFormat> pixelFormatMap;
		map<string, BMDVideoInputFlags> colourModeMap;

		#endif	//DECKLINK_SUPPORT

		/** @name Configuration */ //@{

		#ifdef DECKLINK_SUPPORT
		IDeckLinkIterator* deckLinkIterator=nullptr;	///< API entry point
		IDeckLink* deckLink=nullptr;	///< Device interface
		IDeckLinkInput* deckLinkInput=nullptr;	///< Input interface

		DecklinkInputCallbackC* callback=nullptr;	///< Callback object

		DisplayModeC displayMode;	///< Display mode
		BMDPixelFormat pixelFormat;	///< Pixel format
		BMDVideoInputFlags colourMode;	///< Colour mode
		#endif	//DECKLINK_SUPPORT
		//@}

		/**@name State */ //@{
		bool flagValid=false;	///< \c true if the object is initialised
		//@}

	public:

		DecklinkWrapperC();	///< Constructor
		~DecklinkWrapperC();	///< Destructor

		DecklinkWrapperC(const DecklinkWrapperC&)=delete;
		DecklinkWrapperC& operator=(const DecklinkWrapperC&)=delete;

		void Initialise(const string& displayModeString, const string& pixelFormatString, const string& colourModeString);	///< Initialisation
		void StartCapture();	///< Starts the capture
		void StopCapture();	///< Stops the capture
		Mat GetLastFrame() const;	///< Returns the last frame captured by the frame grabber
		double GetFrameRate() const;	///< Returns the frame rate
		unsigned int GetFrameHeight() const;	///< Returns the frame height
		unsigned int GetFrameWidth() const;	///< Returns the frame width

		bool IsValid() const;	///< Returns \c flagValid
		bool QueryNewFrame() const; ///< Returns \c true if there is a new (and unread) image exists
};	//class DecklinkWrapperC




}	//UtilityN
}	//VisualMediaN




#endif /* DECKLINK_WRAPPER_IPP_6239090 */
