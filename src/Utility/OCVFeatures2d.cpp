/**
 * @file OCVFeatures2d.cpp Implementation of various helper functions for the OpenCV 2D Features Framework
 * @author Evren Imre
 * @date 28 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "OCVFeatures2d.h"
namespace VisualMediaN
{
namespace UtilityN
{

/********** InterfaceORBC **********/

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceORBC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT output;

	output.nFeatures=src.get<int>("NoFeatures", output.nFeatures);
	output.scaleFactor=src.get<float>("ScaleFactor", output.scaleFactor);
	output.nLevels=src.get<int>("NoLevels", output.nLevels);
	output.edgeThreshold=src.get<int>("EdgeThreshold", output.edgeThreshold);
	output.WTA_K=src.get<int>("WTA_K", output.WTA_K);
	output.scoreType=src.get<int>("ScoreType", output.scoreType);
	output.patchSize=src.get<int>("PatchSize", output.patchSize);
	output.fastThreshold=src.get<int>("FASTThreshold", output.fastThreshold);

	return output;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceORBC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("NoFeatures", src.nFeatures);

	if(level>1)
	{
	    output.put("FASTThreshold", src.fastThreshold);
	    output.put("ScoreType", src.scoreType);
	}   //if(level>1)

	if(level>2)
	{

        output.put("ScaleFactor", src.scaleFactor);
        output.put("NoLevels", src.nLevels);
        output.put("EdgeThreshold", src.edgeThreshold);
        output.put("WTA_K", src.WTA_K);

        output.put("PatchSize", src.patchSize);
	}   //if(level>2)

	return output;
}	//auto MakeParameterTree(const ParameterObjectT& src, unsigned int level)

/********** InterfaceKAZEC **********/
/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Property tree holding the parameter values
 * @return A parameter object
 */
auto InterfaceKAZEC::MakeParameterObject(const ptree& src) -> ParameterObjectT
{
	ParameterObjectT output;

	output.flagExtended=src.get<bool>("FlagExtended", output.flagExtended);
	output.flagUpright=src.get<bool>("FlagUpright",output.flagUpright);
	output.threshold=src.get<double>("Threshold",output.threshold);
	output.nOctaves=src.get<unsigned int>("NoOctaves", output.nOctaves);
	output.nOctaveLayers=src.get<unsigned int>("NoOctaveLayers", output.nOctaveLayers);
	output.diffusivity=src.get<int>("Diffusivity",output.diffusivity);

	return output;
}	//auto MakeParameterObject(const ptree& src) -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Parameter object holding the parameter values
 * @param[in] level Higher values expose more parameters
 * @return A property tree
 */
ptree InterfaceKAZEC::MakeParameterTree(const ParameterObjectT& src, unsigned int level)
{
	ptree output;

	output.put("Threshold", src.threshold);

	if(level>2)
	{
        output.put("FlagExtended", src.flagExtended);
        output.put("FlagUpright", src.flagUpright);
        output.put("NoOctaves", src.nOctaves);
        output.put("NoOctaveLayers", src.nOctaveLayers);
        output.put("Diffusivity", src.diffusivity);
	}   //if(level>1)

	return output;
}	//auto MakeParameterTree(const ParameterObjectT& src, unsigned int level)

/********** FREE FUNCTIONS **********/

/**
 * @brief Converts a set of keypoints and descriptors into SeeSaw descriptors
 * @param[in] keypoints Keypoints
 * @param[in] descriptors Descriptors
 * @param[in] flagRandomise If \c true randomly reorder
 * @return A vector of image descriptors
 */
vector<ImageFeatureC> ConvertToImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors, bool flagRandomise)
{
	size_t nFeatures=keypoints.size();
	vector<ImageFeatureC> output(nFeatures);

	if(nFeatures==0)
		return output;

	//Randomise
    vector<size_t> indexRange(nFeatures);
    iota(indexRange.begin(), indexRange.end(), 0);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());


	size_t sDescriptor=descriptors.cols;
	for(size_t c=0; c<nFeatures; ++c)
	{
		size_t index=indexRange[c];
		output[index].Coordinate()=ImageFeatureC::coordinate_type(keypoints[c].pt.x, keypoints[c].pt.y);

		output[index].Descriptor().resize(sDescriptor);

		auto& pDest=output[index].Descriptor();
		const auto* pSrc=descriptors.ptr<float>(c);
		for(size_t c2=0; c2<sDescriptor; ++c2)
			pDest[c2]=pSrc[c2];

		pDest.normalize();
	}	//for(size_t c=0; c<nFeatures; ++c)

	return output;

}	//vector<ImageFeatureC> ConvertToImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors)

/**
 * @brief Converts a set of keypoints and descriptors into SeeSaw descriptors
 * @param[in] keypoints Keypoints
 * @param[in] descriptors Descriptors
 * @param[in] flagRandomise If \c true randomly reorder
 * @return A vector of binary image descriptors
 */
vector<BinaryImageFeatureC> ConvertToBinaryImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors, bool flagRandomise)
{
	size_t nFeatures=keypoints.size();
	vector<BinaryImageFeatureC> output(nFeatures);

	if(nFeatures==0)
		return output;

	//Randomise
    vector<size_t> indexRange(nFeatures);
    iota(indexRange.begin(), indexRange.end(), 0);

    if(flagRandomise)
    	random_shuffle(indexRange.begin(), indexRange.end());

	size_t sDescriptor=descriptors.cols;
	size_t sDescriptorBits=sDescriptor*CHAR_BIT;

	for(size_t c=0; c<nFeatures; ++c)
	{
		size_t index=indexRange[c];
		output[index].Coordinate()=BinaryImageFeatureC::coordinate_type(keypoints[c].pt.x, keypoints[c].pt.y);

		//Move to a temporary buffer with the same data type as Mat
		const auto* pSrc=descriptors.ptr<uchar>(c);
		dynamic_bitset<uchar> buffer;
		buffer.append(make_reverse_iterator(pSrc+sDescriptor), make_reverse_iterator(pSrc));

		//Cast to the binary descriptor type
		//TODO This is convenient but inefficient. Check whether it is possible to directly read from unsigned int pointers
		output[index].Descriptor().resize(sDescriptorBits);
		for(size_t c2=0; c2<sDescriptorBits; ++c2)
			output[index].Descriptor()[c2]=buffer[c2];
	}	//for(size_t c=0; c<nFeatures; ++c)

	return output;

}	//vector<BinaryImageFeatureC> ConvertToBinaryImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors)

/**
 * @brief Performs subpixel refinement on corner features
 * @param[in] keypoints Keypoint list
 * @param[in] image Image
 * @param[in] windowRadius Radius of the search window
 * @param[in] zeroZone Radius of the area excluded from the computation. Centred on the keypoint
 * @param[in] termination Termination criteria
 * @return Keypoints after subpixel refinement
 */
vector<KeyPoint> PerformSubpixelRefinement(const vector<KeyPoint>& keypoints, Mat image, const Size& windowRadius, const Size& zeroZone, const TermCriteria& termination)
{
	if(keypoints.empty())
		return keypoints;

	//Input array
	size_t nKeypoints=keypoints.size();
	vector<Point2f> points; points.reserve(nKeypoints);
	for_each(keypoints, [&](const KeyPoint& current){points.push_back(current.pt);} );

	//Refinement
	cornerSubPix(image, points, windowRadius, zeroZone, termination);

	//Output
	vector<KeyPoint> output(keypoints);
	for(size_t c=0; c<nKeypoints; ++c)
		output[c].pt=points[c];

	return output;
}	//vector<KeyPoint> PerformSubpixelRefinement(const vector<KeyPoint>& keypoints, Mat image, const TermCriteria& termination)

}	//UtilityN
}	//VisualMediaN

