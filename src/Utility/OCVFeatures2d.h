/**
 * @file OCVFeatures2d.h Public interface for various helper functions for the OpenCV 2D Features Framework
 * @author Evren Imre
 * @date 28 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef OCV_FEATURES_2D_H_9918012
#define OCV_FEATURES_2D_H_9918012

#include "OCVFeatures2d.ipp"

namespace VisualMediaN
{
namespace UtilityN
{

struct ORBParametersC;	///< Parameters for ORB
class InterfaceORBC;	///< Application interface for ORB

struct KAZEParametersC;	///< Parameters for KAZE
class InterfaceKAZEC;	///< Application interface for KAZE

vector<ImageFeatureC> ConvertToImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors, bool flagRandomise);	///< Converts a set of keypoints and descriptors into SeeSaw descriptors
vector<BinaryImageFeatureC> ConvertToBinaryImageFeature(const vector<KeyPoint>& keypoints, const Mat& descriptors, bool flagRandomise);	///< Converts a set of keypoints and descriptors into SeeSaw descriptors

vector<KeyPoint> PerformSubpixelRefinement(const vector<KeyPoint>& keypoints, Mat image, const Size& windowRadius, const Size& zeroZone, const TermCriteria& termination);	///< Performs subpixel refinement on corner features

}	//UtilityN
}	//VisualMediaN


#endif /* OCV_FEATURES_2D_H_9918012*/
