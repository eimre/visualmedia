/**
 * @file OCVFeatures2d.ipp Implementation details for various helper functions for the OpenCV 2D Features Framework
 * @author Evren Imre
 * @date 28 Sep 2016
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef OCV_FEATURES_2D_IPP_92108990
#define OCV_FEATURES_2D_IPP_92108990

#include <vector>
#include <iterator>
#include <numeric>
#include <boost/property_tree/ptree.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range/algorithm.hpp>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/imgproc.hpp>
#include <SeeSaw/Elements/Feature.h>
#include <SeeSaw/ApplicationInterface/InterfaceUtility.h>

namespace VisualMediaN
{
namespace UtilityN
{

using std::vector;
using std::make_reverse_iterator;
using std::random_shuffle;
using std::iota;
using boost::property_tree::ptree;
using boost::dynamic_bitset;
using boost::for_each;
using cv::Mat;
using cv::KeyPoint;
using cv::ORB;
using cv::KAZE;
using cv::Size;
using cv::TermCriteria;
using cv::Point2f;
using cv::cornerSubPix;
using SeeSawN::ElementsN::ImageFeatureC;
using SeeSawN::ElementsN::BinaryImageFeatureC;
using SeeSawN::ApplicationN::ReadParameter;

/**
 * @brief Parameters for ORB
 * @ingroup Parameters
 */
struct ORBParametersC
{
	int nFeatures=500;	///< Number of features
	float scaleFactor=1.2;	///< Upsampling rate per pyramid level
	int nLevels=8;	/// Number of levels
	int edgeThreshold=31;	///< Border are to be omitted
	int firstLevel=0;	///< Fixed to 0
	int WTA_K=2;	///< Number of points used for generating a BRIEF descriptor
	int scoreType=ORB::HARRIS_SCORE;	///< Strength score type
	int patchSize=31;	///< Descriptor RoI size
	int fastThreshold=20;	///< Undocumented
};	//struct ORBParametersC

/**
 * @brief Parameters for KAZE
 * @ingroup Parameters
 */
struct KAZEParametersC
{
	bool flagExtended=true;
	bool flagUpright=false;
	double threshold=1e-4;
	unsigned int nOctaves=4;
	unsigned int nOctaveLayers=4;
	int diffusivity = KAZE::DIFF_PM_G2;
};	//struct KAZEParametersC

/**
 * @brief Application interface for ORB
 * @ingroup IO
 */
class InterfaceORBC
{
	public:

		typedef ORBParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceORBC

/**
 * @brief Application interface for KAZE
 * @ingroup IO
 */
class InterfaceKAZEC
{
	public:

		typedef KAZEParametersC ParameterObjectT;	///< Parameter object type

		static ParameterObjectT MakeParameterObject(const ptree& src);	///< Makes a parameter object from a property tree
		static ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level);	///< Makes a property tree from a parameter object
};	//class InterfaceKAZEC


}	//UtilityN
}	//VisualMediaN



#endif /* OCV_FEATURES_2D_IPP_92108990 */
