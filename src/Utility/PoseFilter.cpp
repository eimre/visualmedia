/**
 * @file PoseFilter.cpp Instantiations for \c PoseFilterC
 * @author Evren Imre
 * @date 4 May 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "PoseFilter.h"

namespace VisualMediaN
{
namespace UtilityN
{

/********** EXPLICIT INSTANTIATIONS **********/
template class PoseFilterC<P2PfSolverC>;
template class PoseFilterC<P4PSolverC>;

}	//namespace UtilityN
}	//namespace VisualMediaN
