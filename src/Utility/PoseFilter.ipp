/**
 * @file PoseFilter.ipp Implementation of \c PoseFilterC
 * @author Evren Imre
 * @date 4 May 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef POSE_FILTER_IPP_5610982
#define POSE_FILTER_IPP_5610982

#include <array>
#include <utility>
#include <functional>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/concept_check.hpp>
#include <SeeSaw/Elements/GeometricEntity.h>
#include <SeeSaw/Geometry/GeometrySolverConcept.h>
#include <SeeSaw/Geometry/P2PfSolver.h>
#include <SeeSaw/Geometry/P4PSolver.h>

namespace VisualMediaN
{
namespace UtilityN
{

using std::array;
using std::prev;
using std::divides;
using std::bind;
using boost::rotate;
using boost::accumulate;
using boost::transform;
using SeeSawN::ElementsN::CameraMatrixT;
using SeeSawN::GeometryN::GeometrySolverConceptC;
using SeeSawN::GeometryN::P2PfSolverC;
using SeeSawN::GeometryN::P4PSolverC;

/**
 * @brief A 3-tap pose filter
 * @tparam SolverT A camera solver
 * @pre \c SolverT is a model of \c GeometrySolverConceptC
 * @pre \c SolverT is a PnP solver (unenforced)
 * @remarks The camera solver determines the components on which the filter is applied
 * @remarks The filter has a delay of 1: it predicts the second element as a weighted average of the filter state
 * @ingroup Algorithm
 */
template<class SolverT>
class PoseFilterC
{
	//@cond CONCEPT_CHECK
	BOOST_CONCEPT_ASSERT((GeometrySolverConceptC<SolverT>));
	//@endcond

	private:

		/** @name State */ //@{
		array<CameraMatrixT,3> buffer={CameraMatrixT::Zero(), CameraMatrixT::Zero(), CameraMatrixT::Zero()};	///< Filter state
		array<double, 3> weights={0,0,0};	///< Weights for each tap
		//@}

		/** @name Implementation details */ //@{
		void Update(const CameraMatrixT& measurement, double weight);	///< Updates the filter state
		//@}

	public:

		CameraMatrixT Apply(const CameraMatrixT& measurement, double weight);	///< Applies the filter to the buffer

};	//class PoseFilterC

/**
 * @brief Updates the filter state
 * @param[in] measurement Measurement
 * @param[in] weight Measurement weight
 */
template<class SolverT>
void PoseFilterC<SolverT>::Update(const CameraMatrixT& measurement, double weight)
{
	rotate(buffer, prev(buffer.end(),1));
	buffer[0]=measurement;

	rotate(weights, prev(weights.end(),1));
	weights[0]=weight;
}	//void Update(const CameraMatrixT& measurement, double weight)

/**
 * @brief Applies the filter to the buffer
 * @param[in] measurement Measurement
 * @param[in] weight Measurement weight
 * @return Filtered output
 */
template<class SolverT>
CameraMatrixT PoseFilterC<SolverT>::Apply(const CameraMatrixT& measurement, double weight)
{
	Update(measurement, weight);

	//If the leading or the lagging elements have 0 weight, no filtering
	if(weights[0]==0 || weights[2]==0)
		return buffer[1];

	array<double,3> normalisedWeights;
	double totalWeight=accumulate(weights, 0.0);
	transform(weights, normalisedWeights.begin(), bind(divides<double>(), std::placeholders::_1, totalWeight));

	buffer[1] = SolverT::MakeModelFromMinimal(SolverT::ComputeSampleStatistics(buffer, normalisedWeights, normalisedWeights).GetMean() );

	return buffer[1];
}	//CameraMatrixT Apply(const CameraMatrixT& measurement, double weight)

}	//namespace UtilityN
}	//namespace VisualMediaN



#endif /* POSE_FILTER_IPP_5610982 */
