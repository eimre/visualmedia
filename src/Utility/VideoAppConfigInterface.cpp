/**
 * @file VideoAppConfigInterface.cpp Implementation of the application configuration interface for video input
 * @author Evren Imre
 * @date 29 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "VideoAppConfigInterface.h"

namespace VisualMediaN
{
namespace UtilityN
{

/**
 * @brief Constructor
 * @post Initialises the static variables
 */
VideoAppConfigInterfaceC::VideoAppConfigInterfaceC()
{
	initializer_list<bimap<string, VideoSourceCodeT>::value_type> tmpSourceMap{ {bimap<string, VideoSourceCodeT>::value_type("Local", VideoSourceCodeT::LOCAL)},
																					 {bimap<string, VideoSourceCodeT>::value_type("USB", VideoSourceCodeT::USB)},
																					 {bimap<string, VideoSourceCodeT>::value_type("FrameGrabber", VideoSourceCodeT::FRAMEGRABBER)}
																					};

	sourceMap=bimap<string, VideoSourceCodeT>(tmpSourceMap.begin(), tmpSourceMap.end());

	initializer_list<bimap<string, ColourSpaceT>::value_type> tmpColourSpaceMap{ {bimap<string, ColourSpaceT>::value_type("YUV", ColourSpaceT::YUV)},
																					 	 {bimap<string, ColourSpaceT>::value_type("RGB", ColourSpaceT::RGB)}
																					    };

	colourSpaceMap=bimap<string, ColourSpaceT>(tmpColourSpaceMap.begin(), tmpColourSpaceMap.end());
}	//VideoAppConfigInterfaceC()

/**
 * @brief Makes a parameter object from a property tree
 * @param[in] src Source parameter tree
 * @return Parameter object corresponding to the tree
 */
auto VideoAppConfigInterfaceC::MakeParameterObject(const ptree& src) const -> ParameterObjectT
{
	auto geq0=bind(greater_equal<double>(),_1,0);

	ParameterObjectT output;

	string sourceString=src.get<string>("General.Source", "Local");
	auto itST=sourceMap.left.find(sourceString);
	if(itST==sourceMap.left.end())
		throw(invalid_argument(string("VideoAppConfigInterfaceC::MakeParameterObject: Invalid source type. Value=")+sourceString));
	else
		output.sourceType=sourceMap.left.at(sourceString);

	string colourString=src.get<string>("General.ColourSpace", "RGB");
	auto itCS=colourSpaceMap.left.find(colourString);
	if(itCS==colourSpaceMap.left.end())
		throw(invalid_argument(string("VideoAppConfigInterfaceC::MakeParameterObject: Invalid colour space type. Value=")+colourString));
	else
		output.colourSpace=colourSpaceMap.left.at(colourString);

	output.flagInterlaced=src.get<bool>("General.FlagInterlaced", output.flagInterlaced);
	output.downsamplingFactor=ReadParameter(src, "General.DownsamplingFactor", geq0, "is not >=0", optional<double>(output.downsamplingFactor));

	output.videoFilename=src.get<string>("Local.Filename", output.videoFilename);

	output.deviceID= (output.sourceType== VideoSourceCodeT::USB) ?  src.get<int>("USB.DeviceId", output.deviceID) : src.get<int>("FrameGrabber.DeviceId", output.deviceID);
	output.displayMode= src.get<string>("FrameGrabber.DisplayMode", output.displayMode);
	output.colourMode=src.get<string>("FrameGrabber.ColourMode", output.colourMode);
	output.pixelFormat = src.get<string>("General.ColourSpace", output.pixelFormat);

	return output;
}	//auto MakeParameterObject(const ptree& src) const -> ParameterObjectT

/**
 * @brief Makes a property tree from a parameter object
 * @param[in] src Source object
 * @param[in] level Detail level
 * @return
 */
ptree VideoAppConfigInterfaceC::MakeParameterTree(const ParameterObjectT& src, unsigned int level) const
{
	ptree output;

	output.put("General","");
	output.put("Local","");
	output.put("USB", "");
	output.put("FrameGrabber", "");

	output.put("General.Source", sourceMap.right.at(src.sourceType));
	output.put("General.ColourSpace", colourSpaceMap.right.at(src.colourSpace));
	output.put("General.FlagInterlaced", src.flagInterlaced);
	output.put("General.DownsamplingFactor", src.downsamplingFactor);

	output.put("Local.Filename",src.videoFilename);

	output.put("USB.DeviceId", src.deviceID);

	output.put("FrameGrabber.DeviceId", src.deviceID);
	output.put("FrameGrabber.DisplayMode", "HD1080p30");
	output.put("FrameGrabber.ColourMode", "709");

	return output;
}	//ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level)

}	//UtilityN
}	//VisualMediaN
