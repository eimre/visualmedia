/**
 * @file VideoAppConfigInterface.ipp Implementation details for the application configuration interface for video input
 * @author Evren Imre
 * @date 29 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef VIDEO_APP_CONFIG_INTERFACE_IPP_7701904
#define VIDEO_APP_CONFIG_INTERFACE_IPP_7701904

#include <string>
#include <initializer_list>
#include <functional>
#include <stdexcept>
#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
#include <boost/bimap.hpp>
#include "SeeSaw/ApplicationInterface/InterfaceUtility.h"

namespace VisualMediaN
{
namespace UtilityN
{

using std::greater_equal;
using std::bind;
using std::string;
using std::initializer_list;
using std::invalid_argument;
using boost::property_tree::ptree;
using boost::optional;
using boost::bimap;
using SeeSawN::ApplicationN::ReadParameter;

enum class VideoSourceCodeT{LOCAL,USB,FRAMEGRABBER};	///< Source type. File, camera connected to USB, camera connected through a frame grabber
enum class ColourSpaceT{YUV, RGB};	///< Colour space type

/**
 * @brief Configuration object for the video input
 * @ingroup IO
 */
struct VideoAppConfigC
{
	VideoSourceCodeT sourceType=VideoSourceCodeT::LOCAL;	///< Input source type
	ColourSpaceT colourSpace=ColourSpaceT::RGB;	///< Colour space
	bool flagInterlaced=false;	///< If \c true the frame is treated as interlaced- only the even field is processed
	unsigned int downsamplingFactor=1;	///< Number of pyramid levels for downsampling. Each level decimates the image by 2

	//File
	string videoFilename;	///< Filename for the input video file. Non-empty only if \c sourceType=LOCAL

	//Live
	int deviceID=-1;	///< Device id, for USB and FRAMEGRABBER

	//Frame grabber
	string displayMode;	///< Display mode. See DecklinkWrapperC for valid input values
	string pixelFormat;	///< Pixel format. YUV or RGB
	string colourMode;	///< Colour mode. 601 or 709
};	//struct VideoAppConfigC

/**
 * @brief Application config file helper for video input
 * @ingroup IO
 */
class VideoAppConfigInterfaceC
{
	private:

		typedef VideoAppConfigC ParameterObjectT;	///< Type of the parameter object

		bimap<string, VideoSourceCodeT> sourceMap;	///< Bidirectional map for a source type and a string
		bimap<string, ColourSpaceT> colourSpaceMap;	///< Bidirectional map for a colour space type and a string

	public:

		VideoAppConfigInterfaceC();	///< Constructor

		ParameterObjectT MakeParameterObject(const ptree& src) const;	///< Makes a parameter object from a property tree
		ptree MakeParameterTree(const ParameterObjectT& src, unsigned int level) const;	///< Makes a property tree from a parameter object

};	//class VideoAppConfigInterfaceC

}	//UtilityN
}	//VisualMediaN




#endif /* VIDEOAPPCONFIGINTERFACE_IPP_ */
