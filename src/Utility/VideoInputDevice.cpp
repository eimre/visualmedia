/**
 * @file VideoInputDevice.cpp Implementation of the video input device
 * @author Evren Imre
 * @date 30 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/

#include "VideoInputDevice.h"

namespace VisualMediaN
{
namespace UtilityN
{


/**
 * @brief Default constructor
 * @post The object is not valid
 */
VideoInputDeviceC::VideoInputDeviceC()
{}

/**
 * @brief Initialises an object
 * @param[in] cconfig Video input configuration object
 * @post The object is valid
 */
void VideoInputDeviceC::Initialise(const VideoAppConfigC& cconfig)
{
	flagValid=true;
	config=cconfig;

	//Local capture
	if(config.sourceType==VideoSourceCodeT::LOCAL)
	{
		ocvVideoSource.open(config.videoFilename);

		if(!ocvVideoSource.isOpened())
			throw(runtime_error(string("VideoInputDeviceC::VideoInputDeviceC: File cannot be opened. Value=")+config.videoFilename));
	}	//if(config.captureMode==VideoSourceCodeT::LOCAL)

	//USB camera
	if(config.sourceType==VideoSourceCodeT::USB)
	{
		properties.flagLive=true;

		ocvVideoSource.open(config.deviceID);

		if(!ocvVideoSource.isOpened())
			throw(runtime_error("VideoInputDeviceC::VideoInputDeviceC: Camera not found"));
	}	//if(config.captureMode==VideoSourceCodeT::USB)

	//OpenCV interface
	if(config.sourceType==VideoSourceCodeT::LOCAL || config.sourceType==VideoSourceCodeT::USB)
	{
        properties.frameRate=ocvVideoSource.get(cv::CAP_PROP_FPS);
		properties.width=ocvVideoSource.get(cv::CAP_PROP_FRAME_WIDTH);
		properties.height=ocvVideoSource.get(cv::CAP_PROP_FRAME_HEIGHT);
	}	//if(config.captureMode==VideoSourceCodeT::LOCAL || config.captureMode==VideoSourceCodeT::USB)

	//Decklink interface
	if(config.sourceType==VideoSourceCodeT::FRAMEGRABBER)
	{
		frameGrabberSource.Initialise(config.displayMode, config.pixelFormat, config.colourMode);
		properties.flagLive=true;
		properties.frameRate=frameGrabberSource.GetFrameRate();
		properties.height=frameGrabberSource.GetFrameHeight();
		properties.width=frameGrabberSource.GetFrameWidth();

		frameGrabberSource.StartCapture();
	}
}	//VideoInputDeviceC(const VideoAppConfigC& cconfig) : flagValid(true), config(cconfig)

/**
 * @brief Reads the current frame from the source
 * @param[out] frame Contains the frame read from the device
 * @pre The object is valid
 */
void VideoInputDeviceC::GetFrame(Mat& frame)
{
	//Preconditions
	assert(flagValid);

	if(config.sourceType==VideoSourceCodeT::LOCAL || config.sourceType==VideoSourceCodeT::USB)
		ocvVideoSource>>frame;

	if(config.sourceType==VideoSourceCodeT::FRAMEGRABBER)
		frame=frameGrabberSource.GetLastFrame();
}	//void GetFrame(Mat& frame) const

/**
 * @brief Returns the frame height
 * @return Frame height
 * @pre The object is valid
 */
unsigned int VideoInputDeviceC::GetHeight() const
{
	//Preconditions
	assert(flagValid);

	return properties.height;
}	//unsigned int GetHeight() const

/**
 * @brief Returns the frame width
 * @return Frame width
 * @pre The object is valid
 */
unsigned int VideoInputDeviceC::GetWidth() const
{
	//Preconditions
	assert(flagValid);

	return properties.width;
}	//unsigned int GetWidth() const

/**
 * @brief Returns the frame rate
 * @return Frame rate
 * @pre The object is valid
 */
double VideoInputDeviceC::GetFrameRate() const
{
	return properties.frameRate;
}	//double GetFrameRate() const

/**
 * @brief Returns the position in a video file, in milliseconds
 * @return Frame position, in msec. If not LOCAL, -1
 * @pre The object is valid
 */
double VideoInputDeviceC::GetPosition() const
{
	//Preconditions
	assert(flagValid);

	return 	(config.sourceType==VideoSourceCodeT::LOCAL) ? ocvVideoSource.get(cv::CAP_PROP_POS_MSEC) : -1;
}	//double GetPosition() const

/**
 * @brief Checks whether the input is fully ingested
 * @return \c true if the end of the file is reached. Always \c false for live input
 */
bool VideoInputDeviceC::IsEoF() const
{
	return (config.sourceType==VideoSourceCodeT::LOCAL) && (ocvVideoSource.get(cv::CAP_PROP_POS_FRAMES) == ocvVideoSource.get(cv::CAP_PROP_FRAME_COUNT) );
}	//bool IsEoF() const

/**
 * @brief Returns \c flagLive
 * @return \c true if the data comes from a camera, as opposed to a local file
 * @pre The object is valid
 */
bool VideoInputDeviceC::IsLive() const
{
	return properties.flagLive;
}	//bool IsLive() const

/**
 * @brief Returns \c flagValid
 * @return \c flagValid
 */
bool VideoInputDeviceC::IsValid() const
{
	return flagValid;
}	//bool IsValid() const

/**
 * @brief Returns the OpenCV code for converting the acquired image to grayscale
 * @return OpenCV colour conversion code
 */
int VideoInputDeviceC::GetGrayscaleConversionCode() const
{
	//OpenCV, BGR
	if ( (config.sourceType==VideoSourceCodeT::LOCAL) || (config.sourceType==VideoSourceCodeT::USB) )
		return cv::COLOR_BGR2GRAY;

	if(config.colourSpace==ColourSpaceT::RGB)
		return cv::COLOR_RGB2GRAY;

	if(config.colourSpace==ColourSpaceT::YUV)
		return cv::COLOR_YUV2GRAY_UYVY;

	return -1;
}	//int GetGrayscaleConversionCode()

/**
 * @brief  Returns the OpenCV code for converting the acquired image to BGR
 * @return YUV-to-RGB conversion code. -1 if the image is already BGR
 */
int VideoInputDeviceC::GetBGRConversionCode() const
{
	if(config.sourceType==VideoSourceCodeT::LOCAL)
		return -1;

	if(config.colourSpace==ColourSpaceT::YUV)
		return cv::COLOR_YUV2BGR_UYVY;

	if(config.colourSpace==ColourSpaceT::RGB)
		return cv::COLOR_RGB2BGR;

	return -1;
}	//int GetYUV2RGBConversionCode()

/**
 * @brief Returns \c true if the next frame is available
 * @return \c true if a new frame is available
 */
bool VideoInputDeviceC::QueryNewFrame() const
{
    //If LOCAL, a new frame can be obtained upon request. For USB, no querying facility. So, always true
    return config.sourceType==VideoSourceCodeT::FRAMEGRABBER ? frameGrabberSource.QueryNewFrame() : true;
}   //bool QueryNewFrame() const

}	//UtilityN
}	//VisualMediaN
