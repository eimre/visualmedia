/**
 * @file VideoInputDevice.ipp Implementation details for the video input device
 * @author Evren Imre
 * @date 30 Mar 2017
 */

/***************************************************************************
This file is a part of VisualMedia, a collection of computer vision libraries.

Copyright (C) 2009-2018 University of Surrey

VisualMedia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VisualMedia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VisualMedia.  If not, see <http://www.gnu.org/licenses/>.
 ***************************************************************************/
#ifndef VIDEO_INPUT_DEVICE_IPP_5691282
#define VIDEO_INPUT_DEVICE_IPP_5691282

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include "VideoAppConfigInterface.h"
#include "DecklinkWrapper.h"

namespace VisualMediaN
{
namespace UtilityN
{

using cv::Mat;
using cv::VideoCapture;
using VisualMediaN::UtilityN::VideoAppConfigC;
using VisualMediaN::UtilityN::VideoSourceCodeT;

//TODO Must be singleton
/**
 * @brief Manages a video source
 * @ingroup IO
 */
class VideoInputDeviceC
{
	private:

		struct VideoPropertiesC
		{
			unsigned int height=0;	///< Image height
			unsigned int width=0;	///< Image width
			double frameRate=-1;	///< Frame rate

			bool flagLive=false;	///< \c true if live input (as opposed to file)
		} properties;	///< Video properties

		/** @name State */	//@{
		bool flagValid=false;	///< \c true if the object is initialised
		//@}

		/** @name Configuration */ //@{
		VideoAppConfigC config;	///< Configuration

		//Sources. Only one of them is valid
		VideoCapture ocvVideoSource;	///< OpenCV video device, for local and usb
		DecklinkWrapperC frameGrabberSource;	///< Decklink device, for the frame grabber
		//@}

	public:

		VideoInputDeviceC();	///< Default constructor

		VideoInputDeviceC(const VideoInputDeviceC&)=delete;
		VideoInputDeviceC& operator=(const VideoInputDeviceC&)=delete;

		void Initialise(const VideoAppConfigC& cconfig);	///< Initialises an object

		//Accessors
		void GetFrame(Mat& frame);	///< Reads the current frame from the source
		unsigned int GetHeight() const;	///< Returns the frame height
		unsigned int GetWidth() const;	///< Returns the frame width
		double GetFrameRate() const;	///< Returns the frame rate
		double GetPosition() const;	///< Returns the position in a video file, in milliseconds
		bool IsEoF() const;	///< Checks whether the input is fully ingested
		bool IsLive() const;	///< Returns \c flagLive
		bool IsValid() const;	///< Returns \c flagValid

		bool QueryNewFrame() const; ///< Returns \c true if the next frame is available

		//Utility
		int GetGrayscaleConversionCode() const;	///< Returns the OpenCV code for converting the acquired image to grayscale
		int GetBGRConversionCode() const;	///< Returns the OpenCV code for converting the acquired image to BGR
};	//class VideoInputDeviceC

}	//UtilityN
}	//VisualMediaN



#endif /* VIDEO_INPUT_DEVICE_IPP_5691282 */
